/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**@file otbITKIteratorHelpers.h
 * Define some helper functions to manipulate ITK iterators as C++ standard
 * iterators.
 *
 * @code
 * using InputIterator = itk::ImageScanlineConstIterator< ImageType >;
 * InputIterator  inIt(this->GetInput(), inputRegionForThread);
 * using OutputIterator = itk::ImageScanlineIterator< ImageType >;
 * OutputIterator outIt(this->GetOutput(), outputRegionForThread);
 *
 * for( ; !outIt.IsAtEnd() ; outIt+=itk::Line{}, inIt+=itk::Line{})
 * {
 *   for ( ; !inIt.IsAtEndOfLine() && !outIt.IsAtEndOfLine() ; ++outIt, ++inIt)
 *   {
 *     *outIt = *inIt;
 *   }
 * }
 * @endcode
 */

#ifndef otbITKIteratorHelpers_h
#define otbITKIteratorHelpers_h

#include <type_traits>
#include "Common/otbMetaProgrammingLibrary.h"

namespace itk
{
template<class, class = otb::void_t<>>
struct has_next : std::false_type {};

template<class T>
struct has_next<T, otb::void_t<decltype(std::declval<T&>().Next())>>
: std::true_type {};

template <typename Iterator>
inline
typename std::enable_if<has_next<Iterator>::value, Iterator>::type
operator++(Iterator& it, int)
{
  it.Next();
  return it;
}

template <typename Iterator>
inline
typename std::enable_if<! has_next<Iterator>::value, Iterator>::type
operator++(Iterator& , int)
{
  static_assert(has_next<Iterator>::value, "Not compatible with ITK iterators");
}

template<class, class = otb::void_t<>>
struct has_next_line : std::false_type {};

template<class T>
struct has_next_line<T, otb::void_t<decltype(std::declval<T&>().NextLine())>>
: std::true_type {};

struct Line {};

template <typename Iterator>
inline
typename std::enable_if<has_next_line<Iterator>::value, Iterator>::type &
operator+=(Iterator& it, Line)
{
  it.NextLine();
  return it;
}


template <typename Iterator>
inline
typename std::enable_if<! has_next_line<Iterator>::value, Iterator>::type
operator+=(Iterator&, Line)
{
  static_assert(has_next_line<Iterator>::value, "Not compatible with ITK scanline iterators");
}


template <typename TIterator>
struct ProxyDerefIterator
{
  using IteratorType = TIterator;
  using PixelType    = typename IteratorType::PixelType;

  ProxyDerefIterator(TIterator & it) : m_it(it) {}
  ProxyDerefIterator & operator=(PixelType const& v) {
    m_it.Set(v);
    return *this;
  }
  /*explicit*/ operator PixelType() const {
    return m_it.Get();
  }

#if 1
  decltype(auto) operator[](unsigned i) const {
    return m_it.Get()[i];
  }
#endif
private:
  IteratorType & m_it;
};

template<class, class = otb::void_t<>>
struct has_get : std::false_type {};

template<class T>
struct has_get<T, otb::void_t<decltype(std::declval<T&>().Get())>>
: std::true_type {};

template<class, class = otb::void_t<>>
struct has_set : std::false_type {};

template<class T>
struct has_set<T,
  otb::void_t<decltype(
      std::declval<T&>().Set(std::declval<typename T::PixelType>()))>>
: std::true_type {};

template <typename Iterator>
inline
typename std::enable_if<
  has_get<Iterator>::value && !has_set<Iterator>::value,
  ProxyDerefIterator<Iterator const>>::type
operator*(Iterator const& it)
{
  ProxyDerefIterator<Iterator const> p{it};
  return p;
}

template <typename Iterator>
inline
typename std::enable_if<
  has_get<Iterator>::value && has_set<Iterator>::value,
  ProxyDerefIterator<Iterator>>::type
operator*(Iterator & it)
{
  ProxyDerefIterator<Iterator> p{it};
  return p;
}

#if 0
template <typename Iterator>
inline
typename std::enable_if<
!has_get<Iterator>::value && !has_set<Iterator>::value>::type
operator*(Iterator &)
{
  static_assert(has_get<Iterator>::value, "Not an input iterator");
  static_assert(has_set<Iterator>::value, "Not an output iterator");
}
#endif

} // itk namespace

#endif // otbITKIteratorHelpers_h
