/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file Common/otbMemberVariable.h
 * Defines macros to define setters, getters...
 *
 * Unlike macros from ITK, no call to `Modified()` is issued.
 */

#ifndef otbMemberVariable_h
#define otbMemberVariable_h

#include "Common/otbMetaProgrammingLibrary.h"
#include "otbMacro.h"
#include <utility> // std::forward

/**
 * Defines the `UpperCamelCase` setter named `Set{name}`.
 * The setter will assign its parameter to member variable `m_{name}`.
 * \note The value is perfectly forwarded (through an _universal/perfect reference_).
 */
#define otbSetMacro(name)                                  \
  template <typename T>                                    \
  void Set##name(T && arg)                                 \
  {                                                        \
    otbLogMacro(Debug, << "setting " #name " to " << arg); \
    this->m_##name = std::forward<T>(arg);                 \
  }

/**
 * Defines the `snake_case` setter named `set_{name}`.
 * The setter will assign its parameter to member variable `m_{name}`.
 * \note The value is perfectly forwarded (through an _universal/perfect reference_).
 */
#define otb_set_macro(name)                        \
  template <typename T>                            \
  void set_##name(T && arg)                        \
  {                                                \
    this->m_##name = std::forward<T>(arg);         \
  }

/**
 * Defines the `UpperCamelCase` getter named `Get{name}`.
 * The getter will return the member variable `m_{name}`.
 * \note The value is return is returned by-value if variable type is _trivially copiable_ and
 * smaller than 128bits. It's returned by-const-reference otherwise.
 * \warning Given the parameter type is auto-deduced, the getter needs to be declared after the
 * attribute is declared.
 */
#define otbGetMacro(name)                          \
  auto Get##name() const                           \
  -> otb::best_way_to_return_t<decltype(m_##name)> \
  {                                                \
    return this->m_##name;                         \
  }

/**
 * Defines the `snake_case` getter named `get_{name}`.
 * The getter will return the member variable `m_{name}`.
 * \note The value is return is returned by-value if variable type is _trivially copiable_ and
 * smaller than 128bits. It's returned by-const-reference otherwise.
 * \warning Given the parameter type is auto-deduced, the getter needs to be declared after the
 * attribute is declared.
 */
#define otb_get_macro(name)                        \
  auto get_##name() const                          \
  -> otb::best_way_to_return_t<decltype(m_##name)> \
  {                                                \
    return this->m_##name;                         \
  }

#endif  // otbMemberVariable_h
