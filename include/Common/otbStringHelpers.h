/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef otbStringHelpers_h
#define otbStringHelpers_h

#if OTB_VERSION_MAJOR >= 8
#include "otbImageMetadata.h"
#else
#include "otbImageKeywordlist.h"
#endif
#include "otbStringUtilities.h"
#include <string>

namespace otb
{

template <typename T>
T sto(std::string const&);

template <>
inline int sto<int>(std::string const& s)
{ return std::stoi(s); }

template <>
inline float sto<float>(std::string const& s)
{ return std::stof(s); }

template <>
inline double sto<double>(std::string const& s)
{ return std::stod(s); }

template <>
inline long double sto<long double>(std::string const& s)
{ return std::stold(s); }

template <>
inline std::string sto<std::string>(std::string const& s)
{ return s; }

/**
 * Helper function to fetch data from keyword lists.
 * \tparam T  Type of the element to find
 * \param[in] kwl            Keyword list where to search for
 * \param[in] key            Name of the element to search and decode
 * \param[in] context        Context to help build error message
 * \param[in] default_value  Value returned when there is no element associated
 *                           to `key`
 *
 * \return `default_value` if the `key` isn't found in the keywordlist.
 * \throw itk::ExceptionObject with the `context` as message if the metadata
 * found at the `key` cannot be converted to a `T`.
 * \return the metadata found associated to the `key` decoded as a `T` type.
 * \todo move this function to `StringUtilities.h`
 */
#if OTB_VERSION_MAJOR >= 8
template <typename T>
inline
T value_or_unless(
    otb::ImageMetadata const& kwl,
    std::string const& key,
    otb::string_view context,
    T default_value)
{
  if (kwl.Has(key))
  {
    auto const& value = kwl[key];
    return to<T>(value, context);
  }
  else
  {
    return default_value;
  }
}
#else
template <typename T>
inline
T value_or_unless(
    otb::ImageKeywordlist const& kwl,
    std::string const& key,
    otb::string_view context,
    T default_value)
{
  if (kwl.HasKey(key))
  {
    auto const& value = kwl.GetMetadataByKey(key);
    return to<T>(value, context);
  }
  else
  {
    return default_value;
  }
}
#endif

#if OTB_VERSION_MAJOR >= 8
/**
 * Helper function to fetch data from keyword lists.
 * \tparam T  Type of the element to find
 * \param[in] kwl            Keyword list where to search for
 * \param[in] key            Name of the element to search and decode
 * \param[in] context        Context to help build error message
 *
 * \throw itk::ExceptionObject with the `context` as message if there is no
 * metadata associated to the `key`
 * \throw itk::ExceptionObject with the `context` as message if the metadata
 * found at the `key` cannot be converted to a `T`
 * \return the metadata found associated to the `key` decoded as a `T` type.
 * \todo move this function to `StringUtilities.h`
 */
template <typename T>
inline
T value_or_throw(
    otb::ImageMetadata const& kwl,
    std::string const& key,
    otb::string_view context)
{
  if (kwl.Has(key))
  {
    auto const& value = kwl[key];
    return to<T>(value, "converting metadata '"+key+"' "+context);
  }
  else
  {
    throw std::runtime_error("Cannot fetch metadata '"+key+"' " +context);
  }
}
#else
template <typename T>
inline
T value_or_throw(
    otb::ImageKeywordlist const& kwl,
    std::string const& key,
    otb::string_view context)
{
  if (kwl.HasKey(key))
  {
    auto const& value = kwl.GetMetadataByKey(key);
    return to<T>(value, context);
  }
  else
  {
    throw std::runtime_error("Cannot fetch metadata '"+ key + "' while " + context);
  }
}
#endif

}

// otb namespace


#endif // otbStringHelpers_h
