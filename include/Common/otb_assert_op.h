/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otb_assert_op_h
#define otb_assert_op_h

#if defined(NDEBUG)
#   define assert_op(a, op, b) ((void)0)
#else
/**
 * Helper macro to assert comparison checks and display the tested values.
 * @param a  first operand
 * @param op comparison operator used
 * @param b  second operand
 *
 * _asserts_ that `a {op} b`. If not, the completed failed test is sent to `std::cerr` and execution
 * is `std::abort`ed.
 *
 * \note Not defined when `NDEBUG` is set.
 * \todo Add a flavour that display the backtrace
 */
#   define assert_op(a, op, b) \
    if (!( a op b)) { \
      std::cerr << __FILE__ ":" << __LINE__ <<": assertion failed: " << a << " is not " #op " " << b << std::endl; \
      std::abort(); \
    }
#endif

#endif  // otb_assert_op_h
