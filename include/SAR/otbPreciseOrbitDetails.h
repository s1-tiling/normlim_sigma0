/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbPreciseOrbitDetails_h
#define otbPreciseOrbitDetails_h

#include "SARCalibrationExtendedExport.h"
#include "otbStringUtilities.h"
#include <cassert>

namespace otb
{
namespace details
{

struct orbit_conv {
  int offset;
  int modulo;

  constexpr int to_relative(int absolute) const {
    return (absolute - offset) % modulo + 1;
  }

  /**
   * Returns the first absolute orbit number greater that the reference `start_absolute` orbit
   * number that corresponds to the `target_relative` orbit number.
   * \post `result >= start_absolute`
   * \post `result - start_absolute < 175`
   * \post `to_relative(result) == target_relative`
   */
  constexpr int closest_absolute(int start_absolute, int target_relative) const {
    int const start_relative = to_relative(start_absolute);
    int       delta          = target_relative - start_relative;
    if (delta < 0)
      delta += modulo;
    auto res = start_absolute + delta;

    assert(res >= start_absolute);
    assert(res - start_absolute < modulo);
    assert(to_relative(res) == target_relative);
    return res;
  }
};

SARCalibrationExtended_EXPORT
orbit_conv get_orbit_converter(otb::string_view mission);

} } // otb::details namespaces

#endif  // otbPreciseOrbitDetails_h
