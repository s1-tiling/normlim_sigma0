/*
 * Copyright (C) 2005-2024 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbPreciseOrbit_h
#define otbPreciseOrbit_h

#include "SARCalibrationExtendedExport.h"
#include "otbSARMetadata.h"

#include <vector>
#include <string>

namespace otb
{

// <OSV>
//   <TAI>TAI=2023-12-31T23:00:19.000000</TAI>
//   <UTC>UTC=2023-12-31T22:59:42.000000</UTC>
//   <UT1>UT1=2023-12-31T22:59:42.008796</UT1>
//   <Absolute_Orbit>+51905</Absolute_Orbit>
//   <X unit="m">-253469.255758</X>
//   <Y unit="m">-2625698.144393</Y>
//   <Z unit="m">6555653.058485</Z>
//   <VX unit="m/s">-2423.475743</VX>
//   <VY unit="m/s">6705.522196</VY>
//   <VZ unit="m/s">2586.343409</VZ>
//   <Quality>NOMINAL</Quality>
// </OSV>

/**
 * Reads all Orbit State Vectors from Precise Orbit File.
 * \param[in] orbit_filename Name of the precise orbit file
 * \return list of OSVs
 * \throw std::runtime_error if the EOF file cannot be read
 * \throw std::runtime_error if the EOF file is incorrect
 * \todo Handle non EARTH_FIXED frames
 */
SARCalibrationExtended_EXPORT
std::vector<Orbit> read_precise_orbit(std::string const& orbit_filename);

struct OrbitInformation
{
  std::vector<otb::Orbit> orbit;
  std::size_t             first_idx;
  std::size_t             last_idx;
  int                     relative_orbit_number;
};

/**
 * Reads all Orbit State Vectors from Precise Orbit File matching an orbit
 * number.
 * \param[in] orbit_filename        Name of the precise orbit file
 * \param[in] relative_orbit_number Number of the only orbit to keep
 * \param[in] extra                 Extra number of orbits to take before/after
 *                                  the orbit found.
 *                                  Required for tiles near the equator (Z~0)
 * \return list of OSVs
 * \throw std::runtime_error if the EOF file cannot be read
 * \throw std::runtime_error if the EOF file is incorrect
 * \todo Handle non EARTH_FIXED frames
 */
SARCalibrationExtended_EXPORT
OrbitInformation read_precise_orbit(
    std::string const& orbit_filename,
    int                relative_orbit_number,
    int                extra = 1);

} // otb namespace

#endif  // otbPreciseOrbit_h
