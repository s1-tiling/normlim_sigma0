/*
 * Copyright (C) 2005-2023 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARAntenna_h
#define otbSARAntenna_h

#include "SARCalibrationExtendedExport.h"
#include "otbMacro.h"
// #include "ossim/ossimSarSensorModel.h"
// #define private public
#if OTB_VERSION_MAJOR >= 8
#include "otbSarSensorModel.h"
#else
#include "otbSarSensorModelAdapter.h"
#include "otbImageKeywordlist.h"
#endif
// #undef private

namespace otb
{

/** Generates a sensor model for the specified antenna.
 * \param[in] kwl      keyword list used to generate the model
 *
 * \return A SAR model for master/slave with the correct orbit
 * \throw itk::ExceptionObject if no orbit information for master or slave are
 * available in the keywordlist, or if `kwl` did not contain a valid SAR
 * product.
 *
 * We expect the orbit information for the antennas to be stored into
 * `"orbitList.{antenna_name}[{idx}].(pos|vel)_(x|y|z)"`
 *
 * \internal The parameter is taken by value to permit internally copy-elision
 * & move.
 */
SARCalibrationExtended_EXPORT
#if OTB_VERSION_MAJOR >= 8
std::unique_ptr<SarSensorModel> make_sensor_model(ImageMetadata const& kwl);
#else
SarSensorModelAdapter::Pointer make_sensor_model(ImageKeywordlist const& kwl);
#endif

using Point3DType = ::itk::Point<double,3>;

/**
 * Computes the distance between two 3D points.
 */
inline
auto distance(Point3DType const& p1, Point3DType const& p2)
{
  auto const dx = p1[0] - p2[0];
  auto const dy = p1[1] - p2[1];
  auto const dz = p1[2] - p2[2];
#if 1 || __cplusplus < 201703L
  return ::std::sqrt(dx*dx + dy*dy + dz*dz);
#else
  // In C++17, we should have been able to use std::hypot(x,y,z).
  // But: libstdc++ doesn't provide it, and libc++ resorts to the "naive"
  // sqrt() version.
  // Also, while hypot() has a better precision when overflow may happen, it's
  // also 3 times slower in the 2 parameters cases.
  // => Let's ignore it for now.
  return ::std::hypot(dx, dy, dz);
#endif
}

/** Interpolates the position of the antenna given a line index.
 * \param[in] model SAR model of the Antenna (/satellite...)
 * \param[in] line  (Azimuth) Line index in the coordinate referential of the
 *                  associated image.
 * \return The interpolated position.
 * \todo The interpolated velocity is dropped. This should be optimized
 * eventually.
 */
inline
#if OTB_VERSION_MAJOR >= 8
auto position(SarSensorModel const& model, double line)
#else
auto position(SarSensorModelAdapter const& model, double line)
#endif
{
  Point3DType pos, vel;

#if 0
  // Follows: the expanded code behind LineToSatPositionAndVelocity
  // It's kepts here for (now in case debugging is required.
  static bool logged = false;
  bool log_now = !logged;
  if(log_now)
    logged = true;

  auto & ossim_model = *model.m_SensorModel;

  ossimplugins::ossimSarSensorModel::TimeType azimuthTime;
  ossimEcefPoint sensorPos;
  ossimEcefVector sensorVel;

#if 1
  ossim_model.lineToAzimuthTime(line, azimuthTime);
#else
  using namespace ossimplugins;
#  if defined(USE_BOOST_TIME)
  using boost::posix_time::microseconds;
  using boost::posix_time::seconds;
  typedef boost::posix_time::ptime            TimeType;
  typedef boost::posix_time::precise_duration DurationType;
#  else
  using ossimplugins::time::microseconds;
  using ossimplugins::time::seconds;
  typedef time::ModifiedJulianDate TimeType;
  typedef time::Duration           DurationType;
#  endif

  auto const& currentBurst = ossim_model.theBurstRecords.begin();
  auto const& theAzimuthTimeInterval = ossim_model.theAzimuthTimeInterval;
  auto const& theAzimuthTimeOffset = ossim_model.theAzimuthTimeOffset;
  const DurationType timeSinceStart = (line - currentBurst->startLine)*theAzimuthTimeInterval;
  // std::clog << "timeSinceStart: " << timeSinceStart.total_microseconds() << "us\n";

  // Eq 22 p 27
  azimuthTime = currentBurst->azimuthStartTime + timeSinceStart + theAzimuthTimeOffset;

  if(log_now)
    std::cout << "burst-start(=" << currentBurst->azimuthStartTime
      << ") + timeSinceStart(="<<timeSinceStart
       << " = Δl("<<(line - currentBurst->startLine)<<") * interv(" << theAzimuthTimeInterval<<")"
      << ") + offset(=" << theAzimuthTimeOffset
      <<") = azimuthTime = " << azimuthTime << std::endl;
#endif
  if(log_now)
    std::cout << "\nline=" << line <<  " -> azimuthTime=" << azimuthTime << " ∈ ["<<ossim_model.theFirstLineTime << ", " << ossim_model.theLastLineTime <<"]\n"<<std::endl;
#if 1
  ossim_model.interpolateSensorPosVel(azimuthTime, sensorPos, sensorVel);
#else
  auto const  deg = 8;
  auto const& theOrbitRecords = ossim_model.theOrbitRecords;
  assert(!theOrbitRecords.empty()&&"The orbit records vector is empty");

  // Lagrangian interpolation of sensor position and velocity

  unsigned int nBegin(0), nEnd(0);

  sensorPos[0] = 0;
  sensorPos[1] = 0;
  sensorPos[2] = 0;

  sensorVel[0] = 0;
  sensorVel[1] = 0;
  sensorVel[2] = 0;

  // First, we search for the correct set of record to use during
  // interpolation

  // If there are less records than degrees, use them all
  if(theOrbitRecords.size()<deg)
  {
    nEnd = theOrbitRecords.size()-1;
  }
  else
  {
    // Search for the deg number of records around the azimuth time
    unsigned int t_min_idx = 0;
    DurationType t_min = abs(azimuthTime - theOrbitRecords.front().azimuthTime);

    unsigned int count = 0;

    for(auto it = theOrbitRecords.begin();it!=theOrbitRecords.end();++it,++count)
    {
      const DurationType current_time = abs(azimuthTime-it->azimuthTime);

      if(t_min > current_time)
      {
        t_min_idx = count;
        t_min = current_time;
      }
    }
    // TODO: see if these expressions can be simplified
    nBegin = std::max((int)t_min_idx-(int)deg/2+1,(int)0);
    nEnd = std::min(nBegin+deg-1,(unsigned int)theOrbitRecords.size());
    nBegin = nEnd<theOrbitRecords.size()-1 ? nBegin : nEnd-deg+1;
  }

    if(log_now)
      std::cout << "\nAzTime: " << azimuthTime << " ∈ "
        << "nBegin = " << nBegin << " ; nEnd = " << nEnd << "\n"<<std::endl;

  // Compute lagrangian interpolation using records from nBegin to nEnd
  for(unsigned int i = nBegin; i < nEnd; ++i)
  {
    double w = 1.;

    unsigned int j = nBegin;
    for( ; j != i ; ++j)
    {
      const DurationType td1 = azimuthTime                    - theOrbitRecords[j].azimuthTime;
      const DurationType td2 = theOrbitRecords[i].azimuthTime - theOrbitRecords[j].azimuthTime;
      const double f = td1 / td2;
      w *= f;
      if(log_now)

        std::cout << "f[i=" <<i<<", j="<<j<<"]=" << f << "=" << td1 << "/" << td2 << std::endl;
    }
    ++j;
    for( ; j < nEnd; ++j)
    {
      const DurationType td1 = azimuthTime                    - theOrbitRecords[j].azimuthTime;
      const DurationType td2 = theOrbitRecords[i].azimuthTime - theOrbitRecords[j].azimuthTime;
      const double f = td1 / td2;
      w *= f;
      if(log_now)
        std::cout << "f[i=" <<i<<", j="<<j<<"]=" << f << "=" << td1 << "/" << td2 << std::endl;
    }
    if(log_now)
      std::cout << "w["<< i << "] = " << w << std::endl;

    sensorPos[0]+=w*theOrbitRecords[i].position[0];
    sensorPos[1]+=w*theOrbitRecords[i].position[1];
    sensorPos[2]+=w*theOrbitRecords[i].position[2];

    sensorVel[0]+=w*theOrbitRecords[i].velocity[0];
    sensorVel[1]+=w*theOrbitRecords[i].velocity[1];
    sensorVel[2]+=w*theOrbitRecords[i].velocity[2];
  }
#endif
  pos[0] = sensorPos.x();
  pos[1] = sensorPos.y();
  pos[2] = sensorPos.z();
  return pos;
#else

  if(! model.LineToSatPositionAndVelocity(line, pos, vel))
  {
    throw std::runtime_error("Cannot interpolate!");
  }
  ::std::ignore = vel;
  return pos;
#endif
}

} // otb namespace


#endif  // otbSARAntenna_h
