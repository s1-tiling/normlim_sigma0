/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbFindOrbit_h
#define otbFindOrbit_h

#include "SARCalibrationExtendedExport.h"
#include "SAR/otbPreciseOrbit.h"
#include "Common/otbRepack.h"
#include "otbCoordinateTransformation.h"
#include "otbSARMetadata.h" // otb::Orbit
#include <array>

namespace otb
{

using soa_lonlathgt = std::array<std::array<double, 4>, 3>;
using aos_lonlathgt = std::array<otb::Orbit::PointType, 4>;

otb::CoordinateTransformation::CoordinateTransformationPtr
SARCalibrationExtended_EXPORT
make_transform_to_lonlat_for(std::string const& projection_ref);

template <typename HeightAccessorStrategy>
SARCalibrationExtended_EXPORT
aos_lonlathgt
GetImageCornersAsLatLon(
    double ulx, double uly,
    double lrx, double lry,
    std::string const& projection_ref,
    HeightAccessorStrategy get_height
)
{
 std::array<double, 4> xs = {};
 std::array<double, 4> ys = {};
 std::array<double, 4> hs = {};

 unsigned i = 0;
 for (auto x: {ulx, lrx})
   for (auto y : {uly, lry})
   {
     xs[i] = x;
     ys[i] = y;
     ++i;

     hs[i] = get_height(x, y);
   }

 auto transform = make_transform_to_lonlat_for(projection_ref);
 if (transform)
 {
   // static_assert(xs.size() == ys.size(), "Same size of 4 is expected");
#if GDAL_VERSION_NUM >= 3030000
// #   pragma message "GDAL >= 3.3 => Using TransformWithErrorCodes"
        transform->TransformWithErrorCodes(
            xs.size(),
            &xs[0], &ys[0], &hs[0], nullptr,
            nullptr);
#else
#   pragma message "GDAL < 3.3 => Using Transform"
        transform->Transform(
            xs.size(),
            &xs[0], &ys[0], &hs[0], nullptr,
            nullptr);
#endif
 }
 return {{
   otb::repack<otb::Orbit::PointType>(xs[0], ys[0], hs[0]),
   otb::repack<otb::Orbit::PointType>(xs[1], ys[1], hs[1]),
   otb::repack<otb::Orbit::PointType>(xs[2], ys[2], hs[2]),
   otb::repack<otb::Orbit::PointType>(xs[3], ys[3], hs[3]),
 }};
}

void
SARCalibrationExtended_EXPORT
FilterOrbit(otb::OrbitInformation & orbit_info, aos_lonlathgt const& corners_llh);

} // otb namespace

#endif  // otbFindOrbit_h
