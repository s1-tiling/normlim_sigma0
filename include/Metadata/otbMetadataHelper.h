/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbMetadataHelper_h
#define otbMetadataHelper_h

#include "otbNoDataHelper.h"
#if OTB_VERSION_MAJOR >= 8
#include "otbImageMetadata.h"
#else
#include "itkMetaDataDictionary.h"
#include "itkMetaDataObject.h"
#include "otbMetaDataKey.h"
#endif
#include <boost/outcome.hpp>
#include <algorithm>

namespace outcome = boost::outcome_v2;

namespace otb
{

/// Portable type to store metadatas (for OTB 7 & 8+)
#if OTB_VERSION_MAJOR >= 8
using PortableMetadataType = ImageMetadata;
#else
using PortableMetadataType = itk::MetaDataDictionary;
#endif

/**
 * Portable function (OTB 7 & 8+) to extract image metadata.
 * \tparam TImageType    Actual image type.
 * \param[in,out] image  image from which metadata will be extracted (usually
 *                       input image)
 * \return Returns the metadata object -- and forwards contance and references
 */
template <typename TImageType>
decltype(auto) GetImageMetadata(TImageType & image)
{
#if OTB_VERSION_MAJOR >= 8
  return image.GetImageMetadata();
#else
  // R/W In OTB 7 case
  return image.GetMetaDataDictionary();
#endif
}

/**
 * Portable function to write metadata information in an image.
 * \tparam TImageType  Actual image type
 * \param[in,out] image Image updated
 * \param[in]     meta  Metadata to write
 */
template <typename TImageType>
void SetImageMetadata(TImageType & image, PortableMetadataType meta)
{
#if OTB_VERSION_MAJOR >= 8
  return image.SetImageMetadata(std::move(meta));
#else
  return image.SetMetaDataDictionary(std::move(meta));
#endif
}

/**
 * Portable function (OTB 7 & 8+) to extract image keyword list.
 * - With OTB 7: `ImageKeywordlist`
 * - With OTB 8+: `ImageMetadata`
 * \tparam TImage    Actual image type
 * \param[in] image  image from which keywords will be extracted (usually input
 *                   image)
 * \return Returns the keywordlist/metadata object -- and forwards contance and
 * references
 */
template <typename TImage>
decltype(auto) GetImageKeywordlist(TImage const& image)
{
#if OTB_VERSION_MAJOR >= 8
  return image.GetImageMetadata();
#else
  return image.GetImageKeywordlist();
#endif
}

/**
 * Portable function (OTB 7 & 8+) to add keyword/metadata.
 * \tparam TMetadata  `ImageMetadata` / `ImageKeywordlist`
 * \param[in,out] kwl  KWL to update
 * \param[in] key      exact keyword name to add
 * \param[in] value    its value
 */
template <typename TMetadata, typename TKey, typename TValue>
void AddMetadata(
    TMetadata & kwl,
    TKey        const& key,
    TValue      const& value)
{
#if OTB_VERSION_MAJOR >= 8
  kwl.Add(key, value);
#else
  kwl.AddKey(key);
#endif
}

/**
 * Portable function (OTB 7 & 8+) to remove keyword/metadata.
 * \tparam TMetadata  `ImageMetadata` / `ImageKeywordlist`
 * \param[in,out] kwl  KWL to update
 * \param[in] metadata exact keyword name to remove
 */
template <typename TMetadata>
void RemoveMetadata(TMetadata & kwl, std::string const& metadata)
{
#if OTB_VERSION_MAJOR >= 8
  kwl.Remove(metadata);
#else
  kwl.ClearMetadataByKey(metadata);
#endif
}

/**
 * Portable function to extract novalue data from metadata
 * \return the nodata value found in the first band that has nodata
 * \return `NaN` when there is no meta data in the input image
 *
 * Unlike the `otb::ReadNoDataFlags`, this function considers all bands are
 * supposed to return the same metadata value. This matches the use cases
 * related to S1Tiling support applications that works on GeoTIFF images --
 * GeoTIFF images can't have different value for the nodata information in its
 * bands.
 */
inline
double
ExtractNoDataValue(PortableMetadataType const& meta)
{
  std::vector<bool> has_nodatas;
  std::vector<double> nodatas;
  if (! ReadNoDataFlags(meta, has_nodatas, nodatas))
  {
    otbLogMacro(Warning, << "Image has no 'nodata' metadata");
    return std::nan("Image has no 'nodata' metadata");
  }
  auto const it_first_nodata = std::find(
      has_nodatas.begin(), has_nodatas.end(),
      true);
  assert(it_first_nodata != has_nodatas.end());
  auto const idx_first_nodata = std::distance(has_nodatas.begin(), it_first_nodata);

  return nodatas[idx_first_nodata];
}

/**
 * Extract NoData Value from metadata.
 * \see `ExtractNoDataValue(ImageMetadata const&)`
 */
template <typename TImageType>
double ExtractNoDataValueFromImage(TImageType const& image)
{
  auto const& meta = GetImageMetadata(image);
  return ExtractNoDataValue(meta);
}


/** Helper class to complete Band nodata values incrementally.
 * This object is meant to be used from images `GenerateOutputInformation()`
 * functions.
 *
 * Band meta data will contain name and nodata value.
 */
class BandInformation
{
public:
  enum Bands { clear, append };

  /**
   * Constructor.
   * Initialize the band information filler object.
   *
   * \param[in,out] meta        Metadata to which the obj. shall be associated
   * \param[in] previous_bands  Tells whether all previous bands will be
   *                            removed or wheither we will append extra bands
   *                            to ones already declared.
   */
  BandInformation(PortableMetadataType & meta, Bands previous_bands)
  : m_meta(meta)
  {
#if OTB_VERSION_MAJOR >= 8
    if (previous_bands == BandInformation::clear)
    {
      m_meta.Bands.clear();
    }
#else
    (void) previous_bands;
    assert(previous_bands == BandInformation::clear && "Only clear is supported w/ OTB7");
#endif
  }

  /**
   * Register band information to eventually append.
   * Internal `m_meta` object is not yet altered. At the moment new information
   * are registered for later.
   *
   * To commit the changes, call `write_back()`.
   *
   * \note this peculiar usage is due to OTB 7 retro-compatibility.
   * \param[in] name   Name for a new band
   * \param[in] nodata No-data value for the new band
   */
  void add_new(std::string name, double nodata) {
    // m_info.emplace_back(std::move(name), nodata);
    m_info.push_back({std::move(name), nodata});
  }

  /** Commit band information into external metadata object.
   */
  void write_back()
  {
#if OTB_VERSION_MAJOR >= 8
    for (auto const& info : m_info)
    {
      ImageMetadataBase bmd;
      if (! info.name.empty())
      {
        bmd.Add(MDStr::BandName, info.name);
      }
      bmd.Add(MDNum::NoData,   info.nodata); // seems required only on the first band...
      m_meta.Bands.push_back(std::move(bmd));
    }
#else
    // Don't know how to set band names w/ OTB 7
    std::vector<bool>   flags(m_info.size(), true);
    std::vector<double> nodatas;
    nodatas.reserve(m_info.size());
    for (auto const& info : m_info)
    {
      nodatas.push_back(info.nodata);
    }
    itk::EncapsulateMetaData(m_meta, MetaDataKey::NoDataValueAvailable, flags);
    itk::EncapsulateMetaData(m_meta, MetaDataKey::NoDataValue,          nodatas);
#endif
  }
private:

  struct BandInfo {
    std::string name;
    double      nodata;
  };

  PortableMetadataType & m_meta;
  std::vector<BandInfo>  m_info;
};

} // otb namespace

#endif  // otbMetadataHelper_h
