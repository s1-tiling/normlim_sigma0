/*
 * Copyright (C) 2005-2024 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbApplicationWithNoData_h
#define otbApplicationWithNoData_h

#include "SARCalibrationExtendedExport.h"
#include "otbStringUtilities.h"
#include "otbWrapperApplication.h"

// #define OTB_ISSUE_2305_IS_FIXED

namespace otb
{

namespace Wrapper
{

/**
 * _Unary Type Traits_ that returns `ParameterType` value from a type.
 *
 * \todo This is a simplified version aimed at supporting all variations
 * related to "nodata" parameters. This needs to be moved to its own file.
 */
template <typename T> struct parameter_type {};

template <> struct parameter_type<int> {
  static constexpr ParameterType value = ParameterType_Int;

  static auto get(Application const& app, std::string const& key)
  {
    return app.GetParameterInt(key);
  }
  static void set_default(Application & app, std::string const& key, int value)
  {
    app.SetDefaultParameterInt(key, value);
  }
};

template <> struct parameter_type<float> {
  static constexpr ParameterType value = ParameterType_Float;

  static auto get(Application const& app, std::string const& key)
  {
    return app.GetParameterFloat(key);
  }
  static void set_default(Application & app, std::string const& key, float value)
  {
    app.SetDefaultParameterFloat(key, value);
  }
};

template <> struct parameter_type<double> {
  static constexpr ParameterType value = ParameterType_Double;

  static auto get(Application const& app, std::string const& key)
  {
    return app.GetParameterDouble(key);
  }
  static void set_default(Application & app, std::string const& key, double value)
  {
    app.SetDefaultParameterDouble(key, value);
  }
};

template <> struct parameter_type<std::string> {
  static constexpr ParameterType value = ParameterType_String;

  static auto get(Application const& app, std::string const& key)
  {
    return app.GetParameterString(key);
  }
#if 0
  static void set_default(Application & app, std::string const& key, std::string value)
  {
    app.SetDefaultParameterString(key, value);
  }
#endif
};

/** Helper variable template that returns `ParameterType` value from a type. */
template <typename T>
constexpr ParameterType parameter_type_v = parameter_type<T>::value;


/**
 * Helper parent class for applications that need to inject `&nodata=` in metadata.
 *
 * This class provides two services:
 * - `DoInit_NoData()` meant to be called from `DoInit()` specialisation.
 * - `AddNodataInMetadataThroughExtendedFilename()` meant to be called from
 *   `DoExecute()` specialisation.
 *
 * \author Luc Hermitte
 * \copyright CS Group
 * \ingroup App???
 * \ingroup SARCalibrationExtended
 */
template <typename TNoData, typename TApplication = Application>
class SARCalibrationExtended_EXPORT ApplicationWithNoData : public TApplication
{
protected:
  ApplicationWithNoData(
      std::string nodata_key           = "nodata",
      TNoData     nodata_default_value = -32768)
    : m_nodata_key(std::move(nodata_key))
    , m_nodata_default_value(nodata_default_value)
    {}

  /**
   * Registers a `-nodata` parameter.
   */
  void DoInit_NoData()
  {
#if defined(OTB_ISSUE_2305_IS_FIXED)
    // The following would be OK in OTB9 w/ #2305 fixed
    this->AddParameter(
        parameter_type_v<TNoData>,
        m_nodata_key,
        "NoData value");
    parameter_type<TNoData>::set_default(
        *this,
        m_nodata_key,
        m_nodata_default_value);
#else
    // In the mean time...
    this->AddParameter(ParameterType_String, m_nodata_key, "NoData value");
#endif
    this->SetParameterDescription(
        m_nodata_key,
        "Ouput cells with no data are filled with this value (optional "+std::to_string(m_nodata_default_value)+" by default)");
    this->MandatoryOff(m_nodata_key);
  }

  TNoData GetParameterNodata() const
  {
#if defined(OTB_ISSUE_2305_IS_FIXED)
    return parameter_type<TNoData>::get(*this, m_nodata_key);
#else
    std::string svalue = this->GetParameterString(m_nodata_key);
    return svalue.empty()
      ? m_nodata_default_value
      : otb::to<TNoData>(svalue, "extracting nodata value");
#endif
  }

  /**
   * Appends `&nodata={value}` at the end of the extended filename to force
   * nodata to be added in metadata.
   */
  void AddNodataInMetadataThroughExtendedFilename(
      TNoData            nodata,
      std::string const& out_param = "out")
  {
    auto const origin_FileName = this->GetParameterString(out_param);
    std::ostringstream oss;
    oss << origin_FileName;

    // Check if FileName is extended (with the ? caracter)
    // If not extended then override the FileName
    auto const extension_start = origin_FileName.find('?');
    if (extension_start == std::string::npos && !origin_FileName.empty())
      oss << '?';
    else
    {
      auto const nodata_start = origin_FileName.find("&nodata=");
      if (nodata_start != std::string::npos && nodata_start > extension_start)
      {
        // Let's trust the end-user. Even if the value doesn't match
        otbLogMacro(Warning, <<"Trusting the nodata value required in extended filename. User specified " << nodata << " value won't be propagated in image metadata");
        return ;
      }
    }
    oss << "&nodata="<<nodata;
    // Set the new FileName with extended options
    this->SetParameterString(out_param, oss.str());
  }

private:
  std::string m_nodata_key;
  TNoData     m_nodata_default_value = nan;
};

} //end namespace Wrapper

} //end namespace otb

#endif  // otbApplicationWithNoData_h
