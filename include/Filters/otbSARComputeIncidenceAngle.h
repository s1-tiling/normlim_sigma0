/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARComputeIncidenceAngle_h
#define otbSARComputeIncidenceAngle_h

#include "SARCalibrationExtendedExport.h"
#include "SAR/otbSARAntenna.h" // ImageKeywordlist/ImageMetadata/OTB_VERSION_MAJOR
#include "Common/otbNewMacro.h"
#include "itkImageToImageFilter.h"
#include <boost/optional.hpp>
#include <vector>

namespace otb
{

namespace details
{

template <typename ImageType>
inline
auto image_builder()
{
  return ImageType::New();
}

} // details namespace

/**
 * SAR Filter that produces Local Incidence Angles from XYZ coordinates,
 * surface normal at the point, and sensor trajectory.
 *
 * \tparam TInputImage  Type of inputs images (XYZ, and normals) (vector
 * images)
 * \tparam TOutputImage Type of output images (vector image)
 */
template <typename TInputImage, typename TOutputImage, typename TOutputDegImage>
class SARCalibrationExtended_EXPORT SARComputeIncidenceAngle
: public itk::ImageToImageFilter<TInputImage,TOutputImage>
{
public:

  /**\name Standard class typedefs */
  //@{
  using Self         = SARComputeIncidenceAngle;
  using Superclass   = itk::ImageToImageFilter<TInputImage,TOutputImage>;
  using Pointer      = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;
  //@}

  /** Method for creation through the object factory. */
  otbNewMacro(Self);
  /** Run-time type information (and related methods). */
  itkTypeMacro(SARComputeIncidenceAngle, unused);

  /**\name Image typedef support */
  //@{
  using InputImageType         = typename Superclass::InputImageType;
  using InputImagePointer      = typename Superclass::InputImagePointer;
  using InputImageConstPointer = typename Superclass::InputImageConstPointer;

  using RegionType             = typename Superclass::InputImageRegionType;
  using IndexType              = typename RegionType::IndexType;
  using SizeType               = typename RegionType::SizeType;

  using InputPixelType         = typename InputImageType::PixelType;

  using OutputImageType        = typename Superclass::OutputImageType;
  using OutputImagePointer     = typename Superclass::OutputImagePointer;

  using OutputDegImageType     = TOutputDegImage;
  using OutputSinImageType     = OutputImageType;
  using OutputCosImageType     = OutputImageType;
  using OutputTanImageType     = OutputImageType;

  /// For all float outputs (sin, cos, tan)
  using OutputPixelType        = typename OutputImageType::PixelType;
  using OutputDegPixelType     = typename OutputDegImageType::PixelType;
  using PointType              = typename InputImageType::PointType;

  // Because trajectories are represented with double, this type will be use
  // for internal computations, independently of pixel types.
  using InternalRealType       = double;
  using VNLPoint3DType         = vnl_vector_fixed<InternalRealType, 3>;


  static_assert(std::is_same<RegionType,typename Superclass::OutputImageRegionType>::value,
      "Expect both images to have the same kind of regions");
  static_assert(std::is_same<PointType,typename Superclass::OutputImageType::PointType>::value,
      "Expect both images to have the same kind of points");

  static auto const ImageDimension = InputImageType::ImageDimension;
  static_assert(InputImageType ::ImageDimension == 2, "SAR => 2D");
  static_assert(OutputImageType::ImageDimension == 2, "SAR => 2D");

#if 0
  using ImageIndexType        = typename ImageType::IndexType;
  using ImageIndexValueType   = typename ImageType::IndexValueType;

  using ImageSizeType         = typename ImageType::SizeType;
  using ImageSizeValueType    = typename ImageType::SizeValueType;

  using ConstIteratorType     = typename itk::ImageRegionConstIterator<ImageType>;
  using IteratorType          = typename itk::ImageRegionIterator<ImageType>;
#endif
  //@}

  /**\name Accessors for XYZ input */
  //@{
  void SetXYZInputImage(InputImageType const* xyz) {
    assert(xyz);
    this->SetNthInput(unsigned(idx_image::xyz), const_cast<InputImageType*>(xyz));
  }
  InputImageType const* GetXYZInputImage() const {
    return this->GetInputImage(idx_image::xyz);
  }
  //@}

  /**\name Accessors for normal input */
  //@{
  void SetNormalInputImage(InputImageType const* normals) {
    assert(normals);
    this->SetNthInput(unsigned(idx_image::normals), const_cast<InputImageType*>(normals));
  }
  InputImageType const* GetNormalInputImage() const
  {
    return this->GetInputImage(idx_image::normals);
  }
  //@}

  /**\name Accessors for output images */
  //@{
  OutputSinImageType * GetSinOutputImage()
  {
    return m_sin_map_index
      ? static_cast<OutputSinImageType*>(this->itk::ProcessObject::GetOutput(*m_sin_map_index))
      : nullptr;
  }
  OutputCosImageType * GetCosOutputImage()
  {
    return m_cos_map_index
      ? static_cast<OutputCosImageType*>(this->itk::ProcessObject::GetOutput(*m_cos_map_index))
      : nullptr;
  }
  OutputTanImageType * GetTanOutputImage()
  {
    return m_tan_map_index
      ? static_cast<OutputTanImageType*>(this->itk::ProcessObject::GetOutput(*m_tan_map_index))
      : nullptr;
  }
  OutputDegImageType * GetDegOutputImage()
  {
    return m_deg_map_index
      ? static_cast<OutputDegImageType*>(this->itk::ProcessObject::GetOutput(*m_deg_map_index))
      : nullptr;
  }
  //@}

  /** NoData setter. */
  void SetNoData(double no_data) noexcept
  { m_ia_nodata = no_data; }

protected:
  SARComputeIncidenceAngle(
      bool produce_deg_map,
      bool produce_sin_map,
      bool produce_cos_map,
      bool produce_tan_map
  );

  ~SARComputeIncidenceAngle() noexcept = default;

  void AllocateOutputs() override;

  // void BeforeThreadedGenerateData() override;
  // void AfterThreadedGenerateData() override;
  void ThreadedGenerateData(
      RegionType const& outputRegionForThread,
      itk::ThreadIdType threadId) override;

  void GenerateOutputInformation() override;

private:
  enum class idx_image { xyz, normals };

  /// Helper function to fetch input images
  InputImageType const* GetInputImage(idx_image idx) const
  {
    assert(this->GetNumberOfInputs() == 2);
    if (this->GetNumberOfInputs() < 2)
    {
      return nullptr;
    }
    return static_cast<InputImageType const*>(
        this->::itk::ProcessObject::GetInput(unsigned(idx)));
  }

#if defined(DRAFT_OTB8) || OTB_VERSION_MAJOR >= 8
  std::vector<VNLPoint3DType> InterpolatedAntennaPositions(ImageMetadata const& kwl) const;
#else
  std::vector<VNLPoint3DType> InterpolatedAntennaPositions(ImageKeywordlist const& kwl) const;
#endif

  std::vector<VNLPoint3DType> m_AntennaPositions;

  // Optional indices of the related images in the filter output image list
  // -> no value means: the associated image is not produced
  boost::optional<int>     m_sin_map_index;
  boost::optional<int>     m_cos_map_index;
  boost::optional<int>     m_tan_map_index;
  boost::optional<int>     m_deg_map_index;

  unsigned int             m_XBand  = 0;
  unsigned int             m_YBand  = 1;
  unsigned int             m_ZBand  = 2;
  unsigned int             m_nXBand = 0;
  unsigned int             m_nYBand = 1;
  unsigned int             m_nZBand = 2;
  unsigned int             m_sXBand = -1;
  unsigned int             m_sYBand = -1;
  unsigned int             m_sZBand = -1;
  double                   m_ia_nodata  = -32768;
  double                   m_xyz_nodata = -32768;
  double                   m_nrm_nodata = -32768;
};

} // otb namespace

#ifndef OTB_MANUAL_INSTANTIATION
#include "Filters/otbSARComputeIncidenceAngle.hxx"
#endif

#endif  // otbSARComputeIncidenceAngle_h
