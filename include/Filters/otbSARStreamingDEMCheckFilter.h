/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingDEMCheckFilter_h
#define otbSARStreamingDEMCheckFilter_h

#include "SARCalibrationExtendedExport.h"
#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "itkPoint.h"

#include "otbPersistentFilterStreamingDecorator.h"

namespace otb
{

/** \class PersistentDEMCheckFilter
 * \brief Retrive some pixels from the input DEM
 *
 *  This filter persists its temporary data. It means that if you Update it n times on n different
 * requested regions, the output statistics will be the statitics of the whole set of n regions.
 *
 * To reset the temporary data, one should call the Reset() function.
 *
 * To get the wanted values once the regions have been processed via the pipeline, use the Synthetize() method.
 *
 * \ingroup DiapOTBModule
 */
template<class TInputImage>
class SARCalibrationExtended_EXPORT PersistentDEMCheckFilter
: public PersistentImageFilter<TInputImage, TInputImage>
{
public:
  /** Standard Self typedef */
  using Self                           = PersistentDEMCheckFilter;
  using Superclass                     = PersistentImageFilter<TInputImage, TInputImage>;
  using Pointer                        = itk::SmartPointer<Self>;
  using ConstPointer                   = itk::SmartPointer<const Self>;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentDEMCheckFilter, PersistentImageFilter);

  /** Image related typedefs. */
  using ImageType                      = TInputImage;
  using InputImagePointer              = typename TInputImage::Pointer;

  using RegionType                     = typename TInputImage::RegionType;
  using SizeType                       = typename TInputImage::SizeType;
  using IndexType                      = typename TInputImage::IndexType;
  using PixelType                      = typename TInputImage::PixelType;
  using IndexValueType                 = typename ImageType::IndexValueType;
  using SizeValueType                  = typename ImageType::SizeValueType;

  itkStaticConstMacro(InputImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Type to use for computations. */
  //  using RealType = typename itk::NumericTraits<PixelType>::RealType;
  using RealType                       = typename itk::NumericTraits<double>::RealType;

  /** Smart Pointer type to a DataObject. */
  using DataObjectPointer              = typename itk::DataObject::Pointer;
  using DataObjectPointerArraySizeType = itk::ProcessObject::DataObjectPointerArraySizeType;

  /** Type of DataObjects used for scalar outputs */
  using RealObjectType                 = itk::SimpleDataObjectDecorator<RealType>;
  using LongObjectType                 = itk::SimpleDataObjectDecorator<long int>;
  using PixelObjectType                = itk::SimpleDataObjectDecorator<PixelType>;

  // Set SAR Image Size and Origin
  void setSARSizeAndOrigin(int sizeC, int sizeL, int originC, int originL)
  {
    m_SizeSARC = sizeC;
    m_SizeSARL = sizeL;
    m_OriginSARC = originC;
    m_OriginSARL = originL;
  }

  /** Return the counter of DEM points inside SAR Image. */
  long int GetCounter() const
  {
    return this->GetCounter_Output()->Get();
  }
  LongObjectType* GetCounter_Output();
  const LongObjectType* GetCounter_Output() const;

  /** Make a DataObject of the correct type to be used as the specified
   * output. */
  DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx) override;
  using Superclass::MakeOutput;

  /**
   * Synthetize and Reset function called by our PersistentFilterStreamingDecorator
   */
  void Synthetize(void) override;
  void Reset(void) override;

  itkSetMacro(UserIgnoredValue, RealType);
  itkGetMacro(UserIgnoredValue, RealType);

protected:
  PersistentDEMCheckFilter();
  ~PersistentDEMCheckFilter() override = default;


  /** PersistentDEMCheckFilter needs a input requested region with the same size of
      the output requested region.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  void GenerateInputRequestedRegion() override;

  /** Multi-thread version GenerateData. */
  void  ThreadedGenerateData(const RegionType&
                             outputRegionForThread,
                             itk::ThreadIdType threadId) override;

  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  void AllocateOutputs() override;
  void GenerateOutputInformation() override;

private:
  PersistentDEMCheckFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  itk::Array<long int>       m_Thread_Counter;

  /* Ignored values */
  RealType                   m_UserIgnoredValue;

  // Main characterics for SAR Image (Origin and Size)
  int m_SizeSARC;
  int m_SizeSARL;
  int m_OriginSARC;
  int m_OriginSARL;
}; // end of class PersistentDEMCheckFilter

/*===========================================================================*/

/** \class SARStreamingDEMCheckFilter
 * \brief This class streams the whole input image through the PersistentDEMCheckFilter.
 *
 * This way, it allows computing the first order global statistics of this image. It calls the
 * Reset() method of the PersistentDEMCheckFilter before streaming the image and the
 * Synthetize() method of the PersistentDEMCheckFilter after having streamed the image
 * to compute the statistics. The accessor on the results are wrapping the accessors of the
 * internal PersistentDEMCheckFilter.
 * By default infinite values are ignored, use IgnoreInfiniteValues accessor to consider
 * infinite values in the computation.
 *
 * This filter can be used as:
 * \code
 * typedef otb::StreamingDEMCheckFilter<ImageType> StatisticsType;
 * StatisticsType::Pointer statistics = StatisticsType::New();
 * statistics->SetInput(reader->GetOutput());
 * statistics->setSARSizeAndOrigin(sizeC, sizeL, originC, originL);
 * statistics->Update();
 * long int nbDEMPts_into_SAR = 0;
 * statistics->GetDEMCheck(nbDEMPts_into_SAR);
 * std::cout << nbDEMPts_into_SAR << std::endl;
 * \endcode
 *
 * \sa PersistentDEMCheckFilter
 * \sa PersistentImageFilter
 * \sa PersistentFilterStreamingDecorator
 * \sa StreamingImageVirtualWriter
 * \ingroup Streamed
 * \ingroup Multithreaded
 * \ingroup MathematicalStatisticsImageFilters
 *
 * \ingroup DiapOTBModule
 */

template<class TInputImage>
class SARCalibrationExtended_EXPORT SARStreamingDEMCheckFilter :
  public PersistentFilterStreamingDecorator<PersistentDEMCheckFilter<TInputImage> >
{
public:
  /** Standard Self typedef */
  typedef SARStreamingDEMCheckFilter Self;
  typedef PersistentFilterStreamingDecorator
  <PersistentDEMCheckFilter<TInputImage> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingDEMCheckFilter, PersistentFilterStreamingDecorator);

  typedef typename Superclass::FilterType    StatFilterType;
  typedef typename StatFilterType::PixelType PixelType;
  typedef typename StatFilterType::RealType  RealType;
  typedef TInputImage                        InputImageType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long int>  LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;

  /** Image related typedefs. */
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType RegionType;
  typedef typename TInputImage::SizeType   SizeType;
  typedef typename TInputImage::IndexType  IndexType;

   // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  using Superclass::SetInput;
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

   void setSARSizeAndOrigin(int sizeC, int sizeL, int originC, int originL)
   {
     this->GetFilter()->setSARSizeAndOrigin(sizeC, sizeL, originC, originL);
   }

  /** Return the counter. */
  long int GetCounter() const
  {
    return this->GetFilter()->GetCounter_Output()->Get();
  }
  LongObjectType* GetCounter_Output()
  {
    return this->GetFilter()->GetCounter_Output();
  }
  const LongObjectType* GetCounter_Output() const
  {
    return this->GetFilter()->GetCounter_Output();
  }

  // Get some information abour DEM (number of DEM points with a projection into SAR geometry)
  void GetDEMCheck(long int & nbDEMPts_into_SAR)
  {
    nbDEMPts_into_SAR = this->GetCounter();
  }

  otbSetObjectMemberMacro(Filter, UserIgnoredValue, RealType);
  otbGetObjectMemberMacro(Filter, UserIgnoredValue, RealType);

protected:
  /** Constructor */
  SARStreamingDEMCheckFilter() = default;
  /** Destructor */
  ~SARStreamingDEMCheckFilter() override = default;

private:
  SARStreamingDEMCheckFilter(const Self &) = delete;
  void operator =(const Self&) = delete;

};

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "Filters/otbSARStreamingDEMCheckFilter.hxx"
#endif

#endif
