/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARComputeIncidenceAngle_hxx
#define otbSARComputeIncidenceAngle_hxx

#include "Filters/otbSARComputeIncidenceAngle.h"
#include "Common/otb_assert_op.h"
#include "otbMacro.h"
#include "Metadata/otbMetadataHelper.h"
#include "Filters/otbBandIds.h"
#include "Common/otbRepack.h"
#include "Common/otbAlgoErase.h"
#include "ITKHelpers/otbTupleOptionalIterator.h"
// #include "otbSARAntenna.h" // already included in .h
#include "ITKHelpers/otbITKIteratorHelpers.h"
#include "Common/otbStringHelpers.h"
#include "otbMath.h"
#include "otbZipIterator.h"
// #include "otbLogHelpers.h"  // NeatRegionLogger
#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include <vnl/vnl_vector_fixed.h>
#include <vnl/vnl_cross.h>
#include <cmath>

#if OTB_VERSION_MAJOR >= 8
#include "otbAlgoClamp.h"
#else
namespace otb
{
template <typename T>
constexpr
T const& clamp(T const& v, T const& lo, T const& hi) noexcept
{
  assert( lo <= hi );
  return (v < lo) ? lo
       : (hi < v) ? hi
       :            v;
}
} // namespace otb
#endif

namespace
{ // anonymous namespace
/// Multiplicative constant to convert from radians to 100xdegrees
template <typename T>
constexpr T _18000_PI = 100. * otb::CONST_180_PI;

/**
 * Normalizes a 3d vector.
 * \tparam IRT  Internal real type
 * \param[in] v  input vector
 * \return The vector, normalized
 * \post `result.two_norm() == 1`
 * \throw None
 */
template <typename IRT>
inline
auto normalize(vnl_vector_fixed<IRT, 3> const& v)
{
  auto const norm = v.two_norm();
  assert(norm != IRT(0.));
  return v * (IRT(1.0) / norm);
}

/**
 * Computes the angle between two vectors and their sine.
 * It's done by computing the cross product `v1 ^ v2` which gives their sine (when divided by the
 * dot product `v1 . v2`. The angle is then obtained by `π - sine`
 * \tparam Vector3DType  Works with any vector type that defines `two_norm()`
 * \param[in] v1  1st 3d vector
 * \param[in] v2  2nd 3d vector
 * \return {sine, angle}
 * \throw None
 */
template <typename Vector3DType>
inline
auto precise_angle(Vector3DType const& v1, Vector3DType const& v2)
{
  // expect both vectors to be unit vectors
  auto const norm_product = v1.two_norm() * v2.two_norm();
  auto const dot = dot_product(v1, v2);
  using RealType = decltype(dot);
  auto const threshold = norm_product * RealType(0.9999);
  if ((dot < -threshold) || (dot > threshold)) {
    // the vectors are almost aligned, compute using the sine
    auto const v3 = vnl_cross_3d(v1, v2);
    // clamp is used in case of incorrect roundings
    auto const sine = otb::clamp<RealType>(v3.two_norm() / norm_product, -1.0, +1.0);
    if (dot >= RealType(0.)) {
      return std::make_pair(sine, std::asin(sine));
    }
    return std::make_pair(sine, otb::CONST_PI - std::asin(sine));
  }

  // the vectors are sufficiently separated to use the cosine
  auto const angle = std::acos(otb::clamp<RealType>(dot / norm_product, -1.0, +1.0));
  return std::make_pair(std::sin(angle), angle);
}

/**
 * Fabricates a `optional<otb::ZipIterator<>>` on an image list given a region.
 * In the image list is empty, an empty optional is returned.
 * \tparam ImageType  auto-detected type of the images
 * \tparam RegionType auto-detected image region type
 * \param[in] images  list of images for which a zip iterator will produced
 * \param[in] region  region onto which the terators apply
 *
 * \return an optional zip iterator over the images, when there are images
 * \return an empty optional, when the image list is empty
 * \throw None
 * \pre all images shall share the same spatial references, footprint...
 */
template <typename ImageType, typename RegionType>
inline
auto make_zip(std::vector<ImageType* > const& images, RegionType const& region)
{
  // otbLogMacro(Info, << "make_zip(#" << images.size() << ")");
  using ZipIt = otb::ZipIterator<itk::ImageScanlineIterator<ImageType>>;
  return images.empty()
    ? boost::optional<ZipIt>{}
    : boost::optional<ZipIt>(boost::in_place_init, images, region);
}
} // anonymous namespace


template<typename TInputImage,typename TOutputImage,typename TOutputDegImage>
inline
otb::SARComputeIncidenceAngle<TInputImage,TOutputImage,TOutputDegImage>
::SARComputeIncidenceAngle(
    bool produce_deg_map,
    bool produce_sin_map,
    bool produce_cos_map,
    bool produce_tan_map)
{
  auto to_int = [](bool b)
  {
    return b ? 1 : 0;
  };
  auto const nb_outputs
    = to_int(produce_deg_map)
    + to_int(produce_sin_map)
    + to_int(produce_cos_map)
    + to_int(produce_tan_map);

  this->SetNumberOfRequiredInputs(2);
  this->SetNumberOfIndexedInputs(2);
  this->SetNumberOfRequiredOutputs(nb_outputs);

  int next_index = 0;
  auto declare_output
    = [self=this,&next_index](bool produce_map, auto new_image)
    -> boost::optional<int>
    {
      if (produce_map)
      {
        auto index = next_index++;
        self->SetNthOutput(index, new_image());
        return boost::optional<int>{index};
      }
      return
      {
      };
    };
  m_sin_map_index = declare_output(produce_sin_map, details::image_builder<OutputSinImageType>);
  m_cos_map_index = declare_output(produce_cos_map, details::image_builder<OutputCosImageType>);
  m_tan_map_index = declare_output(produce_tan_map, details::image_builder<OutputTanImageType>);
  m_deg_map_index = declare_output(produce_deg_map, details::image_builder<OutputDegImageType>);
}

template<typename TInputImage,typename TOutputImage,typename TOutputDegImage>
inline
void
otb::SARComputeIncidenceAngle<TInputImage,TOutputImage,TOutputDegImage>
::AllocateOutputs()
{
  // Completely override this function as parent's implementation assumes OutOutputImageType on all
  // outputs

  auto allocate_image = [](auto * image)
  {
    assert(image);
    image->SetBufferedRegion(image->GetRequestedRegion());
    image->Allocate();
  };

  if (m_deg_map_index) allocate_image(this->GetDegOutputImage());
  if (m_sin_map_index) allocate_image(this->GetSinOutputImage());
  if (m_cos_map_index) allocate_image(this->GetCosOutputImage());
  if (m_tan_map_index) allocate_image(this->GetTanOutputImage());
}

template <typename TInputImage, typename TOutputImage, typename TOutputDegImage>
inline
void
otb::SARComputeIncidenceAngle<TInputImage,TOutputImage, TOutputDegImage>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  // output->SetVectorLength(2);

  // ====[ Fetch bands for ground point XYZ coordinates and satellite position
  //        and clear them
  auto xyzImage = this->GetXYZInputImage();
  assert(xyzImage);

  auto meta = GetImageMetadata(*xyzImage);
#if OTB_VERSION_MAJOR >= 8
  auto & kwl = meta;
#else
  auto kwl = xyzImage->GetImageKeywordlist();
#endif

  m_xyz_nodata = ExtractNoDataValue(meta); // needs to be fetched before alterations are done

  m_XBand = value_or_unless(kwl, bands::k_id_X, "extracting number of X band", 0);
  m_YBand = value_or_unless(kwl, bands::k_id_Y, "extracting number of Y band", 1);
  m_ZBand = value_or_unless(kwl, bands::k_id_Z, "extracting number of Z band", 2);
  otbLogMacro(Info, <<"Using XYZ cartesian coordinates in bands " << m_XBand << ", " << m_YBand << ", " << m_ZBand);

  int sX = value_or_unless(kwl, bands::k_id_SatX, "extracting number of Satpos X band", -1);
  if (sX == -1)
  {
    otbLogMacro(Info, << "Reading orbit in inxyz file metadata");
    if (m_AntennaPositions.empty()) // no need to do it several times!
    {
      // Let's expect the trajectory in the xyzImage, for now...
      m_AntennaPositions = InterpolatedAntennaPositions(kwl);
    }
  }
  else
  {
    otbLogMacro(Info, << "Reading satellite positions from 3 bands in inxyz");
    m_sXBand = sX;
    m_sYBand = value_or_throw<unsigned int>(kwl, bands::k_id_SatY, "extracting number of SatPos Y band");
    m_sZBand = value_or_throw<unsigned int>(kwl, bands::k_id_SatZ, "extracting number of SatPos Z band");
    otbLogMacro(Info, <<"Using satpos XYZ cartesian coordinates in bands " << m_sXBand << ", " << m_sYBand << ", " << m_sZBand);
  }

  // Remove meta data from SARDEMProjection2
#if OTB_VERSION_MAJOR < 8
  RemoveMetadata(kwl, bands::k_id_X);
  RemoveMetadata(kwl, bands::k_id_Y);
  RemoveMetadata(kwl, bands::k_id_Z);
  RemoveMetadata(kwl, bands::k_id_SatX);
  RemoveMetadata(kwl, bands::k_id_SatY);
  RemoveMetadata(kwl, bands::k_id_SatZ);
#else
  erase_if(
      meta.ExtraKeys,
      [](auto const& kv){
      return starts_with(kv.first, bands::k_prefix_band)
      ||     starts_with(kv.first, bands::k_prefix_scandir)
      ||     kv.first == "Gain";
      });
#endif

  /// λ that sets image geoTIFF metadata if the image is actually produced.
  auto set_metadata = [&](auto * output, std::string name)
  {
    if (output)
    {
      BandInformation bands(meta, BandInformation::clear);
      bands.add_new(std::move(name), m_ia_nodata);
      bands.write_back();
      SetImageMetadata(*output, meta);
#if OTB_VERSION_MAJOR < 8
      output->SetImageKeywordList(kwl);
#endif
    }
  };

  set_metadata(this->GetDegOutputImage(), "100 * degree(IA)");
  set_metadata(this->GetSinOutputImage(), "sin(IA)");
  set_metadata(this->GetCosOutputImage(), "cos(IA)");
  set_metadata(this->GetTanOutputImage(), "tan(IA)");

  // ====[ Fetch bands for normals X, Y, Z
  auto normalImage = this->GetNormalInputImage();
  assert(normalImage);

  auto const& kwlNormal = GetImageKeywordlist(*normalImage);
  m_nrm_nodata   = ExtractNoDataValueFromImage(*normalImage);
  m_nXBand = value_or_unless(kwlNormal, bands::k_id_nX, "extracting number of nX band", 0);
  m_nYBand = value_or_unless(kwlNormal, bands::k_id_nY, "extracting number of nY band", 1);
  m_nZBand = value_or_unless(kwlNormal, bands::k_id_nZ, "extracting number of nZ band", 2);
  otbLogMacro(Info, <<"Using ground normals from bands " << m_nXBand << ", " << m_nYBand << ", " << m_nZBand);

  // ====[ Log About no data values
  otbLogMacro(Info, << "NoData value in input XYZ grd/sat image: " << m_xyz_nodata);
  otbLogMacro(Info, << "NoData value in input normals image:     " << m_nrm_nodata);
  otbLogMacro(Info, << "NoData value in output IA images:        " << m_ia_nodata);
}

template <typename TInputImage, typename TOutputImage, typename TOutputDegImage>
inline
std::vector<typename otb::SARComputeIncidenceAngle<TInputImage, TOutputImage, TOutputDegImage>::VNLPoint3DType>
otb::SARComputeIncidenceAngle<TInputImage, TOutputImage, TOutputDegImage>
#if OTB_VERSION_MAJOR >= 8
::InterpolatedAntennaPositions(ImageMetadata const& kwl) const
#else
::InterpolatedAntennaPositions(ImageKeywordlist const& kwl) const
#endif
{
  // Workaround just in case, force support_data.product_type to SLC
  // kwl.AddKey("support_data.product_type", "SLC");
  auto antennaSSM = make_sensor_model(kwl);
  // antennaSSM->m_SensorModel->print(std::cout);

  auto const* inputPtr = this->GetXYZInputImage();
  assert(inputPtr);

  auto const& region = inputPtr->GetLargestPossibleRegion();
  auto const& size   = region.GetSize();
  auto const  number_of_lines = size[1];
  std::vector<VNLPoint3DType> res;
  res.reserve(number_of_lines);

  for (double line = 0.0; line < number_of_lines; ++line)
  {
    // Note: ITK SmartPointer::operator* doesn't return a lvalue reference...
    // Hence the convoluted `*shr.GetPointer()`...
#if OTB_VERSION_MAJOR >= 8
    auto const pos = repack<VNLPoint3DType>(position(*antennaSSM, line));
#else
    auto const pos = repack<VNLPoint3DType>(position(*antennaSSM.GetPointer(), line));
#endif
    res.push_back(pos);
  }
  return res;
}

template <typename TInputImage, typename TOutputImage, typename TOutputDegImage>
inline
void
otb::SARComputeIncidenceAngle<TInputImage,TOutputImage, TOutputDegImage>
::ThreadedGenerateData(
    RegionType const& outputRegionForThread,
    itk::ThreadIdType threadId)
{
  // -----[ Input access helpers
  using V3 = vnl_vector_fixed<InternalRealType, 3>;

  /// λ that extracts "ground"/surface XYZ coordinates from a bigger pixel
  auto const grdXYZ = [xband=m_XBand, yband=m_YBand, zband=m_ZBand](auto const& pixel) {
    return otb::repack<V3>(pixel[xband], pixel[yband], pixel[zband]);
  };

  /// λ that extracts satellite XYZ coordinates from a bigger pixel
  auto const satXYZ = [sxband=m_sXBand, syband=m_sYBand, szband=m_sZBand](auto const& pixel) {
    return otb::repack<V3>(pixel[sxband], pixel[syband], pixel[szband]);
  };

  /// λ that extracts surface normal vector XYZ coordinates from a bigger pixel
  auto const nXYZ = [nxband=m_nXBand, nyband=m_nYBand, nzband=m_nZBand](auto const& pixel) {
    return otb::repack<V3>(pixel[nxband], pixel[nyband], pixel[nzband]);
  };

  ///  λ-ception: λ that returns dedicated λ able to tell whether a pixel is invalid.
  auto make_invalidity_checker = [](double nodata) {
    // SARDEMProjection may return {0, 0, 0}
    auto const zero_pixel   = V3{0., 0., 0.};
    // SARDEMProjection & ExtractNormalVector may produce nodata;
    auto const nodata_pixel = V3{nodata, nodata, nodata};
    // and all may produce nan

    return [zero_pixel, nodata_pixel](auto v) {
      V3 const& w = v;
      return
        std::isnan(w[0]) || std::isnan(w[1]) || std::isnan(w[2])
        || w == zero_pixel
        || w == nodata_pixel
      ;
    };
  };

  // For slightly better performances, localise all the member variables as local variables.
  auto const ia_nodata = m_ia_nodata;
  /// λ that tells whether the ground/surface XYZ position is invalid
  auto const is_xyz_invalid = make_invalidity_checker(m_xyz_nodata);
  /// λ that tells whether the normal XYZ vector is invalid
  auto const is_nrm_invalid = make_invalidity_checker(m_nrm_nodata);

  // -----[ Region related information & iterators
  // Compute corresponding input region
  auto const inputRegionForThread = outputRegionForThread;

  using InputIterator = itk::ImageScanlineConstIterator< InputImageType >;
  InputIterator xyzIt(this->GetXYZInputImage(),    inputRegionForThread);
  InputIterator   nIt(this->GetNormalInputImage(), inputRegionForThread);

#if 0
  using OutputSinIterator = itk::ImageScanlineIterator< OutputSinImageType >;
  using OutputCosIterator = itk::ImageScanlineIterator< OutputCosImageType >;
  using OutputTanIterator = itk::ImageScanlineIterator< OutputTanImageType >;
  using OutputDegIterator = itk::ImageScanlineIterator< OutputDegImageType >;

  static_assert(std::is_same<OutputSinIterator, OutputCosIterator>::value, "");
  static_assert(std::is_same<OutputSinIterator, OutputTanIterator>::value, "");
#endif

  // Using 2 ZipIterator to handle 0 to n outputs, encapsulated in a TupleIterator
  // In an ideal world, we would have used a single
  // ZipIterator<variant<OutputSinIterator, OutputDegIterator>>. However, this
  // is not simple to provide. Hence the 2 ZipIterator's used:
  // 1. for all sin, cos and tan outputs
  // 2. for deg output -- possibly empty
  std::vector<OutputSinImageType *> trigo_images;
  if (m_sin_map_index) trigo_images.push_back(this->GetSinOutputImage());
  if (m_cos_map_index) trigo_images.push_back(this->GetCosOutputImage());
  if (m_tan_map_index) trigo_images.push_back(this->GetTanOutputImage());
  std::vector<OutputPixelType>    pixel_trigo_output(trigo_images.size());

  std::vector<OutputDegImageType *> deg_images;
  if (m_deg_map_index) deg_images.push_back(this->GetDegOutputImage());
  std::vector<OutputDegPixelType> pixel_deg_output(m_deg_map_index.has_value() ? 1 : 0);

  auto out_iterators = make_optional_iterator_tuple(
      make_zip(trigo_images, outputRegionForThread),
      make_zip(deg_images,   outputRegionForThread)
  );

  auto const& o_size    = outputRegionForThread.GetSize();
  auto const  o_nblines = o_size[1];
  auto const& o_start   = outputRegionForThread.GetIndex();
  auto const  o_idx_x1  = o_start[0];
  // int  const  o_idx_x2  = o_start[0] + o_size[0];
  int  const  o_idx_y1  = o_start[1];
  // auto const  o_idx_y2  = o_start[1] + o_size[1];

  // auto const& i_size    = inputRegionForThread.GetSize();
  // auto const  i_nblines = i_size[1];
  // auto const& i_start   = inputRegionForThread.GetIndex();
  // auto const  i_idx_x1  = i_start[0];
  // int  const  i_idx_x2  = i_start[0] + i_size[0];
  // int  const  i_idx_y1  = i_start[1];

  // auto const& img_region = this->GetOutput()->GetLargestPossibleRegion();
  // auto const  img_width  = img_region.GetSize()[0];

#if 0
  otbMsgDevMacro("thr #"<< threadId<<" works on output region: "
      << NeatRegionLogger{outputRegionForThread}
      << " from input: " << NeatRegionLogger{inputRegionForThread}
      // << " and grid: " << NeatRegionLogger{gridRegionForThread}
      );
#endif

  auto const as_degree = [](auto rad) { return static_cast<unsigned short>(_18000_PI<decltype(rad)> * rad);};
  // Support progress methods/callbacks
  itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() / o_nblines );

  // -----[ The loop!
  for (auto y = o_idx_y1
       ; !out_iterators.IsAtEnd()
       ; out_iterators += itk::Line{}, xyzIt+=itk::Line{}, nIt+=itk::Line{}, y += 1
      )
  {
    xyzIt.GoToBeginOfLine();
    nIt.GoToBeginOfLine();
    out_iterators.GoToBeginOfLine(); assert(!out_iterators.IsAtEnd());


    for (auto x = o_idx_x1
         ; !out_iterators.IsAtEndOfLine()
         ; ++out_iterators, ++x, ++xyzIt, ++nIt)
    {
      assert(!out_iterators.IsAtEndOfLine());

      assert(!xyzIt.IsAtEndOfLine());
      assert(!nIt.IsAtEndOfLine());

      /// λ that computes IA and its sine
      auto const compute = [&]()
      {
        auto const ground = grdXYZ(*xyzIt);
        if (is_xyz_invalid(ground))
        {
#if 0
          otbMsgDevMacro("@["<<x<<","<<y<<"]: INVALID grd: "<< ground
              << "; antenna: " << pos_antenna);
#endif
          return std::make_pair(ia_nodata, ia_nodata);
        }

        auto const normal = nXYZ(*nIt); // expected normalized
        if (is_nrm_invalid(normal))
        {
#if 0
          otbMsgDevMacro("@["<<x<<","<<y<<"]: grd: "<< ground
              << "; INVALID normal: " << normal
              << "; antenna: " << pos_antenna);
#endif
          return std::make_pair(ia_nodata, ia_nodata);
        }

        auto const ground_unit = normalize(ground);
        assert(dot_product(normal, ground_unit) >= 0);

        auto const& pos_antenna = m_AntennaPositions.empty()
          ?  satXYZ(*xyzIt)
          : m_AntennaPositions[y];
        if (is_xyz_invalid(pos_antenna))
          return std::make_pair(ia_nodata, ia_nodata);


        auto const TS = pos_antenna - ground;
        auto const TS_unit = normalize(TS);

        // Ground normal projected in range plane (S, Ground, Earth Centre)
        // | The projected normal is the normal minus the normal component of
        // | the rangle plane.
        auto const range_plane_normal = vnl_cross_3d(ground_unit, TS_unit);
        auto const range_plane_normal_unit = normalize(range_plane_normal);
        auto const projected_normal = normal
          - dot_product(range_plane_normal_unit, normal) *
          range_plane_normal_unit;
        auto const projected_normal_unit = normalize(projected_normal);
#if 0
        static int cpt = 0;
        if (++cpt % 1'000'000 == 0)
        {
          otbMsgDevMacro("@["<<x<<","<<y<<"]: grd: " << ground
              << "; normal: " << normal
              << "; prj normal: " << projected_normal_unit
              // << "; antenna: " << pos_antenna
              // << "; TS: " << TS
              << "; TS/||TS||: " << TS_unit
              );
        }
#endif
#if 0
        auto const cos_theta = dot_product(normal, TS_unit);

        // clamp is used in case of incorrect roundings
        auto const ia = std::acos(clamp(cos_theta, -1.0, +1.0));
        auto const sin_ia = std::sin(ia);
        return std::make_pair(sin_ia, ia);
#elif 0
        auto const n = vnl_cross_3d(normal, TS_unit);
        auto const sin_ia = n.two_norm();
        auto const ia     = std::asin(clamp(sin_ia, -1.0, +1.0));
        return std::make_pair(sin_ia, ia);
#else
        return precise_angle(projected_normal_unit, TS_unit);
#endif
      };

      auto const sin_n_rad   = compute();
      auto const sin_ia      = sin_n_rad.first;
      auto const rad_ia      = sin_n_rad.second;
      auto const out_sin_ia = static_cast<OutputPixelType>(sin_ia);

      assert_op(pixel_trigo_output.size(), == , trigo_images.size());
      if (m_sin_map_index) pixel_trigo_output[*m_sin_map_index] = out_sin_ia;
      if (m_cos_map_index) pixel_trigo_output[*m_cos_map_index] = std::cos(rad_ia);
      if (m_tan_map_index) pixel_trigo_output[*m_tan_map_index] = std::tan(rad_ia);
      out_iterators.template Set<0>(pixel_trigo_output);

      if (m_deg_map_index)
      {
        pixel_deg_output[0] = rad_ia != ia_nodata ? as_degree(rad_ia) : rad_ia;
        out_iterators.template Set<1>(pixel_deg_output);
      }

#if 0
      static int cpt = 0;
      if (++cpt % 1'000'000 == 0)
      {
        auto const cos_theta = std::cos(rad_ia);
        otbMsgDevMacro(
            << "; cos Θ: " << cos_theta
            << "; Θ: " << rad_ia << "rad / " << (as_degree(rad_ia)/100) << "° ; sin Θ: " << sin_ia
            );
      }
#endif
    } // for x in range

    progress.CompletedPixel(); // Completed...Line()
  } // for y in azimuth

}

#endif  // otbSARComputeIncidenceAngle_hxx
