/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbComputeNormalsOnEllipsoidImageSource_h
#define otbComputeNormalsOnEllipsoidImageSource_h

#include "SARCalibrationExtendedExport.h"
#include "Filters/otbImageSourceProjectionReferenceDecorator.h"
#include "Filters/otbImageSourceGeometryDecorator.h"
#include "otbVectorImage.h"
#include "itkImageSource.h"
#include "itkMacro.h"
#include <type_traits>

namespace otb
{

/**
 * ComputeNormalsOnEllipsoidImageSource.
 * This image source specialization will produce an (X, Y, Z) map of normal
 * vectors on Earth Ellipsoid defined by WGS84 datum.
 *
 * Given a point at lon=φ, lat=λ, normal(φ, λ) =
 * - cos φ cos λ
 * - cos φ sin λ
 * - sin φ
 *
 * \see
 * https://en.wikipedia.org/wiki/N-vector#Converting_latitude/longitude_to_n-vector
 *
 */
class SARCalibrationExtended_EXPORT ComputeNormalsOnEllipsoidImageSource
: public ImageSourceProjectionReferenceDecorator
<        ImageSourceGeometryDecorator
<        itk::ImageSource<otb::VectorImage<double>>>>
{
public:

  /**\name Standard class typedefs */
  //@{

  /** This _image source_ will produce XYZ ECEF coordinates, as `float`
   * precision is non sufficient, `doubles` are hard-coded in this class.
   */
  using PixelRealType = double;

  using Self                          = ComputeNormalsOnEllipsoidImageSource;
  using Superclass                    = ImageSourceProjectionReferenceDecorator<ImageSourceGeometryDecorator<itk::ImageSource<otb::VectorImage<double>>>>;
  using Pointer                       = itk::SmartPointer<Self>;
  using ConstPointer                  = itk::SmartPointer<const Self>;

  using OutputImageType               = otb::VectorImage<double>;

  // Image source typedefs
  static_assert(std::is_same<OutputImageType, Superclass::OutputImageType>::value, "shall match");

  using OutputImagePointer            = Superclass::OutputImagePointer;
  using SpacingType                   = Superclass::SpacingType;
  using SizeType                      = Superclass::SizeType;
  using PointType                     = Superclass::PointType;
  using IndexType                     = Superclass::IndexType;
  using PixelType                     = Superclass::PixelType;
  using OutputImageRegionType         = Superclass::OutputImageRegionType;

  using Point2DType                   = itk::Point<double, 2>;
  using Point3DType                   = itk::Point<double, 3>;
  //@}

  /** Method for creation through the object factory. */
  itkNewMacro(ComputeNormalsOnEllipsoidImageSource);

  /** Run-time type information (and related methods). */
  itkTypeMacro(Self, ImageSource);

protected:
  // void BeforeThreadedGenerateData() override;
  void ThreadedGenerateData(
      OutputImageRegionType const& outputRegionForThread,
      itk::ThreadIdType threadId) override;

  void GenerateOutputInformation() override;

private:
  ComputeNormalsOnEllipsoidImageSource() = default;
  virtual ~ComputeNormalsOnEllipsoidImageSource() = default;

  ComputeNormalsOnEllipsoidImageSource(ComputeNormalsOnEllipsoidImageSource const&) = delete;
  ComputeNormalsOnEllipsoidImageSource& operator=(ComputeNormalsOnEllipsoidImageSource const&) = delete;
};

} // otb namespace

#endif  // otbComputeNormalsOnEllipsoidImageSource_h
