/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbXYZToImageGenerator_hxx
#define otbXYZToImageGenerator_hxx

#include "Filters/otbXYZToImageGenerator.h"
#include "Filters/otbBandIds.h"
#include "SAR/otbPositionHelpers.h"
#include "otbMacro.h"
#include "itkProgressReporter.h"

namespace otb
{

template <class TDEMImage>
XYZToImageGenerator<TDEMImage>::XYZToImageGenerator()
{
  m_OutputOrigin[0]     = 0.0;
  m_OutputOrigin[1]     = 0.0;
  m_OutputSpacing[0]    = 0.0001;
  m_OutputSpacing[1]    = -0.0001;
  m_DefaultUnknownValue = PixelRealType(0);

  m_Transform = GenericRSTransformType::New();
}

template <class TDEMImage>
template <class TImageType>
void XYZToImageGenerator<TDEMImage>
::SetOutputParametersFromImage(TImageType const* image)
{
  this->SetOutputOrigin(image->GetOrigin());
  this->SetOutputSpacing(image->GetSignedSpacing());
  // this->SetOutputStartIndex ( image->GetLargestPossibleRegion().GetIndex() );
  this->SetOutputSize(image->GetLargestPossibleRegion().GetSize());
  this->SetOutputProjectionRef(image->GetProjectionRef());
#if OTB_VERSION_MAJOR >= 8
#if 1
  // We need to store it globally because m_Transform->SetOutputImageMetadata()
  // will store the adress to the variable and not take any copy
  m_OutputImageMetadata = image->GetImageMetadata();
  m_OutputImageMetadata.Add(bands::k_id_X, "0");
  m_OutputImageMetadata.Add(bands::k_id_Y, "1");
  m_OutputImageMetadata.Add(bands::k_id_Z, "2");
  m_OutputImageMetadata.Add(bands::k_id_H, "3");
  this->SetOutputImageMetadata(m_OutputImageMetadata);
#else
  this->SetOutputImageMetadata(image->GetImageMetadata());
#endif
  // this->SetInputImageMetadata(kwl);
#else
  auto kwl = image->GetImageKeywordlist();
  kwl.AddKey(bands::k_id_X, "0");
  kwl.AddKey(bands::k_id_Y, "1");
  kwl.AddKey(bands::k_id_Z, "2");
  kwl.AddKey(bands::k_id_H, "3");
  this->SetOutputKeywordList(kwl);
#endif
  this->m_InputImage = image;

  InstantiateTransform();
}

// GenerateOutputInformation method
template <class TDEMImage>
void XYZToImageGenerator<TDEMImage>::GenerateOutputInformation()
{
  DEMImageType* output = this->GetOutput(0);
  output->SetVectorLength(3);

  IndexType start{0, 0};

  // Specify region parameters
  OutputImageRegionType largestPossibleRegion{start, m_OutputSize};

  output->SetLargestPossibleRegion(largestPossibleRegion);
  output->SetSignedSpacing(m_OutputSpacing);
  output->SetOrigin(m_OutputOrigin);

#if OTB_VERSION_MAJOR >= 8
  // Add the metadata set by the user to the output
  output->m_Imd.Add(MDGeom::ProjectionProj, std::string(m_Transform->GetInputProjectionRef()));
  if (m_Transform->GetInputImageMetadata() != nullptr)
    output->m_Imd.Merge(*m_Transform->GetInputImageMetadata());
#else
  // Get the Output MetaData Dictionary
  itk::MetaDataDictionary& dict = output->GetMetaDataDictionary();

  // Encapsulate the  metadata set by the user
  itk::EncapsulateMetaData<std::string>(dict, MetaDataKey::ProjectionRefKey, m_Transform->GetInputProjectionRef());

  if (this->GetOutputKeywordList().GetSize() > 0)
  {
    itk::EncapsulateMetaData<ImageKeywordlist>(dict, MetaDataKey::OSSIMKeywordlistKey, m_Transform->GetInputKeywordList());
  }
#endif
}

// InstantiateTransform method
template <class TDEMImage>
void XYZToImageGenerator<TDEMImage>::InstantiateTransform()
{
  assert(m_Transform.IsNotNull());
  m_Transform->InstantiateTransform();
}

template <class TDEMImage>
void XYZToImageGenerator<TDEMImage>::BeforeThreadedGenerateData()
{
  // TODO: do once only!!
  InstantiateTransform();
  DEMImagePointerType DEMImage = this->GetOutput();

  // allocate the output buffer
  DEMImage->SetBufferedRegion(DEMImage->GetRequestedRegion());
  DEMImage->Allocate();
  // PixelType zero(4);
  // DEMImage->FillBuffer(zero);
}


template <class TDEMImage>
void XYZToImageGenerator<TDEMImage>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{
  assert(m_Transform.IsNotNull());
  DEMImagePointerType DEMImage = this->GetOutput();

  // Create an iterator that will walk the output region
  ImageIteratorType outIt = ImageIteratorType(DEMImage, outputRegionForThread);

  // support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  // Walk the output image, evaluating the height at each pixel
  double    height;

  // Cache member variables to help compiler optimize, in particular to
  // move the test outside the loop
  bool const aboveEllipsoid      = m_AboveEllipsoid;
  auto const defaultUnknownValue = m_DefaultUnknownValue;
#if OTB_VERSION_MAJOR >= 8
  auto & rDEMHandler = DEMHandler::GetInstance();
#else
  auto & rDEMHandler = *DEMHandler::Instance();
#endif
  assert(DEMImage);
  assert(m_Transform.IsNotNull());
  auto & rDEMImage   = *DEMImage;
  auto & rTransform  = *m_Transform;

  // Wouldn't it be more efficient to use ScanLineIteror and increment
  // progress only once per line?
  for (outIt.GoToBegin(); !outIt.IsAtEnd(); ++outIt)
  {
    auto const currentindex = outIt.GetIndex();
    auto const phyPoint = TransformIndexToPhysicalPoint(rDEMImage, currentindex);

    // Altitude calculation
    auto const geoPoint = rTransform.TransformPoint(phyPoint);
    if (aboveEllipsoid)
    {
      height = rDEMHandler.GetHeightAboveEllipsoid(geoPoint);
    }
    else
    {
      height = rDEMHandler.GetHeightAboveMSL(geoPoint);
    }

    auto const xyz = WorldToCartesian(auto_repack(geoPoint, height));

#if 0
    static int cpt = 0;
    // if (++cpt % 1'000'000 == 0)
    if (++cpt % 1'000 == 0)
    {
      otbMsgDevMacro(<< "Index : (" << currentindex[0]<< "," << currentindex[1] << ") -> PhyPoint : ("
          << phyPoint[0] << "," << phyPoint[1] << ") -> GeoPoint: ("
          << geoPoint[0] << "," << geoPoint[1] << ") -> height: " << height);
      // otbMsgDevMacro(<< "height" << height);
    }
#endif
    // DEM sets a default value (-32768) at point where it doesn't have altitude information.
    // OSSIM has chosen to change this default value in OSSIM_DBL_NAN (-4.5036e15).
    double const h = !vnl_math_isnan(height)
      ?  static_cast<PixelRealType>(height)
      : defaultUnknownValue;
    rDEMImage.SetPixel(currentindex, otb::repack<PixelType>(xyz, h));
    progress.CompletedPixel();
  }
}

template <class TDEMImage>
void XYZToImageGenerator<TDEMImage>::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);

  os << indent << "Output Spacing:" << m_OutputSpacing[0] << "," << m_OutputSpacing[1] << "\n";
  os << indent << "Output Origin: " << m_OutputOrigin[0] << "," << m_OutputOrigin[1] << "\n";
  os << indent << "Output Size:   " << m_OutputSize[0] << "," << m_OutputSize[1] << "\n";
}

} // namespace otb

#endif
