/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbXYZToImageGenerator_h
#define otbXYZToImageGenerator_h

#include "SARCalibrationExtendedExport.h"
#include "otbDEMHandler.h"
#include "otbGenericRSTransform.h"
#if defined(DRAFT_OTB8) || OTB_VERSION_MAJOR >= 8
#include "otbImageMetadata.h"
#endif
#include "itkImageRegionIteratorWithIndex.h"
#include "itkImageSource.h"
#include <string>

namespace otb
{
/** \class XYZToImageGenerator
 *
 * \brief Class to generate an image from DEM data
 *
 * This class is based on DEMHandler.
 * It takes in input the upper left longitude and latitude, the spacing and the
 * output image size.  Handle DTED and SRTM formats.
 *
 * \invariant `m_Transform != nullptr`
 *
 * \ingroup Images
 *
 * \example IO/XYZToImageGenerator.cxx
 *
 * \ingroup OTBDEM
 */
template <class TDEMImage>
class SARCalibrationExtended_EXPORT XYZToImageGenerator : public itk::ImageSource<TDEMImage>
{
public:
  /** Standard class typedefs. */
  using DEMImageType                  = TDEMImage;
  using DEMImagePointerType           = typename DEMImageType::Pointer;
  using PixelType                     = typename DEMImageType::PixelType;
  using PixelRealType                 = typename itk::NumericTraits<PixelType>::ElementRealType;

  using Self                          = XYZToImageGenerator;
  using Superclass                    = itk::ImageSource<DEMImageType>;
  using Pointer                       = itk::SmartPointer<Self>;
  using ConstPointer                  = itk::SmartPointer<const Self>;
  using OutputImageType               = DEMImageType;

  using OutputImagePointer            = typename Superclass::Pointer;
  using SpacingType                   = typename OutputImageType::SpacingType;
  using SizeType                      = typename OutputImageType::SizeType;
  using PointType                     = typename OutputImageType::PointType;
  using IndexType                     = typename OutputImageType::IndexType;
  using OutputImageRegionType         = typename Superclass::OutputImageRegionType;
  using ImageIteratorType             = itk::ImageRegionIteratorWithIndex<DEMImageType>;

  using DEMHandlerType                = otb::DEMHandler;

  /** Specialisation of OptResampleFilter with a remote
    * sensing  transform
    */
  using GenericRSTransformType        = GenericRSTransform<double, 2, 2>;
  using GenericRSTransformPointerType = typename GenericRSTransformType::Pointer;


  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(XYZToImageGenerator, ImageSource);

  /** Set/Get the Output Origin coordinates. */
  itkSetMacro(OutputOrigin, PointType);
  itkGetConstReferenceMacro(OutputOrigin, PointType);

  /** Set/Get the Output Size. */
  itkSetMacro(OutputSize, SizeType);
  itkGetConstReferenceMacro(OutputSize, SizeType);

  /** Set/Get the Output Spacing. */
  itkSetMacro(OutputSpacing, SpacingType);
  itkGetConstReferenceMacro(OutputSpacing, SpacingType);

  /** Set/Get the Default Unknown Value. */
  // itkSetObjectMacro(Transform, GenericRSTransformType);
  itkGetConstObjectMacro(Transform, GenericRSTransformType);

  /** Set/Get the above ellipsoid flag. If false, height is given
  above MSL */
  itkSetMacro(AboveEllipsoid, bool);
  itkGetMacro(AboveEllipsoid, bool);
  itkBooleanMacro(AboveEllipsoid);

  /**
   * @pre `m_Transform != nullptr`
   */
  void InstantiateTransform();

  /**
   * Set/Get input & output projections.
   * Set/Get input & output keywordlist
   * The macro are not used here cause the input and the output are
   * inversed.
   */
  void SetInputProjectionRef(const std::string& ref)
  {
    assert(m_Transform);
    m_Transform->SetOutputProjectionRef(ref);
    this->Modified();
  }

  std::string GetInputProjectionRef() const
  {
    assert(m_Transform);
    return m_Transform->GetOutputProjectionRef();
  }

  void SetOutputProjectionRef(const std::string& ref)
  {
    assert(m_Transform);
    m_Transform->SetInputProjectionRef(ref);
    this->Modified();
  }

  std::string GetOutputProjectionRef() const
  {
    assert(m_Transform);
    return m_Transform->GetInputProjectionRef();
  }

#if (defined(DRAFT_OTB8) || OTB_VERSION_MAJOR >= 8)
  /** Set/Get ImageMetadata*/
  const ImageMetadata* GetInputImageMetadata() const
  {
    assert(m_Transform);
    return m_Transform->GetOutputImageMetadata();
  }

  void SetInputImageMetadata(ImageMetadata const& imd)
  {
    assert(m_Transform);
    m_Transform->SetOutputImageMetadata(&imd);
    this->Modified();
  }

  const ImageMetadata* GetOutputImageMetadata() const
  {
    assert(m_Transform);
    return m_Transform->GetInputImageMetadata();
  }

  void SetOutputImageMetadata(ImageMetadata const& imd)
  {
    assert(m_Transform);
    m_Transform->SetInputImageMetadata(&imd);
    this->Modified();
  }

#else // next gen code
  /** Set/Get Input Keywordlist*/
  void SetInputKeywordList(const ImageKeywordlist& kwl)
  {
    assert(m_Transform);
    m_Transform->SetOutputKeywordList(kwl);
    this->Modified();
  }
  ImageKeywordlist const& GetInputKeywordList() const
  {
    assert(m_Transform);
    return m_Transform->GetOutputKeywordList();
  }

  /** Set/Get output Keywordlist*/
  void SetOutputKeywordList(const ImageKeywordlist& kwl)
  {
    assert(m_Transform);
    m_Transform->SetInputKeywordList(kwl);
    this->Modified();
  }

  ImageKeywordlist GetOutputKeywordList() const
  {
    assert(m_Transform);
    // Returning a temporary... => cannot return a reference
    return m_Transform->GetInputKeywordList();
  }
#endif

  /** Useful to set the output parameters from an existing image*/
  template <class TImageType>
  void SetOutputParametersFromImage(const TImageType* image);

protected:
  XYZToImageGenerator();
  ~XYZToImageGenerator() override = default;

  void PrintSelf(std::ostream& os, itk::Indent indent) const override;
  void BeforeThreadedGenerateData() override;
  void ThreadedGenerateData(
      OutputImageRegionType const& outputRegionForThread,
      itk::ThreadIdType threadId) override;
  void GenerateOutputInformation() override;

  PointType                        m_OutputOrigin{0.0};
  SpacingType                      m_OutputSpacing{0.0};
  SizeType                         m_OutputSize{0,0};
  PixelRealType                    m_DefaultUnknownValue;
  bool                             m_AboveEllipsoid {false};
  itk::ImageBase<2u>::ConstPointer m_InputImage;
#if defined(DRAFT_OTB8) || OTB_VERSION_MAJOR >= 8
  ImageMetadata                    m_OutputImageMetadata;
#endif

private:
  XYZToImageGenerator(const Self&) = delete;
  void operator=(const Self&) = delete;

  GenericRSTransformPointerType m_Transform;
};

} // namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "Filters/otbXYZToImageGenerator.hxx"
#endif

#endif
