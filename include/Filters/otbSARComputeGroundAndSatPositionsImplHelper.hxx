/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARComputeGroundAndSatPositionsImplHelper_hxx
#define otbSARComputeGroundAndSatPositionsImplHelper_hxx

#include "Filters/otbSARComputeGroundAndSatPositionsImplHelper.h"
#include "Metadata/otbMetadataHelper.h"
#include "Common/otbRepack.h"
#include "SAR/otbPositionHelpers.h"
#include "otbGeocentricTransform.h"
#include <itkVariableLengthVector.h>

namespace
{ // Anonymous namespace

double DotProduct(itk::Point<double, 3> const& pt1, itk::Point<double, 3> const& pt2)
{
  // Manual dot product is a bit faster w/ gcc...
  return pt1[0]*pt2[0] + pt1[1]*pt2[1] + pt1[2]*pt2[2];
  // return std::inner_product(pt1.Begin(), pt1.End(), pt2.Begin(), 0.);
}

otb::MetaData::TimePoint time(otb::Orbit const& o) { return o.time; }

} // Anonymous namespace

namespace otb
{
/**
 * Constructor with default initialization
 */
template <class TImageOut>
SARComputeGroundAndSatPositionsImplHelper<TImageOut>
::SARComputeGroundAndSatPositionsImplHelper(
    std::vector<Orbit> osvs,
    unsigned int       polynomial_degree
)
: m_orbit(std::move(osvs))
, m_orbitInterpolator(m_orbit, polynomial_degree)
, m_polynomial_degree(polynomial_degree)
{}

template <class TImageOut>
void
SARComputeGroundAndSatPositionsImplHelper<TImageOut>
::WriteBandMetaData(
    ImageMetadata & metadata,
    ImageOutType  & image
)
{
  BandInformation bands(metadata, BandInformation::clear);

  // Set the number of Components for the Output VectorImage
  // m_withH      = true => 1 component added H : high
  // m_withXYZ    = true => 3 components added Xcart Ycart Zcart : cartesian coordinates
  // m_withSatPos = true => 3 components added : Satellite Position into cartesian

  auto add_metadata = [& metadata](
      std::string const& key,
      std::string const& value)
  {
    metadata.Add(key, value);
  };

  m_NbComponents = 0;
  if (m_withXYZ)
  {
    m_indXYZ = m_NbComponents;
    add_metadata("band.XCart", std::to_string(m_indXYZ)  );
    add_metadata("band.YCart", std::to_string(m_indXYZ+1));
    add_metadata("band.ZCart", std::to_string(m_indXYZ+2));
    m_NbComponents += 3;

    bands.add_new("XCart", m_dstNoData);
    bands.add_new("YCart", m_dstNoData);
    bands.add_new("ZCart", m_dstNoData);
  }
  if (m_withH)
  {
    m_indH = m_NbComponents;
    add_metadata("band.H", std::to_string(m_indH));
    m_NbComponents += 1;

    bands.add_new("Height", m_dstNoData);
  }
  if (m_withSatPos)
  {
    m_indSatPos = m_NbComponents;
    add_metadata("band.XSatPos", std::to_string(m_indSatPos)  );
    add_metadata("band.YSatPos", std::to_string(m_indSatPos+1));
    add_metadata("band.ZSatPos", std::to_string(m_indSatPos+2));
    m_NbComponents += 3;

    bands.add_new("XSatPos", m_dstNoData);
    bands.add_new("YSatPos", m_dstNoData);
    bands.add_new("ZSatPos", m_dstNoData);
  }
  bands.write_back();

  otbLogMacro(Info, <<"SARComputeGroundAndSatPositions will produce a " << m_NbComponents << " bands image");
  image.SetNumberOfComponentsPerPixel(m_NbComponents);

  // Set nodata -- not enough to be globally interpreted as "value on all bands"
  metadata.Add(MDNum::NoData, m_dstNoData);

  otbLogMacro(Debug, << "Output metadata " << metadata);
}

template<class TImageOut>
auto
SARComputeGroundAndSatPositionsImplHelper<TImageOut >
::ZeroDopplerTimeLookupInternal(Point3DType const& inEcefPoint) const
-> std::tuple<TimeType, OrbitIterator, OrbitIterator>
{
  if (m_orbit.size() < 2)
  {
    otbGenericExceptionMacro(itk::ExceptionObject, <<"Orbit records vector contains less than 2 elements");
  }

  auto it = m_orbit.cbegin();

  // Compute range and doppler of first record
  // NOTE: here we only use the scalar product with vel and discard
  // the constant coef as it has no impact on doppler sign

  double doppler1 = DotProduct(inEcefPoint - it->position, it->velocity);

  bool dopplerSign1 = doppler1 < 0;

  ++it;
  double doppler2 = 0.;

  // Look for the consecutive records where doppler freq changes sign
  // Note: implementing a bisection algorithm here might be faster
  for ( ; it != m_orbit.cend() ; ++it)
  {
     // compute range and doppler of current record
     doppler2 = DotProduct(inEcefPoint - it->position, it->velocity);

     const bool dopplerSign2 = doppler2 < 0;

     // If a change of sign is detected
     if(dopplerSign1 != dopplerSign2)
     {
        break;
     }
     else
     {
        doppler1 = doppler2;
     }
  }

  // In this case, we need to extrapolate
  if(it == m_orbit.cend())
  {
    //TODO test this case
    auto record1 = m_orbit.cbegin();
    auto record2 = record1 + m_orbit.size()-1;
    doppler1 = DotProduct(inEcefPoint - record1->position, record1->velocity);
    doppler2 = DotProduct(inEcefPoint - record2->position, record2->velocity);
    auto const delta_td = record2->time - record1->time;

    return {
      record1->time - doppler1 / (doppler2 - doppler1) * delta_td,
      record1, record2
    };
  }
  else
  {
    assert(it != m_orbit.cbegin());
    assert(it != m_orbit.cend());
    auto record2 = it;
    auto record1 = --it;
    // now interpolate time and sensor position
    double const abs_doppler1 = std::abs(doppler1);
    double const interpDenom = abs_doppler1 + std::abs(doppler2);
    assert(interpDenom>0&&"Both doppler frequency are null in interpolation weight computation");
    double const interp = abs_doppler1 / interpDenom;
    auto const delta_td = record2->time - record1->time;
    // Compute interpolated time offset wrt record1
    // (No need for that many computations (day-frac -> ms -> day frac))
    auto const td       = delta_td * interp;

    // Compute interpolated azimuth time
    return { record1->time + td, record1, record2 };
  }
}
template<class TImageOut >
auto
SARComputeGroundAndSatPositionsImplHelper<TImageOut >
::ZeroDopplerLookup(Point3DType const& inEcefPoint) const
-> ZeroDopplerInfo
{
  TimeType      azimuthTime;
  OrbitIterator itRecord1, itRecord2;
  std::tie(azimuthTime, itRecord1, itRecord2)
    = ZeroDopplerTimeLookupInternal(inEcefPoint);

  // Interpolate sensor position and velocity
  Point3DType  sensorPos;
  Vector3DType sensorVel;
  if (Abs(time(*itRecord1) - azimuthTime) > Abs(time(*itRecord2) - azimuthTime))
  { // While this may degrade results, this makes sure to keep identical
    // results to previous code
    // Ideally, we should center around the interval [record1, record2]
    ++itRecord1;
  }
  std::tie(sensorPos, sensorVel) = interpolateSensorPosVel(azimuthTime, itRecord1);

  ZeroDopplerInfo res{azimuthTime, sensorPos, sensorVel};
  return res;
}

template<class TImageOut>
auto
SARComputeGroundAndSatPositionsImplHelper<TImageOut >
::interpolateSensorPosVel(TimeType azimuthTime, OrbitIterator itRecord1) const
-> std::pair<Point3DType, Vector3DType>
{
  assert(m_orbit.size() &&"The orbit records vector is empty");
  auto deg = m_polynomial_degree;

  auto t_min_idx = std::distance(m_orbit.begin(), itRecord1);

  // TODO: see if these expressions can be simplified
  std::size_t nBegin = std::max(t_min_idx-(int)deg/2+1, 0L);
  std::size_t nEnd   = std::min(nBegin+deg-1, m_orbit.size());
  nBegin = nEnd<m_orbit.size()-1 ? nBegin : nEnd-deg+1;

  // If there are less records than degrees, use them all
  if(m_orbit.size() < deg)
  {
    nBegin = 0;
    nEnd   = m_orbit.size()-1;
  }

  // Compute lagrangian interpolation using records from nBegin to nEnd
  assert(nEnd - nBegin < 30); // Lagrangian interpolator fails miserably at 20
                              // elements... Let's expected less than 30 as
                              // reasonable
  assert(nBegin != nEnd);

  return m_orbitInterpolator.interpolatePosVel(azimuthTime, nBegin, nEnd);
}

template<class TImageOut>
void
SARComputeGroundAndSatPositionsImplHelper<TImageOut >
::ComputePixel(
    ImageOutPixelType & pixelOutput,
    Point2DType         lonlat,
    double              elevation
) const
{
  // Correct the height with earth geoid
  Point3DType demGeoPoint = otb::repack<Point3DType>(lonlat, elevation);

  // Always compute the ground point is ECEF
  Point3DType xyzCart = Projection::WorldToEcef(demGeoPoint); // Fastest version!

  auto const zero_doppler_info = this->ZeroDopplerLookup(xyzCart);

  // Add channels if required
  if (this->m_withXYZ)
  {
    assert(0 <= this->m_indXYZ);
    assert(pixelOutput.Size() >= unsigned(this->m_indXYZ + 3));
    pixelOutput[this->m_indXYZ+0] = xyzCart[0];
    pixelOutput[this->m_indXYZ+1] = xyzCart[1];
    pixelOutput[this->m_indXYZ+2] = xyzCart[2];
  }
  if (this->m_withH)
  {
    assert(0 <= unsigned(this->m_indH));
    assert(pixelOutput.Size() > unsigned(this->m_indH));
    pixelOutput[this->m_indH] = demGeoPoint[2];
  }
  if (this->m_withSatPos)
  {
    assert(0 <= this->m_indSatPos);
    assert(pixelOutput.Size() >= unsigned(this->m_indSatPos + 3));
    pixelOutput[this->m_indSatPos+0] = zero_doppler_info.sensorPos[0];
    pixelOutput[this->m_indSatPos+1] = zero_doppler_info.sensorPos[1];
    pixelOutput[this->m_indSatPos+2] = zero_doppler_info.sensorPos[2];
  }
}

} // otb namespace

#endif  // otbSARComputeGroundAndSatPositionsImplHelper_hxx
