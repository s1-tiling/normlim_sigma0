/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This file is a fork of DiapOTB SARDEMProjection adapted to OTB 8 source code
// for the needs of S1Tiling

#ifndef otbSARDEMProjectionImageFilter2_h
#define otbSARDEMProjectionImageFilter2_h

#include "SARCalibrationExtendedExport.h"
#include "otbCoordinateTransformation.h"
#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#if OTB_VERSION_MAJOR >= 8
# include "otbImageMetadata.h"
# include "SAR/otbFastSarSensorModel8.h"
#else
#  include "otbSarSensorModelAdapter.h"
#  include "otbImageKeywordlist.h"

#  if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Woverloaded-virtual"
#  if defined(__GNUC__) && (__GNUC__ > 5)
#    pragma GCC diagnostic ignored "-Wnonnull-compare"
#  endif
#    include "ossim/base/ossimGeoidEgm96.h"
#    pragma GCC diagnostic pop
#  else
#    include "ossim/base/ossimGeoidEgm96.h"
#  endif
#endif

#include "ogr_spatialref.h"
#include <vector>

namespace otb
{

/** \class SARDEMProjectionImageFilter2
 * \brief Performs the projection of the DEM into SAR geometry
 *
 * This filter performs the projection of the DEM into SAR geometry.
 *
 * \ingroup DiapOTBModule
 */
template <typename TImageIn,  typename TImageOut>
class SARCalibrationExtended_EXPORT SARDEMProjectionImageFilter2
: public itk::ImageToImageFilter<TImageIn,TImageOut>
{
public:

    // Standard class typedefs
    using Self         = SARDEMProjectionImageFilter2;
    using Superclass   = itk::ImageToImageFilter<TImageIn,TImageOut>;
    using Pointer      = itk::SmartPointer<Self>;
    using ConstPointer = itk::SmartPointer<const Self>;

    // Method for creation through object factory
    // itkNewMacro(Self);
    /** Method for creation through the object factory. */
    template <typename ... Args>
    static Pointer New(Args&&... args)
    {
      Pointer smartPtr = new Self(std::forward<Args>(args)...);
      smartPtr->UnRegister();
      return smartPtr;
    }
    // Run-time type information
    itkTypeMacro(SARDEMProjectionImageFilter2,ImageToImageFilter);

    /** Typedef to image type */
    using ImageInType              = TImageIn;
    /** Typedef to describe the inout image pointer type. */
    using ImageInPointer           = typename ImageInType::Pointer;
    using ImageInConstPointer      = typename ImageInType::ConstPointer;

    /** Typedef to describe the inout image region type. */
    using ImageInRegionType        = typename ImageInType::RegionType;
    /** Typedef to describe the type of pixel and point for inout image. */
    using ImageInPixelType         = typename ImageInType::PixelType;
    using ImageInPointType         = typename ImageInType::PointType;
    /** Typedef to describe the image index, size types and spacing for inout image. */
    using ImageInIndexType         = typename ImageInType::IndexType;
    using ImageInIndexValueType    = typename ImageInType::IndexValueType;
    using ImageInSizeType          = typename ImageInType::SizeType;
    using ImageInSizeValueType     = typename ImageInType::SizeValueType;
    using ImageInSpacingType       = typename ImageInType::SpacingType;
    using ImageInSpacingValueType  = typename ImageInType::SpacingValueType;


    using ImageOutType             = TImageOut;
    /** Typedef to describe the output image pointer type. */
    using ImageOutPointer          = typename ImageOutType::Pointer;
    using ImageOutConstPointer     = typename ImageOutType::ConstPointer;
    /** Typedef to describe the output image region type. */
    using ImageOutRegionType       = typename ImageOutType::RegionType;
    /** Typedef to describe the type of pixel and point for output image. */
    using ImageOutPixelType        = typename ImageOutType::PixelType;
    using ImageOutPointType        = typename ImageOutType::PointType;
    /** Typedef to describe the image index, size types and spacing for output image. */
    using ImageOutIndexType        = typename ImageOutType::IndexType;
    using ImageOutIndexValueType   = typename ImageOutType::IndexValueType;
    using ImageOutSizeType         = typename ImageOutType::SizeType;
    using ImageOutSizeValueType    = typename ImageOutType::SizeValueType;
    using ImageOutSpacingType      = typename ImageOutType::SpacingType;
    using ImageOutSpacingValueType = typename ImageOutType::SpacingValueType;

    // Define Point2DType and Point3DType
#if OTB_VERSION_MAJOR >= 8
    using Point2DType = FastSarSensorModel::Point2DType;
    using Point3DType = FastSarSensorModel::Point3DType;
#else
    using Point2DType = itk::Point<double,2>;
    using Point3DType = itk::Point<double,3>;
#endif

    // Setters
#if OTB_VERSION_MAJOR >= 8
    void SetSARImageMetadata(ImageMetadata sarImageMetadata);
#else
    void SetSARImageKeyWorList(ImageKeywordlist sarImageKWL);
#endif
    void SetNoData(double nodata) { m_dstNoData = nodata; }
    itkSetMacro(withXYZ,    bool);
    itkSetMacro(withH,      bool);
    itkSetMacro(withSatPos, bool);
    itkSetMacro(withCRYZ,   bool);
    itkSetMacro(directionToScanDEMC, int);
    itkSetMacro(directionToScanDEML, int);
    itkSetMacro(gain, float);

    // Getters
    itkGetMacro(withXYZ,    bool);
    itkGetMacro(withH,      bool);
    itkGetMacro(withSatPos, bool);
    itkGetMacro(withCRYZ,   bool);
    itkGetMacro(directionToScanDEMC, int);
    itkGetMacro(directionToScanDEML, int);
    itkGetMacro(gain, float);

protected:
    // Constructors
    SARDEMProjectionImageFilter2() = delete;

    /** Init constructor.
     * \pre Expects `geoid_filename == DEMHandler.GetGeoidFile()`
     *
     * This constructor won't update the Geoid data on `DEMHandler` singleton.
     * The singleton is expected to have been correctly configured beforehand.
     * However with older OSSIM-based versions of OTB, it will configure the
     * internal OSSIM based GEOID reader in the case of egm96 geoids.
     */
    SARDEMProjectionImageFilter2(std::string const& geoid_filename);

    /// Destructor
    ~SARDEMProjectionImageFilter2() override = default;

    /** SARDEMProjectionImageFilter2 produces an image which the same size
     * its input image. The different between output and input images are
     * the dimensions. The output image are a otb::VectorImage with four components.
     * As such, SARDEMProjectionImageFilter needs to provide
     * an implementation for GenerateOutputInformation() in order to
     * inform the pipeline execution model.
     */
    void GenerateOutputInformation() override;

    /** SARDEMProjectionImageFilter2 needs a input requested region with the same size of
      the output requested region.
     * \sa ProcessObject::GenerateInputRequestedRegion()
     */
    void GenerateInputRequestedRegion() override;


    /** SARDEMProjectionImageFilter2 can be implemented as a multithreaded
     * filter.
     * Therefore, this implementation provides a `ThreadedGenerateData()`
     * routine which is called for each processing thread. The output image
     * data is allocated automatically by the superclass prior to calling
     * `ThreadedGenerateData()`.  `ThreadedGenerateData` can only write to the
     * portion of the output image specified by the parameter
     * "outputRegionForThread"
     *
     * \sa `ImageToImageFilter::ThreadedGenerateData()`,
     *     `ImageToImageFilter::GenerateData()`
     */
    void ThreadedGenerateData(
        const ImageOutRegionType& outputRegionForThread,
        itk::ThreadIdType         threadId) override;

private:
    template <typename GeoidOffsetPolicy>
    void DoThreadedGenerateData(
        const ImageOutRegionType& outputRegionForThread,
        itk::ThreadIdType         threadId,
        GeoidOffsetPolicy         get_geoid_offset);

    SARDEMProjectionImageFilter2(const Self&) = delete;
    void operator=(const Self &) = delete;

    std::string                      m_geoid_filename;

    // Instance of SarSensorModelAdapter
#if OTB_VERSION_MAJOR >= 8
    // Instance of SarSensorModel
    std::unique_ptr<FastSarSensorModel>  m_SarSensorModel;

    // SAR Image Metadata
    ImageMetadata                    m_SarImageMetadata;
#else
    SarSensorModelAdapter::Pointer   m_SarSensorModel;

    // SAR Image KeyWorldList
    ImageKeywordlist                 m_SarImageKwl;

    std::unique_ptr<ossimGeoidEgm96> m_geoidEmg96;
#endif

    // Values for NoData into DEM
    double m_dstNoData = -32768; //!< nodata value for image produced
    double m_srcNoData = -32768; //!< nodata value found in input DEM

    // Flags that tell which components shall be produced
    bool m_withXYZ    = false; //!< Produce cartesian coordinates?
    bool m_withH      = false; //!< Produce height?
    bool m_withSatPos = false; //!< Produce topographic phase?
    bool m_withCRYZ   = true;

    int m_NbComponents = 0;
    int m_indXYZ       = -1; //!< index into output to set the XYZ components
    int m_indH         = -1; //!< index into output to set the H component
    int m_indSatPos    = -1; //!< index into output to set the SatPos components

    // Gain and directions to save information into metadata (outputkwl)
    int m_directionToScanDEMC = 42; // Default values to avoid jump on ...
    int m_directionToScanDEML = 42; // ... non-initialized in valgrind
    float m_gain              = 0.;

    // TODO: The day otb::CoordinateTransformation exposes batch tranformation,
    // we could have directly a vector<CoordinateTransformation>
    // => For the moment, use the right unique_ptr type.
    std::vector<CoordinateTransformation::CoordinateTransformationPtr> m_tlsCTs;
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "Filters/otbSARDEMProjectionImageFilter2.hxx"
#endif

#endif
