/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDEMPolygonsAnalysisImageFilter2_hxx
#define otbSARDEMPolygonsAnalysisImageFilter2_hxx

#include "Filters/otbSARDEMPolygonsAnalysisImageFilter2.h"

#include "Metadata/otbMetadataHelper.h"
#include "Common/equal_range_interval.h"
#include "ITKHelpers/otbVLVPointIterator.h"
#include "otbStringUtilities.h"
#include "SAR/otbPositionHelpers.h"
#include "Common/otbStringHelpers.h"

#include "otbLogHelpers.h"  // NeatRegionLogger

#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include <cmath>
#include <type_traits>
#include <cassert>

namespace otb
{
namespace details
{

/**
 * Internal function predicate to compare `itk::VariableLengthVector`
 * components with scalar values.
 */
struct CmpVLVComponent
{
  explicit CmpVLVComponent(unsigned comp) noexcept : m_comp(comp) {}

  template <typename U>
  bool operator()(U d, itk::VariableLengthVector<U> const& v) const noexcept
  { return d < v[m_comp]; }

  template <typename U>
  bool operator()(itk::VariableLengthVector<U> const& v, U d) const noexcept
  { return v[m_comp] < d; }

  template <typename U>
  bool operator()(itk::VariableLengthVector<U> const& v1, itk::VariableLengthVector<U> const& v2) const noexcept
  { return v1[m_comp] < v2[m_comp]; }
private:
  unsigned m_comp;
};

/**
 * Internal helper to return the adress to the first component of an
 * `itk::VariableLengthVector`
 */
template <typename T>
inline
auto as_ptr(itk::VariableLengthVector<T> const& vlv)
{
  return &vlv[0];
}
} // otb::details namespace

/**
 * Set Sar Image Metadata
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn ,TImageOut, TImageDEM, TImageSAR, TFunction >
#if OTB_VERSION_MAJOR >= 8
:: SetSARImageMetadata(ImageMetadata sarImageKWL)
{
  m_SarImageKwl = std::move(sarImageKWL);
}
#else
::SetSARImageKeyWorList(ImageKeywordlist const& sarImageKWL)
{
  m_SarImageKwl = sarImageKWL;
}
#endif

/**
 * Set Sar Image Ptr
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn ,TImageOut, TImageDEM, TImageSAR, TFunction >
::SetSARImagePtr(ImageSARPointer sarPtr)
{
  // Check if sarImageKWL not NULL
  assert(sarPtr && "SAR Image doesn't exist.");
  m_SarImagePtr = sarPtr;
}

/**
 * Set Dem Image Ptr
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn ,TImageOut, TImageDEM, TImageSAR, TFunction >
::SetDEMImagePtr(ImageDEMPointer demPtr)
{
  // Check if sarImageKWL not NULL
  assert(demPtr && "DEM Image doesn't exist.");
  m_DemImagePtr = demPtr;
}

/**
 * Set Dem Image Ptr
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn ,TImageOut, TImageDEM, TImageSAR, TFunction >
::SetFunctorPtr(FunctionPointer functor)
{
  m_FunctionOnPolygon = functor;
}

/**
 * Set DEM Information
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void SARDEMPolygonsAnalysisImageFilter< TImageIn ,TImageOut, TImageDEM, TImageSAR, TFunction >
::SetDEMInformation(double gain, int DEMDirC, int DEMDirL)
{
  // Check if gain is greater than 0
  assert(gain >= 0 && "gain must be greater than 0.");
  // Check if directions are equal to 1 or -1
  assert((DEMDirC == 1 || DEMDirC == -1)  && "direction to scan the DEM in range must be 1 or -1.");
  assert((DEMDirL == 1 || DEMDirL == -1)  && "direction to scan the DEM in aimut must be 1 or -1.");

  m_Gain = gain;
  m_DEMdirL= DEMDirL;
  m_DEMdirC = DEMDirC;
}

/**
 * Initialize margin and GenericRSTransform
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void SARDEMPolygonsAnalysisImageFilter< TImageIn ,TImageOut, TImageDEM, TImageSAR, TFunction >
::initializeMarginAndRSTransform()
{
  // Initialize the GenericRSTransforms
  auto const nbThreads = this->GetNumberOfThreads();

  for (unsigned i=0; i!=nbThreads ; ++i)
  {
    auto transform = RSTransformType2D::New();
#if OTB_VERSION_MAJOR >= 8
    transform->SetInputImageMetadata( &(m_SarImagePtr->GetImageMetadata()) );
#else
    transform->SetInputKeywordList( m_SarImagePtr->GetImageKeywordlist() );
#endif
    transform->SetInputProjectionRef( m_SarImagePtr->GetProjectionRef() );
    transform->SetOutputProjectionRef(m_DemImagePtr->GetProjectionRef());
    transform->InstantiateTransform();

    m_RSTransforms.push_back(transform);
  }

  // Set the margin
  int nbLinesSAR = m_SarImagePtr->GetLargestPossibleRegion().GetSize()[1];
  int nbLinesDEM = m_DemImagePtr->GetLargestPossibleRegion().GetSize()[1];
  if (nbLinesSAR/2 > nbLinesDEM)
  {
    m_Margin = (nbLinesSAR/nbLinesDEM)*30;
  }
  else if (nbLinesSAR > nbLinesDEM)
  {
    m_Margin = (nbLinesSAR/nbLinesDEM)*60;
  }
  else
  {
    m_Margin = 100;
  }
  otbMsgDevMacro(<< "margin used: " << m_Margin);
}

/**
 * Print
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn, TImageOut, TImageDEM, TImageSAR, TFunction >
::PrintSelf(std::ostream & os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);

  os << indent << "Gain : " << m_Gain << std::endl;
  os << indent << "DEM scan direction in range : " << m_DEMdirC << std::endl;
  os << indent << "DEM scan direction in aeimut : " << m_DEMdirL << std::endl;
}

/**
 * Method GenerateOutputInformation()
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn, TImageOut, TImageDEM, TImageSAR, TFunction >
::GenerateOutputInformation()
{
  // Call superclass implementation
  Superclass::GenerateOutputInformation();

  // Get pointers to the input and output
  ImageInConstPointer inputPtr = this->GetInput();
  ImageOutPointer     outputPtr = this->GetOutput();

#if OTB_VERSION_MAJOR >= 8
  auto const& inputKwl = inputPtr->GetImageMetadata();
#else
  ImageKeywordlist inputKwl = inputPtr->GetImageKeywordlist();
#endif
  m_srcNoData   = ExtractNoDataValue(inputKwl);
  otbLogMacro(Info, << "NoData in input SARDEMProjected image: " << m_srcNoData);

  // SAR dimensions
  m_NbLinesSAR = m_SarImagePtr->GetLargestPossibleRegion().GetSize()[1];
  m_NbColSAR   = m_SarImagePtr->GetLargestPossibleRegion().GetSize()[0];

  m_FunctionOnPolygon->SetSARDimensions(m_NbColSAR, m_NbLinesSAR);

  //////// Check Input/Output in function of Functor ////////
  //////// The functor can be required a classic image or a vector Image as main output ////////
  if (strcmp(inputPtr->GetNameOfClass(), "VectorImage")  != 0)
  {
    itkExceptionMacro(<<"Input must be a Vector Image.");
    return;
  }

  unsigned int numberOfExpectedComponents = m_FunctionOnPolygon->GetNumberOfExpectedComponents();
  if (numberOfExpectedComponents > inputPtr->GetNumberOfComponentsPerPixel())
  {
    itkExceptionMacro(<<"Number of Components by pixels of the input is not enough for the used functor ("<< numberOfExpectedComponents << " expected, " << inputPtr->GetNumberOfComponentsPerPixel() << " available.");
    return;
  }

  unsigned int numberOfEstimatedComponents = m_FunctionOnPolygon->GetNumberOfEstimatedComponents();
  if (numberOfEstimatedComponents > 1)
  {
    if (strcmp(outputPtr->GetNameOfClass(), "VectorImage") != 0)
    {
      itkExceptionMacro(<<"Output must be a Vector Image.");
      return;
    }

    // Set the number of Components
    outputPtr->SetNumberOfComponentsPerPixel(numberOfEstimatedComponents);
  }
  else
  {
    if (strcmp(outputPtr->GetNameOfClass(), "Image") != 0)
    {
      itkExceptionMacro(<<"Output must be a Vector Image.");
      return;
    }
  }


  ////////// Check the nature of Components into projeted DEM //////////
  // The functor requires several components (with GetRequiredComponents). These components have to be into
  // projeted DEM and correspond to a band into our input.
#if OTB_VERSION_MAJOR >= 8
  std::string const BandKey = "band.";
#else
  std::string const RootKey = "support_data.";
  std::string const BandKey = RootKey + "band.";
#endif
  std::vector<std::string> vecRequiredComponents = m_FunctionOnPolygon->GetRequiredComponents();
  std::map<std::string, int> mapRequiredComponents;

  for (auto const& requiredComponent : vecRequiredComponents)
  {
    std::string component = BandKey + requiredComponent;
    mapRequiredComponents[requiredComponent] = value_or_throw<int>(inputKwl, component, "(PolygonsAnalysisImageFilter)");
  }

  m_FunctionOnPolygon->SetRequiredComponentsToInd(mapRequiredComponents);


  ////////// Define geometry output with the used functor //////////
  // Two kinds of output geometries : SAR or ML
  m_FunctionOnPolygon->GetOutputGeometry(m_OutGeometry, m_MLRan, m_MLAzi);

  if (m_OutGeometry == "SAR")
  {
    // The output is defined with the m_SarImageKwl
    // Origin, Spacing and Size (SAR image geometry)
    ImageOutSizeType outputSize;
    outputSize[0] = m_NbColSAR;
    outputSize[1] = m_NbLinesSAR;
    ImageOutPointType outOrigin = m_SarImagePtr->GetOrigin();
    ImageOutSpacingType outSP = m_SarImagePtr->GetSignedSpacing();

    // Define Output Largest Region
    ImageOutRegionType outputLargestPossibleRegion = m_SarImagePtr->GetLargestPossibleRegion();
    outputLargestPossibleRegion.SetSize(outputSize);
    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSignedSpacing(outSP);
    outputPtr->SetDirection(m_SarImagePtr->GetDirection());
  }
  else if (m_OutGeometry == "ML")
  {
    // The output is defined with the m_SarImageKwl and by ML factors
    // Origin, Spacing and Size (SAR image geometry) in function of ML factors
    ImageOutSizeType outputSize;

    outputSize[0] = std::max<ImageOutSizeValueType>(std::floor( (double) m_NbColSAR/m_MLRan),   1);
    outputSize[1] = std::max<ImageOutSizeValueType>(std::floor( (double) m_NbLinesSAR/m_MLAzi), 1);

    ImageOutPointType outOrigin = m_SarImagePtr->GetOrigin();
    ImageOutSpacingType outSP = m_SarImagePtr->GetSignedSpacing();
    outSP[0] *= m_MLRan;
    outSP[1] *= m_MLAzi;

    // Define Output Largest Region
    ImageOutRegionType outputLargestPossibleRegion = m_SarImagePtr->GetLargestPossibleRegion();
    outputLargestPossibleRegion.SetSize(outputSize);
    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSignedSpacing(outSP);
    outputPtr->SetDirection(m_SarImagePtr->GetDirection());

#if OTB_VERSION_MAJOR >= 8
    // Add ML factors into Metadata
    m_SarImageKwl.Add("ml_ran", std::to_string(m_MLRan));
    m_SarImageKwl.Add("ml_azi", std::to_string(m_MLAzi));
#else
    // Add ML factors into keyWordList
    m_SarImageKwl.AddKey(RootKey + "ml_ran", std::to_string(m_MLRan));
    m_SarImageKwl.AddKey(RootKey + "ml_azi", std::to_string(m_MLAzi));
#endif
  }
  else
  {
    itkExceptionMacro(<<"Wrong Output Geometry: '" << m_OutGeometry << "'.");
    return;
  }

  BandInformation bands(m_SarImageKwl, BandInformation::clear);

  ////////// Add the estimated bands to Metadata //////////
  std::vector<std::string> vecEstimatedComponents = m_FunctionOnPolygon->GetEstimatedComponents();

  for (unsigned int i = 0; i < vecEstimatedComponents.size(); i++)
  {
    std::string key = BandKey + vecEstimatedComponents[i];
#if OTB_VERSION_MAJOR >= 8
    m_SarImageKwl.Add(key, std::to_string(i));
#else
    m_SarImageKwl.AddKey(key, std::to_string(i));
#endif
    bands.add_new(vecEstimatedComponents[i], m_srcNoData);
  }
  bands.write_back();

  // Set new metadata to output image
#if OTB_VERSION_MAJOR >= 8
  outputPtr->SetImageMetadata(m_SarImageKwl);
#else
  outputPtr->SetImageKeywordList(m_SarImageKwl);
#endif

  // Change Projection to fit with SAR image
  auto const sar_projection = m_SarImagePtr->GetProjectionRef();
  // This is likelly to be empty for a SAR Input
  otbMsgDebugMacro(<<"SARDEMPolygonsAnalysisImageFilter::GenerateOutputInformation -> SAR ProjectionRef: " << sar_projection);
#if OTB_VERSION_MAJOR >= 8
  if (!sar_projection.empty())
  {
    // Work around OTB 8.1.2 bug !976 by not setting empty projection
    outputPtr->SetProjectionRef(sar_projection);
  }
#else
  // Even empty, it seems required with OTB 7/ Ossim
  outputPtr->SetProjectionRef(sar_projection);
#endif
}

/**
 * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
typename SARDEMPolygonsAnalysisImageFilter< TImageIn, TImageOut, TImageDEM, TImageSAR, TFunction >::ImageInRegionType
SARDEMPolygonsAnalysisImageFilter< TImageIn, TImageOut, TImageDEM, TImageSAR, TFunction >
::OutputRegionToInputRegion(
    ImageOutRegionType const& outputRegion,
    itk::ThreadIdType         threadId) const
{
  // Compute the input requested region (size and start index)
  // Use the image transformations to insure an input requested region
  // that will provide the proper range
  ImageOutSizeType  outputRequestedRegionSize = outputRegion.GetSize();
  ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();

  // Here we are breaking traditional pipeline steps because we need to access the input field data
  // so as to compute the input image requested region
  // The inverse localisation (SAR to DEM) is performed in order to determine the DEM region (input)
  // represented by the SAR region (output)
  int nbColsDEM = m_DemImagePtr->GetLargestPossibleRegion().GetSize()[0];
  int nbLinesDEM = m_DemImagePtr->GetLargestPossibleRegion().GetSize()[1];

  // Margin to apply on inverse localisation for the four sides.
  int margin = 100;
  if (std::max(nbColsDEM, nbLinesDEM) > 2000)
  {
    margin = std::max(nbColsDEM, nbLinesDEM)*0.01; // %1 of the largest dimension
  }
  if (margin < 100)
  {
    margin = 100;
  }

  // Indice for the SAR bloc (determined by the Pipeline)
  ImageOutIndexType id[4] ;
  if (m_OutGeometry == "SAR")
  {
    id[0][0] = outputRequestedRegionIndex[0];
    id[0][1] = outputRequestedRegionIndex[1];
    id[1][0] = outputRequestedRegionIndex[0];
    id[1][1] = outputRequestedRegionIndex[1] + outputRequestedRegionSize[1];
    id[2][0] = outputRequestedRegionIndex[0] + outputRequestedRegionSize[0];
    id[2][1] = outputRequestedRegionIndex[1];
    id[3][0] = outputRequestedRegionIndex[0] + outputRequestedRegionSize[0];
    id[3][1] = outputRequestedRegionIndex[1] + outputRequestedRegionSize[1];
  }
  else if (m_OutGeometry == "ML")
  {
    id[0][0] = outputRequestedRegionIndex[0]*m_MLRan;
    id[0][1] = outputRequestedRegionIndex[1]*m_MLAzi;
    id[1][0] = outputRequestedRegionIndex[0]*m_MLRan;
    id[1][1] = (outputRequestedRegionIndex[1] + outputRequestedRegionSize[1])*m_MLAzi;
    id[2][0] = (outputRequestedRegionIndex[0] + outputRequestedRegionSize[0])*m_MLRan;
    id[2][1] = outputRequestedRegionIndex[1]*m_MLAzi;
    id[3][0] = (outputRequestedRegionIndex[0] + outputRequestedRegionSize[0])*m_MLRan;
    id[3][1] = (outputRequestedRegionIndex[1] + outputRequestedRegionSize[1])*m_MLAzi;

    // Check Size
    if (id[1][1] > m_NbLinesSAR)
    {
      id[1][1] = m_NbLinesSAR;
      id[3][1] = m_NbLinesSAR;
    }
    if (id[2][0] > m_NbColSAR)
    {
      id[2][0] = m_NbColSAR;
      id[3][0] = m_NbColSAR;
    }
  }


  // Initialize the first and last DEM indice
  int firstL = nbLinesDEM;
  int lastL = 0;
  int firstC = nbColsDEM;
  int lastC = 0;


  // For each side of the output region
  for (int i = 0; i < 4; i++)
  {
    // Index to physical
    auto const pixelSAR = TransformIndexToPhysicalPoint(*m_SarImagePtr, id[i]);

    // Call TransformPoint with physical point
    auto const pixelSAR_Into_MNT_LatLon = m_RSTransforms[threadId]->TransformPoint(pixelSAR);

    // Transform Lat/Lon to index
    auto const pixelSAR_Into_MNT = TransformPhysicalPointToContinuousIndex<double>(*m_DemImagePtr, pixelSAR_Into_MNT_LatLon);

    // Assign the limits
    if (firstL > pixelSAR_Into_MNT[1])
    {
      firstL = pixelSAR_Into_MNT[1];
    }

    if (lastL < pixelSAR_Into_MNT[1])
    {
      lastL = pixelSAR_Into_MNT[1];
    }

    if (firstC > pixelSAR_Into_MNT[0])
    {
      firstC = pixelSAR_Into_MNT[0];
    }

    if (lastC < pixelSAR_Into_MNT[0])
    {
      lastC = pixelSAR_Into_MNT[0];
    }
  }

  // Apply the marge
  firstL -= margin;
  firstC -= margin;
  lastL +=  margin;
  lastC +=  margin;

  // Check the limits
  if (firstC < 0)
  {
    firstC = 0;
  }
  if (firstL < 0)
  {
    firstL = 0;
  }
  if (lastC > nbColsDEM-1)
  {
    lastC = nbColsDEM-1;
  }
  if (lastL > nbLinesDEM-1)
  {
    lastL = nbLinesDEM-1;
  }


  // Transform sides to region
  ImageInIndexType inputRequestedRegionIndex;
  inputRequestedRegionIndex[0] = static_cast<ImageInIndexValueType>(firstC);
  inputRequestedRegionIndex[1] = static_cast<ImageInIndexValueType>(firstL);
  ImageInSizeType inputRequestedRegionSize;
  inputRequestedRegionSize[0] = static_cast<ImageInIndexValueType>(lastC - firstC)+1;
  inputRequestedRegionSize[1] = static_cast<ImageInIndexValueType>(lastL - firstL)+1;

  ImageInRegionType inputRequestedRegion(inputRequestedRegionIndex, inputRequestedRegionSize);
  // assert(this->GetInput()->GetBufferedRegion().IsInside(inputRequestedRegion));

  return inputRequestedRegion;
}

/**
 * Method GenerateInputRequestedRegion
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn, TImageOut, TImageDEM, TImageSAR, TFunction >
::GenerateInputRequestedRegion()
{
  // call the superclass' implementation of this method
  Superclass::GenerateInputRequestedRegion();

  ///////////// Set Output Requestion Region (Strips) ///////////////
  ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

  ///////////// With the new output requested region, find the region into Projeted DEM /////////////
  ImageInRegionType inputRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion, 0);
  ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetInput() );
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}


/**
 * Method PolygonAndSarLineIntersection
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
bool
SARDEMPolygonsAnalysisImageFilter< TImageIn, TImageOut, TImageDEM, TImageSAR, TFunction >
::PolygonAndSarLineIntersection(const int ind_LineSAR,
      const ImageInSubPixelType ligUL, const ImageInSubPixelType colUL,
      const ImageInSubPixelType ligUR, const ImageInSubPixelType colUR,
      const ImageInSubPixelType ligLR, const ImageInSubPixelType colLR,
      const ImageInSubPixelType ligLL, const ImageInSubPixelType colLL,
      int & i_InSideUp, int & i_InSideDown,
      int & i_OutSideUp, int & i_OutSideDown)
{
  auto const Lmax_ulur = std::max(ligUL, ligUR);
  auto const Lmin_ulur = std::min(ligUL, ligUR);

  auto const Lmax_urlr = std::max(ligLR, ligUR);
  auto const Lmin_urlr = std::min(ligLR, ligUR);

  auto const Lmax_lllr = std::max(ligLL, ligLR);
  auto const Lmin_lllr = std::min(ligLL, ligLR);

  auto const Lmax_ulll = std::max(ligUL, ligLL);
  auto const Lmin_ulll = std::min(ligUL, ligLL);

  // Selection for ind_LineSAR intersection
  auto const intersect_ulur = Lmax_ulur >= ind_LineSAR && Lmin_ulur <= ind_LineSAR;
  auto const intersect_urlr = Lmax_urlr >= ind_LineSAR && Lmin_urlr <= ind_LineSAR;
  auto const intersect_lllr = Lmax_lllr >= ind_LineSAR && Lmin_lllr <= ind_LineSAR;
  auto const intersect_ulll = Lmax_ulll >= ind_LineSAR && Lmin_ulll <= ind_LineSAR;
  auto const as_int  = [](bool b){ return b ? 1U : 0U;};

  auto const nb_intersects = as_int(intersect_ulur) + as_int(intersect_urlr)
    + as_int(intersect_lllr) + as_int(intersect_ulll) ;

  if (nb_intersects <= 1 || nb_intersects == 4)
  {
    return false;
  }

  unsigned int countSides = 0;
  ImageInSubPixelType CLZY_Up [4][2];
  ImageInSubPixelType CLZY_Down [4][2];
  int i_Up [4];
  int i_Down [4];

  // Select if two sides is intersected by the current lineSar (ind_LineSAR)
  if (intersect_ulur)
  {
    if (ligUR > ligUL)
    {
      CLZY_Up[countSides][0] = colUR;
      CLZY_Up[countSides][1] = ligUR;
      i_Up[countSides] = 1;
      CLZY_Down[countSides][0] = colUL;
      CLZY_Down[countSides][1] = ligUL;
      i_Down[countSides] = 0;
    }
    else
    {
      CLZY_Up[countSides][0] = colUL;
      CLZY_Up[countSides][1] = ligUL;
      i_Up[countSides] = 0;
      CLZY_Down[countSides][0] = colUR;
      CLZY_Down[countSides][1] = ligUR;
      i_Down[countSides] = 1;
    }
    ++countSides;
  }

  if (intersect_urlr)
  {
    if (ligLR > ligUR)
    {
      CLZY_Up[countSides][0] = colLR;
      CLZY_Up[countSides][1] = ligLR;
      i_Up[countSides] = 2;
      CLZY_Down[countSides][0] = colUR;
      CLZY_Down[countSides][1] = ligUR;
      i_Down[countSides] = 1;
    }
    else
    {
      CLZY_Up[countSides][0] = colUR;
      CLZY_Up[countSides][1] = ligUR;
      i_Up[countSides] = 1;
      CLZY_Down[countSides][0] = colLR;
      CLZY_Down[countSides][1] = ligLR;
      i_Down[countSides] = 2;
    }
    ++countSides;
  }

  if (intersect_lllr)
  {
    if (ligLR > ligLL)
    {
      CLZY_Up[countSides][0] = colLR;
      CLZY_Up[countSides][1] = ligLR;
      i_Up[countSides] = 2;
      CLZY_Down[countSides][0] = colLL;
      CLZY_Down[countSides][1] = ligLL;
      i_Down[countSides] = 3;
    }
    else
    {
      CLZY_Up[countSides][0] = colLL;
      CLZY_Up[countSides][1] = ligLL;
      i_Up[countSides] = 3;
      CLZY_Down[countSides][0] = colLR;
      CLZY_Down[countSides][1] = ligLR;
      i_Down[countSides] = 2;
    }
    ++countSides;
  }

  if (intersect_ulll)
  {
    if (ligUL > ligLL)
    {
      CLZY_Up[countSides][0] = colUL;
      CLZY_Up[countSides][1] = ligUL;
      i_Up[countSides] = 0;
      CLZY_Down[countSides][0] = colLL;
      CLZY_Down[countSides][1] = ligLL;
      i_Down[countSides] = 3;
    }
    else
    {
      CLZY_Up[countSides][0] = colLL;
      CLZY_Up[countSides][1] = ligLL;
      i_Up[countSides] = 3;
      CLZY_Down[countSides][0] = colUL;
      CLZY_Down[countSides][1] = ligUL;
      i_Down[countSides] = 0;
    }
    ++countSides;
  }

  // If good Intesection (select input and output side)
  assert(countSides == nb_intersects);
  // Intersection with two sides or three sides
  assert(countSides ==  2 || countSides == 3);

  if (countSides == 2)
  {
    i_InSideUp = i_Up[0];
    i_InSideDown  = i_Down[0];
    i_OutSideUp = i_Up[1];
    i_OutSideDown = i_Down[1];
  }
  else // if (countSides == 3)
  {
    // Select extremes colunms among the three CLZY points to cover the biggest part of SAR Image
    auto const Cmax_0 = std::max(CLZY_Up[0][0], CLZY_Down[0][0]);
    auto const Cmin_0 = std::min(CLZY_Up[0][0], CLZY_Down[0][0]);
    auto const Cmax_1 = std::max(CLZY_Up[1][0], CLZY_Down[1][0]);
    auto const Cmin_1 = std::min(CLZY_Up[1][0], CLZY_Down[1][0]);
    auto const Cmax_2 = std::max(CLZY_Up[2][0], CLZY_Down[2][0]);
    auto const Cmin_2 = std::min(CLZY_Up[2][0], CLZY_Down[2][0]);

    // Max colunm
    if (Cmax_0 > Cmax_1 && Cmax_0 > Cmax_2)  // Cmax_0 is the max
    {
      i_InSideUp = i_Up[0];
      i_InSideDown  = i_Down[0];

      // Min colunm
      // TODO: can be simplified to idx = 1 if Cmin_1 < Cmax_2 else 2
      if (Cmin_0 < Cmin_1 && Cmin_0 < Cmin_2) // Cmin_0 is the min
      {
        if (Cmin_1 < Cmin_2) // Cmin_0 < Cmin_1 < Cmin_2
        {
          i_OutSideUp = i_Up[1];
          i_OutSideDown  = i_Down[1];
        }
        else                 // Cmin_0 < Cmin_2 < Cmin_1
        {
          i_OutSideUp = i_Up[2];
          i_OutSideDown  = i_Down[2];
        }
      }
      else if (Cmin_1 < Cmin_0 && Cmin_1 < Cmin_2) // Cmin_1 is the min
      {
        i_OutSideUp = i_Up[1];
        i_OutSideDown  = i_Down[1];
      }
      else // Cmin_2 is the min
      {
        i_OutSideUp = i_Up[2];
        i_OutSideDown  = i_Down[2];
      }
    }
    else if (Cmax_1 > Cmax_0 && Cmax_1 > Cmax_2) // Cmax_1 is tha max
    {
      i_InSideUp = i_Up[1];
      i_InSideDown  = i_Down[1];

      // Min colunm
      // TODO: can be simplified to idx = 0 if Cmin_0 < Cmax_2 else 2
      if (Cmin_0 < Cmin_1 && Cmin_0 < Cmin_2)
      {
        i_OutSideUp = i_Up[0];
        i_OutSideDown  = i_Down[0];
      }
      else if (Cmin_1 < Cmin_0 && Cmin_1 < Cmin_2)
      {
        if (Cmin_0 < Cmin_2)
        {
          i_OutSideUp = i_Up[0];
          i_OutSideDown  = i_Down[0];
        }
        else
        {
          i_OutSideUp = i_Up[2];
          i_OutSideDown  = i_Down[2];
        }
      }
      else
      {
        i_OutSideUp = i_Up[2];
        i_OutSideDown  = i_Down[2];
      }
    }
    else
    {
      i_InSideUp = i_Up[2];
      i_InSideDown  = i_Down[2];

      // Min colunm
      // TODO: can be simplified to idx = 0 if Cmin_0 < Cmax_1 else 1
      if (Cmin_0 < Cmin_1 && Cmin_0 < Cmin_2) // Cmin_0 is the min
      {
        i_OutSideUp = i_Up[0];
        i_OutSideDown  = i_Down[0];
      }
      else if (Cmin_1 < Cmin_0 && Cmin_1 < Cmin_2) // Cmin_1 is the min
      {
        i_OutSideUp = i_Up[1];
        i_OutSideDown  = i_Down[1];
      }
      else // Cmin_2 is the min
      {
        if (Cmin_0 < Cmin_1)
        {
          i_OutSideUp = i_Up[0];
          i_OutSideDown  = i_Down[0];
        }
        else
        {
          i_OutSideUp = i_Up[1];
          i_OutSideDown  = i_Down[1];
        }
      }
    }
  } // countSides == 3
  return true;
}

/**
 * Method DEMPolygonsAnalysis
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn, TImageOut, TImageDEM, TImageSAR, TFunction >
::PolygonsScan(
    const int ind_LineSAR,
    std::vector<std::vector<FastPixelType> const*> const& CLZY_InSideUp_Vec,
    std::vector<std::vector<FastPixelType> const*> const& CLZY_InSideDown_Vec,
    std::vector<std::vector<FastPixelType> const*> const& CLZY_OutSideUp_Vec,
    std::vector<std::vector<FastPixelType> const*> const& CLZY_OutSideDown_Vec,
    int firstCol_into_outputRegion,
    otb::Span<ImageOutPixelType> outValueTab,
    int threadId)
{
  ImageInConstPointer inputPtr = this->GetInput();
  auto const nbcompo = inputPtr->GetNumberOfComponentsPerPixel();
  bool isFirstPolygon = true;

  // Elevation angle max (for shadows)
  double tgElevationMaxForCurrentLine = 0;

  int const nbLinesIntoDEM_withSelectedPolygon = CLZY_InSideUp_Vec.size();

  // Memorize the "external" member variables.
  // * -1 index descreases, +1 index increases
  // * Ideally it should be more efficient to have 4 pairs of loop
  //   It should be easier to acheive w/ 2 lambdas, but PolygonsScan() isn't a
  //   bottleneck and doesn't deserve obfuscation. Let's bet on branch
  //   prediction.
  auto const shallIncreaseDirectionL = m_DEMdirL == 1;
  auto const shallIncreaseDirectionC = m_DEMdirC == 1;

  int ind_LDEM, ind_CDEM;

  // Loop on polygons (for lines into DEM with al least one selected polygon)
  // the loop direction depends on m_DEMdirL
  for (int i = 0 ; i < nbLinesIntoDEM_withSelectedPolygon; i++)
  {
    if (shallIncreaseDirectionL)
    {
      ind_LDEM = i;
    }
    else
    {
      ind_LDEM = nbLinesIntoDEM_withSelectedPolygon - (i+1);
    }

    // for colunms into DEM with selected polygons, depends on m_DEMdirC
    int const nbColIntoDEM_withSelectedPolygon = CLZY_InSideUp_Vec[ind_LDEM]->size();
    for (int j = 0; j <  nbColIntoDEM_withSelectedPolygon; j++)
    {
      if (shallIncreaseDirectionC)
      {
        ind_CDEM = j;
      }
      else
      {
        ind_CDEM = nbColIntoDEM_withSelectedPolygon - (j+1);
      }

      // Select the CLY_InSideUp, CLY_InSideDown, CLY_OutSideUp and CLY_OutSideDown and
      // estimate the wanted value (for example amplitude)
      auto as_vlv = [nbcompo](FastPixelType p) {
        return ImageInPixelType(p, nbcompo, false);
      };
      m_FunctionOnPolygon->estimate(ind_LineSAR,
          as_vlv((*CLZY_InSideUp_Vec   [ind_LDEM])[ind_CDEM]),
          as_vlv((*CLZY_InSideDown_Vec [ind_LDEM])[ind_CDEM]),
          as_vlv((*CLZY_OutSideUp_Vec  [ind_LDEM])[ind_CDEM]),
          as_vlv((*CLZY_OutSideDown_Vec[ind_LDEM])[ind_CDEM]),
          firstCol_into_outputRegion, outValueTab, isFirstPolygon,
          tgElevationMaxForCurrentLine, threadId);

      isFirstPolygon = false;
    }
  }
}

template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn, TImageOut, TImageDEM, TImageSAR, TFunction >
::BeforeThreadedGenerateData()
{
  auto const* input                  = this->GetInput();
  assert(input);
  auto const  nBands                 = input->GetNumberOfComponentsPerPixel();
  auto const& inputRegion            = input->GetRequestedRegion();  // for  the stream
  auto const& in_size                = inputRegion.GetSize();
  auto const& in_index               = inputRegion.GetIndex();
  auto const  in_sizeX               = in_size[0];
  auto const  in_sizeY               = in_size[1];
  // auto const  in_startX              = in_index[0];
  auto const  in_startY              = in_index[1];
  // itk::IndexValueType const in_endX  = in_startX + in_sizeX;
  itk::IndexValueType const in_endY  = in_startY + in_sizeY;
  // auto const   in_full_line          = in_sizeX * nBands;
  ImageInSubPixelType const *in_data = input->GetBufferPointer();
  unsigned long const in_line_off    = nBands * input->GetBufferedRegion().GetSize(0);

  m_is_lineX_sorted.clear(); // To be sure its all false at the start of the region!
  m_is_lineX_sorted.resize(in_endY, false);

  auto point = [=](unsigned dx, unsigned y) {
    return VLVPointIterator<ImageInSubPixelType const>(in_data + nBands*dx + (y-in_startY)*in_line_off, nBands);
  };  // ------------------------------[ point
  auto line = [](VLVPointIterator<ImageInSubPixelType const> it) {
    return (*it)[1];
  };  // ------------------------------[ line

  // For 1 minute of execution, this function takes less than 0.5 seconds.
  // => there is no need to optimize it more (parallelization, or skipping of
  // lines already analysed)
  m_all_lines_are_sorted = true;
  bool last_was_ascending=false, last_was_descending=false;
  for (unsigned y = in_startY ; y < in_endY ; ++y)
  {
    int nb_up = 0;
    int nb_dw = 0;
    auto prev = line(point(0, y));
    for (unsigned dx = 0 ; dx < in_sizeX ; ++dx)
    {
      auto const UL_line = line(point(dx, y));
      if (UL_line > prev) ++nb_up;
      else                ++nb_dw;
      if (nb_up > 0 && nb_dw > 0)
      { // no need to continue to scan the line!
        break;
      }
      prev = UL_line;
    }
    if (nb_up == 0 || nb_dw == 0) {
      m_is_lineX_sorted[y] = true;
      last_was_ascending = nb_up > 0 && nb_dw == 0;
      last_was_descending = nb_up == 0 && nb_dw > 0;
    } else {
      m_all_lines_are_sorted = false;
    }
  }
  otbMsgDevMacro("BeforeThreadedGenerateData: in current block: all lines are sorted: " << m_all_lines_are_sorted);
  otbMsgDevMacro("BeforeThreadedGenerateData: order is " << (last_was_ascending ? "ASC" : last_was_descending ? "DSC" : "???"));

  otbMsgDevMacro("BeforeThreadedGenerateData: buffered region: " << NeatRegionLogger{input->GetBufferedRegion()});
  otbMsgDevMacro("BeforeThreadedGenerateData: input    region: " << NeatRegionLogger{inputRegion});
}

/**
 * Method ThreadedGenerateData
 */
template<class TImageIn, class TImageOut, class TImageDEM, class TImageSAR, class TFunction>
void
SARDEMPolygonsAnalysisImageFilter< TImageIn, TImageOut, TImageDEM, TImageSAR, TFunction >
::ThreadedGenerateData(
    ImageOutRegionType const& outputRegionForThread,
    itk::ThreadIdType         threadId)
{
  auto const& origin = this->GetOutput()->GetOrigin();
  // Compute corresponding input region
  ImageInRegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread, threadId);

  // Iterator on output (SAR geometry)
  OutputIterator OutIt(this->GetOutput(), outputRegionForThread);

  OutIt.GoToBegin();

  const ImageOutSizeType &  outputRegionForThreadSize = outputRegionForThread.GetSize();
  unsigned int const nbCol    = static_cast<unsigned int>(outputRegionForThreadSize[0]);
  auto         const nblines  = outputRegionForThreadSize[1];
  unsigned int const firstCol = static_cast<unsigned int>(outputRegionForThread.GetIndex()[0]+origin[0]);

  ImageInSizeType const & inputRegionForThreadSize = inputRegionForThread.GetSize();
  unsigned int    const   nbDEMLines   =  static_cast<unsigned int>(inputRegionForThreadSize[1]);
  // unsigned int    const   nbDEMColumns =  static_cast<unsigned int>(inputRegionForThreadSize[0]);
  // otbMsgDevMacro("ThreadedGenerateData: input region #"<<threadId<<" : " << NeatRegionLogger{inputRegionForThread});
  assert(this->GetInput()->GetBufferedRegion().IsInside(inputRegionForThread));


  // Allocate outValueTab (size = nbCol)
  // ImageOutPixelType *outValueTab = new ImageOutPixelType[nbCol];
  std::vector<ImageOutPixelType> outValueTab(nbCol);

  // Allocate Vectors for polygon storage (vector of vectors to separate line and colunm of polygon into DEM)
  std::vector<std::vector<FastPixelType> const*> Polygon_SideInUp_Vec;
  std::vector<std::vector<FastPixelType> const*> Polygon_SideInDown_Vec;
  std::vector<std::vector<FastPixelType> const*> Polygon_SideOutUp_Vec;
  std::vector<std::vector<FastPixelType> const*> Polygon_SideOutDown_Vec;

  // A polygon is composed of four lines
  // => four sides (UL UR LR LL) : UL = current_point
  //                               UR = current_point + 1 colunm
  //                               LR = current_point + 1 colunm and 1 line
  //                               LL = current_point + 1 line
  // To construct all polygons of input region, four iterators are necessary
  auto const* input  = this->GetInput();
  auto const  nBands = input->GetNumberOfComponentsPerPixel();

  auto const& in_size                = inputRegionForThread.GetSize();
  auto const& in_index               = inputRegionForThread.GetIndex();
  auto const  in_sizeX               = in_size[0];
  auto const  in_sizeY               = in_size[1];
  // auto const  in_startX              = in_index[0];
  auto const  in_startY              = in_index[1];
  // itk::IndexValueType const in_endX  = in_startX + in_sizeX;
  // itk::IndexValueType const in_endY  = in_startY + in_sizeY;
  // auto const   in_full_line          = in_sizeX * nBands;
  ImageInSubPixelType const *in_data = input->GetBufferPointer();
  // number of Float/Double par buffered region line
  unsigned long const in_line_off    = nBands * input->GetBufferedRegion().GetSize(0);

  // Make in_data point to the first pixel in the input_region_for_thread
  in_data += nBands * input->ComputeOffset(in_index);

  /** Returns an iterator to the (dx,dy) pixel in inputRegionForThread. */
  auto point = [=](unsigned dx, unsigned dy) {
    return VLVPointIterator<ImageInSubPixelType const>(in_data + nBands*dx + dy*in_line_off, nBands);
  };

  // Allocate Vectors for polygon storage for this line of DEM
  std::vector<std::vector<FastPixelType>> Polygon_SideInUp_VecLine   (nbDEMLines);
  std::vector<std::vector<FastPixelType>> Polygon_SideInDown_VecLine (nbDEMLines);
  std::vector<std::vector<FastPixelType>> Polygon_SideOutUp_VecLine  (nbDEMLines);
  std::vector<std::vector<FastPixelType>> Polygon_SideOutDown_VecLine(nbDEMLines);

  // Memorize member variables so they can be optimized in loops
  auto const margin                = m_Margin;
  ImageInSubPixelType const nodata = m_srcNoData;

  itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() / nblines );

  // For each line of output : construct polygon after polygon, determine if polygon intersects the current
  // line and estimate the amplitude with the contribution of all selected polygon
  for (OutIt.GoToBeginOfLine(); !OutIt.IsAtEnd() ; OutIt.NextLine())
  {
    // indice of current line (into output Geometry)
    //~ int ind_Line = OutIt.GetIndex()[1] + int(origin[1]);

    // The Projeted DEM Pixel/Bands is always into SAR geometry => must be adapted for mlRan and mlAzi != 1
    // If mlRan and mlAzi == 1 same as SAR geometry (loop on ind_Line_start=ind_Line only)
    int ind_Line_start = OutIt.GetIndex()[1]     * m_MLAzi + int(origin[1]);
    int ind_Line_end   = (OutIt.GetIndex()[1]+1) * m_MLAzi + int(origin[1]);
    if (ind_Line_end > (m_NbLinesSAR + int(origin[1])))
    {
      ind_Line_end = m_NbLinesSAR + int(origin[1]);
    }

    // Initialize outValueTab
    m_FunctionOnPolygon->initalize(outValueTab, threadId);

    // Loop on Line into SAR Geometry
    for (int ind_Line_Into_SARGeo = ind_Line_start; ind_Line_Into_SARGeo < ind_Line_end;
        ind_Line_Into_SARGeo++)
    {
      ImageInSubPixelType const lo_margin = ind_Line_Into_SARGeo - margin;
      ImageInSubPixelType const hi_margin = ind_Line_Into_SARGeo + margin;
      ///////////////////////////// Polygon Construction and Check intersection /////////////////////////
      // Using the input region associated to the output line doesn't make a
      // big difference...
      ImageOutRegionType outputRegionForLine(
          OutIt.GetIndex(),
          {outputRegionForThread.GetSize()[0], 1});
      ImageInRegionType inputRegionForWindow = OutputRegionToInputRegion(outputRegionForLine, threadId);

      int countDEMLine = 0;

      // For each Line of input
      for (std::size_t dy = 0 ; dy < in_sizeY - 1 ; ++dy)
      {
        bool has_match = false;

        auto register_pixel = [&](std::size_t dx, std::size_t dy, auto const& pt_UL)
        {
          auto const pt_UR = *point(dx+1, dy  );
          auto const pt_LL = *point(dx  , dy+1);
          auto const pt_LR = *point(dx+1, dy+1);

          auto const UL_col = pt_UL[0];
          auto const UR_col = pt_UR[0];
          auto const LR_col = pt_LR[0];
          auto const LL_col = pt_LL[0];

          // Check if No Data
          if (!(UL_col == nodata || UR_col == nodata || LR_col == nodata || LL_col == nodata))
          {
            // Determine if intersection
            auto const UL_line = pt_UL[1];
            auto const UR_line = pt_UR[1];
            auto const LR_line = pt_LR[1];
            auto const LL_line = pt_LL[1];

            // Pixel of otb::VectorImage (input) : 4 Components : C, L, Z and Y
            int i_InSideUp, i_InSideDown, i_OutSideUp, i_OutSideDown;

            // With lig and col of UL, UR, LR and LL select iterators for InSide and Outside of
            // each polygon (if intersection)
            bool const hasIntersection = this->PolygonAndSarLineIntersection(
                ind_Line_Into_SARGeo,
                UL_line, UL_col,
                UR_line, UR_col,
                LR_line, LR_col,
                LL_line, LL_col,
                i_InSideUp,
                i_InSideDown,
                i_OutSideUp,
                i_OutSideDown);

            // If intersection == true : Retrive the intersected sides (two intersected among
            // the four sides of the polygon)
            if (hasIntersection)
            {
              using details::as_ptr;
              FastPixelType tabInPt[] = { as_ptr(pt_UL), as_ptr(pt_UR), as_ptr(pt_LR), as_ptr(pt_LL)};

              // Store the polygons
              Polygon_SideInUp_VecLine   [countDEMLine].push_back(tabInPt[i_InSideUp]);
              Polygon_SideInDown_VecLine [countDEMLine].push_back(tabInPt[i_InSideDown]);
              Polygon_SideOutUp_VecLine  [countDEMLine].push_back(tabInPt[i_OutSideUp]);
              Polygon_SideOutDown_VecLine[countDEMLine].push_back(tabInPt[i_OutSideDown]);
            }
          }
        }; // ------------------------------[ register_pixel

        // assume for now progression is always monotonic
        // TODO: precheck the input window to fall back to the old input method
        // otherwise
        /** Returns whether line ∈ [lo_margin, hi_margin] */
        auto is_in_window = [lo_margin, hi_margin](ImageInSubPixelType line)
        {
          static_assert(std::is_same<typename std::remove_cv<decltype(lo_margin)>::type, ImageInSubPixelType>::value, "Types shall match!");
          return lo_margin < line && line < hi_margin;
        }; // ------------------------------[ is_in_window
        /** Extracts the "line" information from the SARDEMProjected input pixel */
        auto line = [](VLVPointIterator<ImageInSubPixelType const> it)
        {
          return (*it)[1]; // L is at index #1
        };  // ------------------------------[ line

        bool const is_line_monotonic = m_all_lines_are_sorted || m_is_lineX_sorted[dy + in_startY];
        if (is_line_monotonic)
        {
          // In case the input line is sorted, we can search for [lo, hi]
          // interval in a dichtomic way O(ln n).

          /** Wrapper around `equal_range_interval` that masks the
           * increasing/decreasing order.
           * @return the index of the first pixel and of the last pixel in the
           * line that belongs to the searched interval.
           */
          auto find_lo_hi_window = [lo_margin, hi_margin, line, is_in_window](auto line_start, auto line_end)
          {
            if (line(line_start) < line(line_end))
            {
              return equal_range_interval(
                  line_start, line_end,
                  lo_margin,  hi_margin, details::CmpVLVComponent(1));
            }
            else
            {
              auto const lo_hi = equal_range_interval(
                  std::make_reverse_iterator(line_end),
                  std::make_reverse_iterator(line_start),
                  lo_margin,  hi_margin, details::CmpVLVComponent(1));
              auto const lo = lo_hi.first;
              auto const hi = lo_hi.second;
              // std::cout
                // << /*&(*lo)[0] <<*/ "(" << (*lo)[1] << '/' << line(lob) << ") -- "
                // << /*&(*hi)[0] <<*/ "(" << (*hi)[1] << '/' << line(hib) << ")\t";
              return std::make_pair(hi.base(), lo.base());
            }
          }; // ------------------------------[ find_lo_hi_window

          auto const line_start = point(0, dy);
          auto const line_end   = point(in_sizeX, dy);
          auto const window = find_lo_hi_window(line_start, line_end-1); // -1 because UL is at -1, and UR at +0
          auto first = window.first;
          auto last  = window.second;

          // DiapOTB only keeps line in ]lo_margin, hi_margin[ but
          // equal_range/lower_bound returns [it_lo, it_hi[
          // Also in the end after reversing, we need to return range that
          // exclude hi, and keep lo
          while (first < last  && !is_in_window(line(first)))
          {
            ++first;
          }
          //
          if (last == line_end - 2 && is_in_window(line(last)))
          { // make "last" past-end
            ++last;
            assert(last-line_start == long(in_sizeX - 1));
          }
          else
          {
            while (first < last && !is_in_window(line(last-1)))
            {
              --last;
            }
          }

          // std::cout << "#y="<<(dy+in_startY) << "\t; start[0]="<<(*point(0, dy))[1]
          //   << "\t; end[" << (in_sizeX-1) << "]= " << (*point(in_sizeX-1, dy))[1]
          //   << "\t VS ["<<lo_margin<<", " << hi_margin << "]"
          //   << "\t -> " << (first - line_start) << " -- " << (last - line_start)
          //   << "\n";
          has_match = first.data() != last.data();
          if (has_match)
          { // a window has been found!
            assert(is_in_window(line(line_end - 2)) == (last == line_end - 1));

            if (first > line_start)
            { // assert before is outside
              assert(!is_in_window(line(first-1)));
            }
            if (last < (line_end-1))
            { // assert after is outside
              assert(!is_in_window(line(last)));
            }
            // after within is inside
            assert(is_in_window(line(first)));
            if (last > line_start)
            {
              assert(is_in_window(line(last-1)));
            }

            assert((last-line_start) <= long(in_sizeX - 1));
            for (auto dx = (first-line_start); dx < (last-line_start) ; ++dx)
            {
              assert(dx < long(in_sizeX - 1));
              auto const pt_UL = *point(dx, dy);
              auto const UL_line = pt_UL[1];
              assert (is_in_window(UL_line));
              register_pixel(dx, dy, pt_UL);
            }
            // comment asserter que l'on va au moins jusqu'à sX-1???
            assert(is_in_window(line(point(in_sizeX-2,dy))) == (last == line_end - 1));
          } // has_match
        }
        else
        {
          // DEM line is not monotonic in respect of progression of UL_line
          // value => we analyse each and every point of the line -> O(n)

          // For each column
          for (unsigned dx = 0 ; dx < in_sizeX -1 ; ++dx)
          {
            auto const pt_UL = *point(dx, dy);
            auto const UL_line = pt_UL[1];

            // Protect Memory access and input access
            static_assert(std::is_same<decltype(UL_line), decltype(lo_margin)>::value, "no convertion!!!");
            if (is_in_window(UL_line)) // true 2% of the time...
            {
              register_pixel(dx, dy, pt_UL);
              has_match = true;
            }
          } // End colunms (input)
        } // else: not monotonic

        // if vector of for the current Line of DEM is not empty => store this vector into our vectors
        // of vectors
        if ( !(Polygon_SideInUp_VecLine[countDEMLine].empty()))
        {
          Polygon_SideInUp_Vec.push_back   (&Polygon_SideInUp_VecLine[countDEMLine]);
          Polygon_SideInDown_Vec.push_back (&Polygon_SideInDown_VecLine[countDEMLine]);
          Polygon_SideOutUp_Vec.push_back  (&Polygon_SideOutUp_VecLine[countDEMLine]);
          Polygon_SideOutDown_Vec.push_back(&Polygon_SideOutDown_VecLine[countDEMLine]);
        }

        ++countDEMLine;
      } // End of lines (input)

      ///////////////////////////// Scan of all selected polygons to extract wanted data //////////////
      this->PolygonsScan(ind_Line_Into_SARGeo,
          Polygon_SideInUp_Vec, Polygon_SideInDown_Vec,
          Polygon_SideOutUp_Vec, Polygon_SideOutDown_Vec,
          firstCol, outValueTab, threadId);

      // Clear Vectors
      for (unsigned int k = 0; k < nbDEMLines; k++)
      {
        Polygon_SideInUp_VecLine   [k].clear();
        Polygon_SideInDown_VecLine [k].clear();
        Polygon_SideOutUp_VecLine  [k].clear();
        Polygon_SideOutDown_VecLine[k].clear();
      }

      Polygon_SideInUp_Vec.clear();
      Polygon_SideInDown_Vec.clear();
      Polygon_SideOutUp_Vec.clear();
      Polygon_SideOutDown_Vec.clear();

    } // End Loop on Line into SAR Geometry

    // After the scan : Synthetize results
    m_FunctionOnPolygon->synthetize(outValueTab, threadId);

    // Assignate the output with the contribution of the current polygons
    for (int counter = 0 ; !OutIt.IsAtEndOfLine() ; ++OutIt, ++counter)
    {
      OutIt.Set(outValueTab[counter]);
    } // End colunms (ouput)

    progress.CompletedPixel(); // Completed...Line()
  } // End lines (Main output)
}

} /*namespace otb*/

#endif
