/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARComputeGroundAndSatPositionsOnEllipsoidImageSource_hxx
#define otbSARComputeGroundAndSatPositionsOnEllipsoidImageSource_hxx

#include "Filters/otbSARComputeGroundAndSatPositionsOnEllipsoidImageSource.h"
#include "itkImageScanlineIterator.h"

template<typename TImageOut>
otb::SARComputeGroundAndSatPositionsOnEllipsoidImageSource<TImageOut>
::SARComputeGroundAndSatPositionsOnEllipsoidImageSource(
    std::vector<Orbit> osvs,
    unsigned int       polynomial_degree /* = 8 */)
: SARComputeGroundAndSatPositionsImplHelper<TImageOut>(std::move(osvs), polynomial_degree)
{
}


template<typename TImageOut>
void
otb::SARComputeGroundAndSatPositionsOnEllipsoidImageSource<TImageOut>
::GenerateOutputInformation()
{
  Superclass :: GenerateOutputInformation();

  OutputImagePointer output = this->GetOutput();
  auto meta = GetImageMetadata(*output);
  this->WriteBandMetaData(meta, *output);
  output->SetImageMetadata(meta);
}


template<typename TImageOut>
void
otb::SARComputeGroundAndSatPositionsOnEllipsoidImageSource<TImageOut>
::ThreadedGenerateData(
    OutputImageRegionType const& outputRegionForThread,
    itk::ThreadIdType            threadId)
{
  assert(this->m_NbComponents ==
        (this->m_withXYZ    ? 3 : 0)
      + (this->m_withH      ? 1 : 0)
      + (this->m_withSatPos ? 3 : 0));

  PixelType pixelOutput;
  pixelOutput.Reserve(this->m_NbComponents);

  using OutputIterator = itk::ImageScanlineIterator< OutputImageType >;

  auto compute_pixel = [&](Point2DType demLonLatPoint, auto /*col_idx*/, OutputIterator OutIt) {
    this->ComputePixel(pixelOutput, demLonLatPoint, 0);
    OutIt.Set(pixelOutput);
  };
  this->DoGenerateTileDataLineWise(compute_pixel, outputRegionForThread, threadId);
}

#endif  // otbSARComputeGroundAndSatPositionsOnEllipsoidImageSource_hxx
