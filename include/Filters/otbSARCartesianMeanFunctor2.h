/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCartesianMeanFunctor2_h
#define otbSARCartesianMeanFunctor2_h

#include "SARCalibrationExtendedExport.h"
#include "Filters/otbSARPolygonsFunctor2.h"

#include "itkObject.h"
#include "itkObjectFactory.h"
#include <cassert>

namespace otb
{
namespace Function
{
/** \class SARCartesianMeanFunctor
 * \brief Catesian mean estimation.
 *
 * This functor is an implementation of SARPolygonsFunctor. It estimates the Cartesian coordonates mean image.
 *
 * The characteristics are the following :
 * _ Output Geometry : ML (defined by ML factors)
 * _ Seven required components : L, C, Y, Z, XCart, YCart and ZCart
 * _ Four estimated components : Mean of XCart, YCart, ZCart and an isData mask
 * _ Optionnal image : No
 *
 * \ingroup DiapOTBModule
 */
template <class TInputPixel, class TOutputPixel>
class SARCalibrationExtended_EXPORT SARCartesianMeanFunctor : public SARPolygonsFunctor<TInputPixel, TOutputPixel>
{
public:
  /** Standard class typedefs */
  using Self         = SARCartesianMeanFunctor;
  using Superclass   = SARPolygonsFunctor<TInputPixel, TOutputPixel>;
  using Pointer      = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  /** Runtime information */
  itkTypeMacro(SARCartesianMeanFunctor, itk::Object);

  /**
   * Method SetRequiredComponentsToInd (override)
   */
  void SetRequiredComponentsToInd(std::map<std::string, int> const& mapForInd) final
  {
    std::map<std::string, int>::const_iterator it = mapForInd.begin();
    while(it!=mapForInd.end())
    {
      if (it->first == "L")
      {
        m_indL = it->second;
      }
      else if (it->first == "C")
      {
        m_indC = it->second;
      }
      else if (it->first == "Y")
      {
        m_indY = it->second;
      }
      else if (it->first == "Z")
      {
        m_indZ = it->second;
      }
      else if (it->first == "XCart")
      {
        m_indXCart = it->second;
      }
      else if (it->first == "YCart")
      {
        m_indYCart = it->second;
      }
      else if (it->first == "ZCart")
      {
        m_indZCart = it->second;
      }
      ++it;
    }
  }

  /**
   * Method initialize (override)
   */
  void initalize(otb::Span<TOutputPixel> outValue, int threadId=1) final
  {
    unsigned int const nbComponentsMax = this->GetNumberOfEstimatedComponents();
    unsigned int const nbElt = outValue.size();

    m_CountPolygons[threadId].resize(nbElt);

    std::fill(std::begin(m_CountPolygons[threadId]), std::end(m_CountPolygons[threadId]), 0);

    // Reserve Components into outValue and Initalize
    for(unsigned int ind = 0; ind < nbElt; ind++)
    {
      if (outValue[ind].GetSize() != nbComponentsMax)
      {
        outValue[ind].Reserve(nbComponentsMax);
      }

      for (unsigned int iC = 0; iC < nbComponentsMax; iC++)
      {
        outValue[ind][iC] = 0;
      }
    }
  }

  /**
   * Method estimate (override)
   */
  void estimate(const int ind_LineSAR,
      TInputPixel const& CLZY_InSideUp,
      TInputPixel const& CLZY_InSideDown,
      TInputPixel const& CLZY_OutSideUp,
      TInputPixel const& CLZY_OutSideDown,
      int firstCol_into_outputRegion,
      otb::Span<TOutputPixel> outValue,
      bool isFirstMaille, double & tgElevationMaxForCurrentLine,int threadId=1) final
  {
    // ML factor in range (to get SAR geometry (= Projeted DEM Pixel/Bands Geometry))
    firstCol_into_outputRegion = m_originC + (firstCol_into_outputRegion - m_originC)*m_MLran;
    int nbCol_into_outputRegion = outValue.size() * m_MLran;

    if (static_cast<unsigned int>(nbCol_into_outputRegion+firstCol_into_outputRegion) >
        (this->GetNbColSAR() + m_originC))
    {
      nbCol_into_outputRegion = this->GetNbColSAR() - firstCol_into_outputRegion + m_originC;
    }

    assert(unsigned(threadId) < m_CountPolygons.size());
    auto & polygons_count = m_CountPolygons[threadId];

    // Elevation angle (for shadow)
    double tgElevationMax = 0.;


    // Define the coef a and b
    double a1 =  CLZY_InSideUp[m_indL] - ind_LineSAR;
    a1 /= CLZY_InSideUp[m_indL] - CLZY_InSideDown[m_indL];

    double const b1 = 1-a1;

    double const c1 = b1*CLZY_InSideUp[m_indC] + a1*CLZY_InSideDown[m_indC];

    double a2 = CLZY_OutSideUp[m_indL] - ind_LineSAR;
    a2 /= (CLZY_OutSideUp[m_indL] - CLZY_OutSideDown[m_indL]);

    double const b2 = 1-a2;

    double const c2 = b2*CLZY_OutSideUp[m_indC] + a2*CLZY_OutSideDown[m_indC];

    // Caculate zE, yE, zS and yS
    // Select input and output side for the current maille with c1 and c2 comparison
    double zE = b2*CLZY_OutSideUp[m_indZ] + a2*CLZY_OutSideDown[m_indZ];
    double yE = b2*CLZY_OutSideUp[m_indY] + a2*CLZY_OutSideDown[m_indY];

    double xCartE = b2*CLZY_OutSideUp[m_indXCart] + a2*CLZY_OutSideDown[m_indXCart];
    double yCartE = b2*CLZY_OutSideUp[m_indYCart] + a2*CLZY_OutSideDown[m_indYCart];
    double zCartE = b2*CLZY_OutSideUp[m_indZCart] + a2*CLZY_OutSideDown[m_indZCart];

    double zS = b1*CLZY_InSideUp[m_indZ] + a1*CLZY_InSideDown[m_indZ];
    double yS = b1*CLZY_InSideUp[m_indY] + a1*CLZY_InSideDown[m_indY];

    double xCartS = b1*CLZY_InSideUp[m_indXCart] + a1*CLZY_InSideDown[m_indXCart];
    double yCartS = b1*CLZY_InSideUp[m_indYCart] + a1*CLZY_InSideDown[m_indYCart];
    double zCartS = b1*CLZY_InSideUp[m_indZCart] + a1*CLZY_InSideDown[m_indZCart];

    double cE = c2;
    double cS = c1;

    if (c2 >= c1)
    {
      std::swap(zE, zS);
      std::swap(yE, yS);

      std::swap(xCartE, xCartS);
      std::swap(yCartE, yCartS);
      std::swap(zCartE, zCartS);
      std::swap(cE, cS);
    }

    double const dc = cS - cE;

    // Colunm into // TODO: the current maille
    int const col1 = int(std::min(cE, cS))+1;        // +1 ?
    int const col2 = int(std::max(cE, cS));

    // tgElevationMax per mailles (not sure)
    if (isFirstMaille)
    {
      tgElevationMax = (zE > 0.0) ?  yE / zE : sign (1e20, yE);
      if (tgElevationMax < 0.) tgElevationMax = 0.;
    }
    else
    {
      tgElevationMax = tgElevationMaxForCurrentLine;
    }

    // Loop on colunm
    for (int k = std::max(col1, firstCol_into_outputRegion),
        N = std::min(col2, nbCol_into_outputRegion+firstCol_into_outputRegion-1)
        ; k <= N; ++k)
    {
      double const a = (cS - k) / dc;
      double const b = 1.0 - a;
      double const Z = a * zE + b * zS;
      double const Y = a * yE + b * yS;
      double const XCart = a*xCartE + b*xCartS;
      double const YCart = a*yCartE + b*yCartS;
      double const ZCart = a*zCartE + b*zCartS;

      double const tgElevation = (Z > 0.0) ? Y / Z : sign (1e20, Y);

      // Check into shadows?
      bool const not_in_shadows = tgElevation > tgElevationMax;
      int const validity_flag = not_in_shadows ? 1 : 2;
      if (not_in_shadows)
        tgElevationMax = tgElevation;

      // Contribution for this maille
      int ind_for_out = k - firstCol_into_outputRegion;

      // Into ML Geo if m_MLRan > 1
      ind_for_out /= m_MLran; // Integer division

      // Accumulate X, Y and Z Cartesian
      assert(unsigned(ind_for_out) < outValue.size());
      outValue[ind_for_out][0] += XCart;
      outValue[ind_for_out][1] += YCart;
      outValue[ind_for_out][2] += ZCart;

      // Set isData at 1
      outValue[ind_for_out][3] = validity_flag;

      // Increment the counter
      assert(unsigned(ind_for_out) < polygons_count.size());
      polygons_count[ind_for_out] += 1;
    }

    // Store the tgElevationMax (for the next mailles)
    tgElevationMaxForCurrentLine = tgElevationMax;
  }

  /**
   * Synthetize to apply on `XCart,` `YCart `and `ZCart` the counter of polygons.
   */
  void synthetize(otb::Span<TOutputPixel> outValue, int threadId=1) final
  {
    assert(unsigned(threadId) < m_CountPolygons.size());
    auto const& polygons_count = m_CountPolygons[threadId];
    for(auto ind = 0ull, nbElt = outValue.size(); ind < nbElt; ind++)
    {
      assert(ind < polygons_count.size());
      if (polygons_count[ind] !=0)
      {
        outValue[ind][0] /= polygons_count[ind];
        outValue[ind][1] /= polygons_count[ind];
        outValue[ind][2] /= polygons_count[ind];
      }
    }
  }

  // default constructor
  SARCartesianMeanFunctor() = delete;

  /** Constructor */
  SARCartesianMeanFunctor(
      int nbThreads,
      unsigned int mlran = 1,
      unsigned int mlazi = 1,
      unsigned int originC = 0)
  : m_NbThreads(nbThreads)
  , m_CountPolygons(m_NbThreads) // Allocate the buffer of polygons for each thread
  , m_MLran( mlran )
  , m_MLazi( mlazi )
  , m_originC( originC )
  {
    // Global argument (from PolygonsFunctor)
    this->SetNumberOfExpectedComponents(7); // expected components in input = 7
    this->SetNumberOfEstimatedComponents(4); // estimated components in output = 4

    if (m_MLran == 1 && m_MLazi == 1)
    {
      this->SetOutputGeometry("SAR");
    }
    else
    {
      this->SetOutputGeometry("ML");
    }

    // Set the vector of Required Components
    std::vector<std::string> vecComponents{
      "C", "L", "Z", "Y", "XCart", "YCart", "ZCart"};
    this->SetRequiredComponents(move(vecComponents));

    std::vector<std::string> estimatedComponents{"XCart", "YCart", "ZCart", "isData"};
    this->SetEstimatedComponents(move(estimatedComponents));
  }

  using Superclass::GetOutputGeometry;

  /**
   * Method GetOutputGeometry (override)
   */
  void GetOutputGeometry(std::string & outputGeo, unsigned int & mlran, unsigned int & mlazi) final
  {
    if (m_MLran == 1 && m_MLazi == 1)
    {
      outputGeo = "SAR";
    }
    else
    {
      outputGeo = "ML";
    }

    mlran = m_MLran;
    mlazi = m_MLazi;
  }

  // Redefinition to use construction with NumberOfThreads and ML factor (Functor used in Multi-Threading)
  static Pointer New(int nbThreads, unsigned int mlran = 1, unsigned int mlazi = 1, unsigned int originC = 0)
  {
    Pointer smartPtr = ::itk::ObjectFactory< Self >::Create();
    if ( smartPtr == nullptr )
    {
      smartPtr = new Self(nbThreads, mlran, mlazi, originC);
    }
    smartPtr->UnRegister();
    return smartPtr;
  }

  /** Destructor */
  ~SARCartesianMeanFunctor() final = default;

protected :
  int m_NbThreads;
  std::vector<std::vector<int>> m_CountPolygons;

  // Index for each components
  int m_indL;
  int m_indC;
  int m_indZ;
  int m_indY;
  int m_indXCart;
  int m_indYCart;
  int m_indZCart;

  // ML factors
  unsigned int m_MLran;
  unsigned int m_MLazi;

  // Origin for range dimension
  unsigned int m_originC;
};

}
}

#endif
