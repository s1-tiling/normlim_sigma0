/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbBandNames_h
#define otbBandNames_h

namespace otb
{
namespace bands
{
/**\name List of bands ids.
 *
 * Used in images metadata to find in which band an information is.
 */
//@{
#if OTB_VERSION_MAJOR >= 8
#  define OTB_META_PREFIX
#else
#  define OTB_META_PREFIX "support_data."
#endif
constexpr char const* k_prefix_band    = OTB_META_PREFIX "band.";
constexpr char const* k_prefix_scandir = OTB_META_PREFIX "DirectionToScanDEM";

constexpr char const* k_id_X           = OTB_META_PREFIX "band.XCart";
constexpr char const* k_id_Y           = OTB_META_PREFIX "band.YCart";
constexpr char const* k_id_Z           = OTB_META_PREFIX "band.ZCart";
constexpr char const* k_id_H           = OTB_META_PREFIX "band.H";
constexpr char const* k_id_SatX        = OTB_META_PREFIX "band.XSatPos";
constexpr char const* k_id_SatY        = OTB_META_PREFIX "band.YSatPos";
constexpr char const* k_id_SatZ        = OTB_META_PREFIX "band.ZSatPos";

constexpr char const* k_id_nX          = OTB_META_PREFIX "band.nX";
constexpr char const* k_id_nY          = OTB_META_PREFIX "band.nY";
constexpr char const* k_id_nZ          = OTB_META_PREFIX "band.nZ";

constexpr char const* k_id_LIA         = OTB_META_PREFIX "band.LIA";
constexpr char const* k_id_abs_sin_LIA = OTB_META_PREFIX "band.abs_sin_LIA";

#undef OTB_META_PREFIX
//@}
}
} // namespace otb::bands

#endif  // otbBandNames_h
