/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This file is a fork of DiapOTB SARDEMProjection adapted to OTB 8 source code
// for the needs of S1Tiling

#ifndef otbSARDEMProjectionImageFilter2_hxx
#define otbSARDEMProjectionImageFilter2_hxx

#include "Filters/otbSARDEMProjectionImageFilter2.h"
#include "Metadata/otbMetadataHelper.h"
#include "Common/otbRepack.h"
#include "otbDEMHandler.h"

#if OTB_VERSION_MAJOR >= 8
#  include "otbGeocentricTransform.h"
#else
#  if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wunused-parameter"
#    include "ossim/base/ossimFilename.h"
#    include "ossim/base/ossimGpt.h"
#    pragma GCC diagnostic pop
#  else
#    include "ossim/base/ossimFilename.h"
#    include "ossim/base/ossimGpt.h"
#  endif
#endif

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"

#include <cassert>
#include <vector>

namespace otb
{

/**
 * Constructor with default initialization
 */
template <class TImageIn, class TImageOut>
SARDEMProjectionImageFilter2<TImageIn ,TImageOut>
::SARDEMProjectionImageFilter2(std::string const& geoid_filename)
: m_geoid_filename(geoid_filename)
{
#if OTB_VERSION_MAJOR >= 8
  auto& demHandler = DEMHandler::GetInstance();
  assert(demHandler.GetGeoidFile() == geoid_filename
      && "DEMHandler is expected to have already been initialized w/ the GEOID");
#else
  auto& demHandler = *DEMHandler::Instance();

  if (! geoid_filename.empty())
  {
    const std::string isEmg96 =  "egm96";
    std::size_t found = geoid_filename.find(isEmg96);

    if (found!=std::string::npos)
    {
      m_geoidEmg96 = std::make_unique<ossimGeoidEgm96>(ossimFilename(geoid_filename));
    }
  }
#endif
  (void) demHandler; // silence "unused value"
}

/**
 * Set Sar Image keyWordList
 */
#if OTB_VERSION_MAJOR >= 8
template<class TImageIn, class TImageOut>
void
SARDEMProjectionImageFilter2< TImageIn ,TImageOut >
::SetSARImageMetadata(ImageMetadata sarImageMetadata)
{
  m_SarImageMetadata = std::move(sarImageMetadata);
}
#else
template<class TImageIn, class TImageOut>
void
SARDEMProjectionImageFilter2< TImageIn ,TImageOut >
::SetSARImageKeyWorList(ImageKeywordlist sarImageKWL)
{
  m_SarImageKwl = std::move(sarImageKWL);
}
#endif

/**
 * Method GenerateOutputInformation()
 */
template<class TImageIn, class TImageOut>
void
SARDEMProjectionImageFilter2< TImageIn, TImageOut >
::GenerateOutputInformation()
{
  // Call superclass implementation
  Superclass :: GenerateOutputInformation();

  // Get pointers to the input and output
  ImageInConstPointer inputPtr = this->GetInput();
  ImageOutPointer     outputPtr = this->GetOutput();

  // Image metadata
  auto metadata = GetImageMetadata(*inputPtr);
  m_srcNoData   = ExtractNoDataValue(metadata);
  otbLogMacro(Info, << "NoData in input DEM image: " << m_srcNoData);

#if OTB_VERSION_MAJOR >= 8
  auto & inputKwl = metadata;
#else
  // OTB7 distinguished metadata (where band nodata will be stored) and
  // keyWordList where global metadata are stored.
  // => use only one metadata reference eventually
  auto   inputKwl = inputPtr->GetImageKeywordlist();
#endif

  BandInformation bands(metadata, BandInformation::clear);

  // Set the number of Components for the Output VectorImage
  // At Least 4 Components :
  //                _ C : Colunm of DEM into SAR Image
  //                _ L : Line of DEM into SAR Image
  //                _ Z : Coordonates Z
  //                _ Y : Coordonates Y
  //  (Z,Y)  coordinates, defined into ossimSarSensorModel (if otb version < 8) or SarSensorModel as follows:
  //  Let n = |sensorPos|,
  //  ps2 = scalar_product(sensorPos,worldPoint)
  //  d = distance(sensorPos,worldPoint)
  //
  //  Z = n - ps2/n
  //  Y = sqrt(d*d - Z*Z)
  //
  // m_withH      = true => 1 component added H : high
  // m_withXYZ    = true => 3 components added Xcart Ycart Zcart : cartesian coordinates
  // m_withSatPos = true => 3 components added : Satellite Position into cartesian

  auto add_metadata = [& inputKwl](
      std::string const& key,
      std::string const& value)
  {
#if OTB_VERSION_MAJOR >= 8
    inputKwl.Add(key, value);
#else
    inputKwl.AddKey("support_data." + key, value);
#endif
  };

  m_NbComponents = 0;
  if (m_withCRYZ)
  {
    add_metadata("band.L", "1");
    add_metadata("band.C", "0");
    add_metadata("band.Z", "2");
    add_metadata("band.Y", "3");
    m_NbComponents += 4;

    bands.add_new("C", m_dstNoData);
    bands.add_new("L", m_dstNoData);
    bands.add_new("Z", m_dstNoData);
    bands.add_new("Y", m_dstNoData);
  }

  if (m_withXYZ)
  {
    m_indXYZ = m_NbComponents;
    add_metadata("band.XCart", std::to_string(m_indXYZ)  );
    add_metadata("band.YCart", std::to_string(m_indXYZ+1));
    add_metadata("band.ZCart", std::to_string(m_indXYZ+2));
    m_NbComponents += 3;

    bands.add_new("XCart", m_dstNoData);
    bands.add_new("YCart", m_dstNoData);
    bands.add_new("ZCart", m_dstNoData);
  }
  if (m_withH)
  {
    m_indH = m_NbComponents;
    add_metadata("band.H", std::to_string(m_indH));
    m_NbComponents += 1;

    bands.add_new("Height", m_dstNoData);
  }
  if (m_withSatPos)
  {
    m_indSatPos = m_NbComponents;
    add_metadata("band.XSatPos", std::to_string(m_indSatPos)  );
    add_metadata("band.YSatPos", std::to_string(m_indSatPos+1));
    add_metadata("band.ZSatPos", std::to_string(m_indSatPos+2));
    m_NbComponents += 3;

    bands.add_new("XSatPos", m_dstNoData);
    bands.add_new("YSatPos", m_dstNoData);
    bands.add_new("ZSatPos", m_dstNoData);
  }

  // Add gain and direction into kwl
  add_metadata("DirectionToScanDEMC", std::to_string(m_directionToScanDEMC));
  add_metadata("DirectionToScanDEML", std::to_string(m_directionToScanDEML));
  add_metadata("Gain",                std::to_string(m_gain));

  otbLogMacro(Info, <<"SARDEMProjection will produce a " << m_NbComponents << " bands image");
  outputPtr->SetNumberOfComponentsPerPixel(m_NbComponents);

#if OTB_VERSION_MAJOR >= 8
  // Create and Initilaze the SarSensorModel
  m_SarSensorModel = std::make_unique<FastSarSensorModel>(m_SarImageMetadata);
#else
  // Create and Initilaze the SarSensorModelAdapter
  m_SarSensorModel = SarSensorModelAdapter::New();
  bool loadOk = m_SarSensorModel->LoadState(m_SarImageKwl);

  if(!loadOk || !m_SarSensorModel->IsValidSensorModel())
  {
    itkExceptionMacro(<<"SAR image does not contain a valid SAR sensor model.");
  }
#endif


#if OTB_VERSION_MAJOR >= 8
  // Set nodata -- not enough to be globally interpreted as "value on all bands"
  inputKwl.Add(MDNum::NoData, m_dstNoData);

  otbLogMacro(Debug, << "Output metadata " << inputKwl);
#endif

  // Compute the output image size, spacing and origin (same as the input)
  ImageOutRegionType outputLargestPossibleRegion = inputPtr->GetLargestPossibleRegion();
  ImageInSizeType const &    inputSize = outputLargestPossibleRegion.GetSize();
  ImageInPointType           origin    = inputPtr->GetOrigin();
  ImageInSpacingType const & inSP      = inputPtr->GetSpacing();

  outputLargestPossibleRegion.SetSize(static_cast<ImageOutSizeType>(inputSize));
  outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
  outputPtr->SetOrigin(static_cast<ImageOutPointType>(origin));
  outputPtr->SetSpacing(static_cast<ImageOutSpacingType>(inSP));

  bands.write_back();
  SetImageMetadata(*outputPtr, std::move(metadata));
#if OTB_VERSION_MAJOR < 8
  outputPtr->SetImageKeywordList(inputKwl);
#endif

  // Check the need for reprojecting image coordinates into WGS84
  static auto const& wgs84Srs = OGRSpatialReference::GetWGS84SRS();
  assert(wgs84Srs->GetAxisMappingStrategy() == OAMS_TRADITIONAL_GIS_ORDER);
  auto const projection = inputPtr->GetProjectionRef();

  OGRSpatialReference demSrs(projection.c_str());
  // Make sure to use the same axis order in order to not mix coordinates
  // orders with transforming
  demSrs.SetAxisMappingStrategy(wgs84Srs->GetAxisMappingStrategy());
  if (!demSrs.IsSame(wgs84Srs))
  {
    otbLogMacro(Info, << "DEM Projection is " << projection << " => need to convert to WGS84");
    auto const nbThreads = this->GetNumberOfThreads();
    m_tlsCTs.reserve(nbThreads);
    for (auto i=0u; i!=nbThreads ; ++i)
    {
#if 1
      m_tlsCTs.emplace_back(OGRCreateCoordinateTransformation(&demSrs, wgs84Srs));
#else
      // Eventually when we can have a vector<CoordinateTransformation>
      m_tlsCTs.emplace_back(&demSrs, wgs84Srs);
#endif
    }
  }
  else
  {
    otbLogMacro(Info, << "DEM Projection is " << projection << " => NO need to convert to WGS84");
  }
}

/**
 * Method GenerateInputRequestedRegion
 */
template<class TImageIn, class TImageOut>
void
SARDEMProjectionImageFilter2< TImageIn, TImageOut >
::GenerateInputRequestedRegion()
{
  ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

  ImageInRegionType inputRequestedRegion = static_cast<ImageInRegionType>(outputRequestedRegion);
  ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetInput() );

  inputPtr->SetRequestedRegion(inputRequestedRegion);
}

/**
 * Method ThreadedGenerateData
 */
template<class TImageIn, class TImageOut >
template <typename GeoidOffsetPolicy>
void
SARDEMProjectionImageFilter2< TImageIn, TImageOut >
::DoThreadedGenerateData(
    ImageOutRegionType const & outputRegionForThread,
    itk::ThreadIdType threadId,
    GeoidOffsetPolicy get_geoid_offset)
{
  // Compute corresponding input region (same as output here)
  ImageInRegionType inputRegionForThread = static_cast<ImageInRegionType>(outputRegionForThread);

  //  Define/declare an iterator that will walk the input region for this
  // thread.
  typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
  InputIterator  InIt(this->GetInput(), inputRegionForThread);

  // Define/declare an iterator that will walk the output region for this
  // thread. NO VECTOR IMAGE INTO ITERATOR TEMPLATE
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;
  OutputIterator OutIt(this->GetOutput(), outputRegionForThread);

  // Support progress methods/callbacks
  itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

  // Transform all points
  InIt.GoToBegin();
  OutIt.GoToBegin();

  assert(m_NbComponents ==
      (m_withCRYZ     ? 4 : 0)
      + (m_withXYZ    ? 3 : 0)
      + (m_withH      ? 1 : 0)
      + (m_withSatPos ? 3 : 0));

  ImageOutPixelType pixelOutput;
  pixelOutput.Reserve(m_NbComponents);

  std::size_t const line_size = inputRegionForThread.GetSize()[0];
  std::vector<double> buffer(line_size * 2);
  auto * poCT = m_tlsCTs.empty() ? nullptr : m_tlsCTs[threadId].get();
#if 0
  // We don't need it actually
  std::vector<int>    buffer_error_code;
  if (m_poCT)
  {
    buffer_error_code.resize(line_size);
  }
#endif

  // For each Line in MNT
  while ( !InIt.IsAtEnd())
  {
    InIt.GoToBeginOfLine();
    OutIt.GoToBeginOfLine();

    { // Compute lon/lat for all pixels of the current line
      std::size_t idx = 0;
      for (auto itLine=InIt; !itLine.IsAtEndOfLine(); ++itLine, ++idx)
      {
        assert(idx < line_size);
        if (itLine.Get() == m_srcNoData) continue;

        // Transform index to Lat/Lon Point
        Point2DType demLonLatPoint;
        this->GetInput()->TransformIndexToPhysicalPoint(itLine.GetIndex(), demLonLatPoint);
        buffer[idx]             = demLonLatPoint[0];
        buffer[idx + line_size] = demLonLatPoint[1];
      }
      assert(idx == line_size);

      if (poCT)
      {
#if GDAL_VERSION_NUM >= 3030000
// #   pragma message "GDAL >= 3.3 => Using TransformWithErrorCodes"
        // Directly call TransformWithErrorCodes() instead of Transform() that
        // would allocate everytime with malloc, and with a vector as well in
        // order to store results... that we'll ignore, for now...
        poCT->TransformWithErrorCodes(
            line_size,
            &buffer[0], &buffer[line_size], nullptr, nullptr,
            nullptr /* or buffer_error_code.data() */);
        // success == all_of(buffer_error_code == 0);
        // otbLogMacro(Info, << "Converting from " << demLonLatPoint << " to " << demGeoPoint);
#else
#   pragma message "GDAL < 3.3 => Using Transform"
        poCT->Transform(
            line_size,
            &buffer[0], &buffer[line_size], nullptr, nullptr,
            nullptr);
#endif
      }
    }

    // For each column in MNT
    for (std::size_t idx = 0; !InIt.IsAtEndOfLine() ; ++idx, ++InIt, ++OutIt)
    {
      // Check if NODATA
      if (InIt.Get() != m_srcNoData)
      {
        // Transform index to Lat/Lon Point
        assert(idx < line_size);
        Point2DType demLonLatPoint;
        demLonLatPoint[0] = buffer[idx];
        demLonLatPoint[1] = buffer[idx+line_size];

        // Get elevation from earth geoid
        double const h = get_geoid_offset(demLonLatPoint);

        // Correct the height with earth geoid
        Point3DType demGeoPoint = otb::repack<Point3DType>(demLonLatPoint, InIt.Get() + h);

        // Always compute the ground point is ECEF
#if OTB_VERSION_MAJOR >= 8
        Point3DType xyzCart = Projection::WorldToEcef(demGeoPoint); // Fastest version!

        auto const zero_doppler_info = m_SarSensorModel->ZeroDopplerLookup(xyzCart);
#endif

        if (m_withCRYZ)
        {
#if OTB_VERSION_MAJOR >= 8
          // Only required in 2 cases!
          double const range_time   = m_SarSensorModel->CalculateRangeTime(xyzCart, zero_doppler_info.sensorPos);
          auto const line_sample_yz = m_SarSensorModel->Doppler0ToLineSampleYZ(xyzCart, zero_doppler_info, range_time);
          //
          // Fill the four channels for output
          pixelOutput[0] = line_sample_yz.col_row[0];
          pixelOutput[1] = line_sample_yz.col_row[1];
          pixelOutput[2] = line_sample_yz.yz[1];
          pixelOutput[3] = line_sample_yz.yz[0];
#else
          Point2DType col_row(0);
          Point2DType y_z(0);
          m_SarSensorModel->WorldToLineSampleYZ(demGeoPoint, col_row, y_z);

          // Fill the four channels for output
          pixelOutput[0] = col_row[0];
          pixelOutput[1] = col_row[1];
          pixelOutput[2] = y_z[1];
          pixelOutput[3] = y_z[0];
#endif
        }

        // Add channels if required
        if (m_withXYZ)
        {
          assert(0 <= m_indXYZ);
          assert(pixelOutput.Size() >= unsigned(m_indXYZ + 3));
#if OTB_VERSION_MAJOR < 8
          Point3DType xyzCart;
          SarSensorModelAdapter::WorldToCartesian(demGeoPoint, xyzCart);
#endif
          pixelOutput[m_indXYZ+0] = xyzCart[0];
          pixelOutput[m_indXYZ+1] = xyzCart[1];
          pixelOutput[m_indXYZ+2] = xyzCart[2];
        }
        if (m_withH)
        {
          assert(0 <= unsigned(m_indH));
          assert(pixelOutput.Size() > unsigned(m_indH));
          pixelOutput[m_indH] = demGeoPoint[2];
        }
        if (m_withSatPos)
        {
          assert(0 <= m_indSatPos);
          assert(pixelOutput.Size() >= unsigned(m_indSatPos + 3));
#if OTB_VERSION_MAJOR >= 8
          pixelOutput[m_indSatPos+0] = zero_doppler_info.sensorPos[0];
          pixelOutput[m_indSatPos+1] = zero_doppler_info.sensorPos[1];
          pixelOutput[m_indSatPos+2] = zero_doppler_info.sensorPos[2];
#else
          Point3DType satPos;
          Point3DType satVel;
          m_SarSensorModel->WorldToSatPositionAndVelocity(demGeoPoint, satPos, satVel);
          pixelOutput[m_indSatPos+0] = satPos[0];
          pixelOutput[m_indSatPos+1] = satPos[1];
          pixelOutput[m_indSatPos+2] = satPos[2];
#endif
        }
      }
      else
      {
        // Fill all channels with NoData
        for (int id = 0; id < m_NbComponents; id++)
        {
          pixelOutput[id] = m_dstNoData;
        }
      }

      // Set the output (without iterator because no VectorImage into iterator template)
      OutIt.Set(pixelOutput);
      progress.CompletedPixel();
    }

    // Next Line
    InIt.NextLine();
    OutIt.NextLine();
  }
}

/**
 * Method ThreadedGenerateData
 */
template<class TImageIn, class TImageOut >
void
SARDEMProjectionImageFilter2< TImageIn, TImageOut >
::ThreadedGenerateData(
    ImageOutRegionType const & outputRegionForThread,
    itk::ThreadIdType threadId)
{

#if OTB_VERSION_MAJOR >= 8
  auto& demHandler = DEMHandler::GetInstance();
  // demHandler.ClearElevationParameters(); // NO!!!!!
  auto& demHandlerTLS = demHandler.GetHandlerForCurrentThread();
#else
  auto& demHandler = *DEMHandler::Instance();
#endif


  if (m_geoid_filename.empty())
  {
    DoThreadedGenerateData(
        outputRegionForThread, threadId,
        [](auto const&){return 0.;});
  }
  else
  {
#if OTB_VERSION_MAJOR >= 8
    // TODO: GetGeoidHeight has very bad performances (*): GDAL
    // isn't meant to be used for one coordinate at the time.
    // Instead: have a multi-pass algorithm
    // 1. get in a SINGLE call the geoid height for all points in
    //    the buffer
    // 2. Compute the rest
    //
    // (*)
    // - 96.3% of execution time spent in ThreadedGenerateData
    //   - 61.3% in GetGeoidHeight
    //     - 31.3% GDALRasterBand::RasterIO
    //       - 11.7% in mutex loc+unlock
    //     - 29.2% OCRCoordinateTransform
    //       - 20.2% in get_pid
    //
    // => Eventually call a future DEMHandler function that returns heights
    // for all points in buffer[lon|lat]
    auto get_geoid_offset
      = [&](auto demLonLatPoint)
      { return GetGeoidHeight(demHandlerTLS, demLonLatPoint); };
#else
    auto get_geoid_offset
      = [&](auto demLonLatPoint)
      {
        if (m_geoidEmg96)
        {
          ossimGpt gptPt;
          gptPt.lon =  demLonLatPoint[0];
          gptPt.lat =  demLonLatPoint[1];
          return m_geoidEmg96->offsetFromEllipsoid(gptPt);
        }
        else
        {
          return demHandler.GetHeightAboveEllipsoid(demLonLatPoint[0],demLonLatPoint[1]);
        }
      };
#endif

    DoThreadedGenerateData(outputRegionForThread, threadId, get_geoid_offset);

  }
}

} /*namespace otb*/

#endif
