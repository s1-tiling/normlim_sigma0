/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARPolygonsFunctor2_h
#define otbSARPolygonsFunctor2_h

#include "otbSpan.h"
#include "itkObject.h"

#include <map>
#include <string>
#include <cmath>
#include <algorithm>

namespace
{ // Anonymous namespace
template <typename F>
inline
F sign(F a, F b)
{
  return b >= 0
    ? std::fabs(a)
    :-std::fabs(a);
}
} // Anonymous namespace

namespace otb
{
namespace Function
{
/** \class SARPolygonsFunctor
 * \brief Base class for functor used on polygons into SARDEMPolygonsAnalysis
 *
 * This base class contains four main functions:
 * - initialize : to initialize the outValue array (one for each output line). Initialize a simple array by default.
 * - estimate : to calculate the outValue array. Has to be implemented.
 * - synthetize : to synthetize the estimation made into estimate function.
 *
 * Useful to calculate mean, for example. Empty By default.
 * - estimateOptionnalImage : to estimate the optionnal image. Empty By default.
 *
 * Several setter/getter are available to specify each functor.
 * For example each functor can choose :
 *
 * - The main output geometry (SAR or ML)
 * - The required components into projeted DEM
 * - The estimated components
 * - The optionnal image estimation
 *
 * \sa PolygonsFunctor
 *
 * \ingroup DiapOTBModule
 */
template <class TInputPixel, class TOutputPixel>
class SARPolygonsFunctor : public itk::Object
{
public:
  /** Standard class typedefs */
  using Self         = SARPolygonsFunctor;
  using Superclass   = itk::Object;
  using Pointer      = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  /** Runtime information */
  itkTypeMacro(SARPolygonsFunctor, itk::Object);

  // Initialize method
  virtual void initalize(otb::Span<TOutputPixel> outValue, int /*threadId=1*/)
  {
    // Re Initialize outValue
    std::fill(std::begin(outValue), std::end(outValue), TOutputPixel{});
  }

  // Estimate method
  virtual void estimate(const int ind_LineSAR,
      TInputPixel const& CLZY_InSideUp,
      TInputPixel const& CLZY_InSideDown,
      TInputPixel const& CLZY_OutSideUp,
      TInputPixel const& CLZY_OutSideDown,
      int firstCol_into_outputRegion,
      otb::Span<TOutputPixel> outValue,
      bool isFirstMaille, double & tgElevationMaxForCurrentLine, int threadId=1) = 0;

  // Synthetize method
  virtual void synthetize(otb::Span<TOutputPixel> , int /*threadId=1*/) = 0;

  // estimateOptionnalImage method
  virtual void estimateOptionnalImage(TOutputPixel * /*outValue*/, int /*threadId=1*/) {}

  // Getter/Setter
  itkSetMacro(NumberOfExpectedComponents, unsigned int);
  itkSetMacro(NumberOfEstimatedComponents, unsigned int);
  itkSetMacro(OutputGeometry, std::string);
  itkSetMacro(WithOptionnalOutput, bool);

  void SetEstimatedComponents(std::vector<std::string> vecEstimated)
  {
    m_EstimatedComponents = move(vecEstimated);
  }

  void SetRequiredComponents(std::vector<std::string> vecRequired)
  {
    m_RequiredComponents = move(vecRequired);
  }

  void SetSARDimensions(int nbColSAR, int nbLinesSAR)
  {
    m_NbColSAR = nbColSAR;
    m_NbLinesSAR = nbLinesSAR;
  }

  virtual void SetRequiredComponentsToInd(std::map<std::string, int> const& mapForInd)
  {
    std::map<std::string, int>::const_iterator it = mapForInd.begin();
    while(it!=mapForInd.end())
    {
      m_RequiredComponentsToInd[it->first] = it->second;
      ++it;
    }
  }

  itkGetMacro(NumberOfExpectedComponents, unsigned int);
  itkGetMacro(NumberOfEstimatedComponents, unsigned int);
  itkGetMacro(EstimatedComponents, std::vector<std::string>);
  itkGetMacro(RequiredComponents, std::vector<std::string>);
  itkGetMacro(OutputGeometry, std::string);
  itkGetMacro(NbColSAR, int);
  itkGetMacro(NbLinesSAR, int);
  itkGetMacro(WithOptionnalOutput, bool);
  virtual void GetOutputGeometry(std::string & outputGeo, unsigned int & mlran, unsigned int & mlazi)
  {
    outputGeo = m_OutputGeometry;
    mlran = 1;
    mlazi = 1;
  }
  std::map<std::string, int> const& GetRequiredComponentsToInd() const
  {
    return m_RequiredComponentsToInd;
  }

  /** Default constructor */
  SARPolygonsFunctor() = default;

  /** Destructor */
  ~SARPolygonsFunctor() override = default;

private :

  // Number of expected Components for input
  unsigned m_NumberOfExpectedComponents = 1;
  // Number of estimated Components for output
  unsigned m_NumberOfEstimatedComponents = 1;
  // Geometry of output image into PolygonsAnalysis Filter (SAR or Polygons)
  std::string m_OutputGeometry = "SAR";
  // Vector of required Components
  std::vector<std::string> m_RequiredComponents;
  // Map of required Components
  std::map<std::string, int> m_RequiredComponentsToInd;
  // Vector of estimated Components
  std::vector<std::string> m_EstimatedComponents;
  // SAR Dimensions
  int m_NbColSAR;
  int m_NbLinesSAR;
  // Boolean for optionnal output
  bool m_WithOptionnalOutput = false; // Optionnal output must be a complement of the main output (small image)
};

}
}

#endif
