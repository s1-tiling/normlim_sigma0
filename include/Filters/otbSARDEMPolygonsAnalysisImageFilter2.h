/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDEMPolygonsAnalysisImageFilter2_h
#define otbSARDEMPolygonsAnalysisImageFilter2_h

#include "SARCalibrationExtendedExport.h"
#include "otbSpan.h"
#if OTB_VERSION_MAJOR >= 8
#else
#  include "otbImageKeywordlist.h"
#endif
#include "otbGenericRSTransform.h"

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkImageRegionIterator.h"

#include <vector>
#include <type_traits>

namespace otb
{
/** \class SARDEMPolygonsAnalysisImageFilter
 * \brief Analyses the projected DEM.
 *
 * This filter :
 * _ builds polygons with projected DEM points.
 * _ select DEM polygons thanks to intersection with each SAR line.
 * _ scan all selected polygons to extract wanted information for each pixel into output geometry. The "wanted
 * information" is defined by the TFunction. This functor has to be instanciated outside this filter and is set
 * with SetFunctorPtr.
 *
 * This filter can be used to estimate the amplitude image of a SAR image. Two kinds of output geometries are
 * available : SAR geometry (same as TImageSAR) or ML geometry (thanks to ML factors). A optional image defined
 * as a vector can be estimated.
 *
 * The output geometry and the optional image are required by the functor itself. Outputs can be a simple images
 * or vector images.
 *
 * \ingroup DiapOTBModule
 */

template <typename TImageIn,  typename TImageOut, typename TImageDEM, typename TImageSAR, typename TFunction>
class SARCalibrationExtended_EXPORT SARDEMPolygonsAnalysisImageFilter
: public itk::ImageToImageFilter<TImageIn,TImageOut>
{
public:

  // Standard class typedefs
  using Self         = SARDEMPolygonsAnalysisImageFilter;
  using Superclass   = itk::ImageToImageFilter<TImageIn,TImageOut>;
  using Pointer      = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARDEMPolygonsAnalysisImageFilter,ImageToImageFilter);

  /** Typedef to image input type : otb::VectorImage */
  using ImageInType                = TImageIn;
  /** Typedef to describe the inout image pointer type. */
  using ImageInPointer             = typename ImageInType::Pointer;
  using ImageInConstPointer        = typename ImageInType::ConstPointer;
  /** Typedef to describe the inout image region type. */
  using ImageInRegionType          = typename ImageInType::RegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  using ImageInPixelType           = typename ImageInType::PixelType;
  using ImageInPointType           = typename ImageInType::PointType;
  using ImageInSubPixelType        = typename ImageInType::InternalPixelType;
  static_assert(std::is_floating_point<ImageInSubPixelType>::value, "float or double!");
  using FastPixelType              = ImageInSubPixelType const*;

  /** Typedef to describe the image index, size types and spacing for inout image. */
  using ImageInIndexType           = typename ImageInType::IndexType;
  using ImageInIndexValueType      = typename ImageInType::IndexValueType;
  using ImageInSizeType            = typename ImageInType::SizeType;
  using ImageInSizeValueType       = typename ImageInType::SizeValueType;
  using ImageInSpacingType         = typename ImageInType::SpacingType;
  using ImageInSpacingValueType    = typename ImageInType::SpacingValueType;

  /** Typedef to image output type : otb::Image or otb::VectorImage with SAR or ML geometry  */
  using ImageOutType               = TImageOut;
  /** Typedef to describe the output image pointer type. */
  using ImageOutPointer            = typename ImageOutType::Pointer;
  using ImageOutConstPointer       = typename ImageOutType::ConstPointer;
  /** Typedef to describe the output image region type. */
  using ImageOutRegionType         = typename ImageOutType::RegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  using ImageOutPixelType          = typename ImageOutType::PixelType;
  using ImageOutPointType          = typename ImageOutType::PointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  using ImageOutIndexType          = typename ImageOutType::IndexType;
  using ImageOutIndexValueType     = typename ImageOutType::IndexValueType;
  using ImageOutSizeType           = typename ImageOutType::SizeType;
  using ImageOutSizeValueType      = typename ImageOutType::SizeValueType;
  using ImageOutSpacingType        = typename ImageOutType::SpacingType;
  using ImageOutSpacingValueType   = typename ImageOutType::SpacingValueType;

  /** Typedef to optionnal image output type : otb::Image or otb::VectorImage. This output is a vector
   * with dimensions 1 x nbLineOfMainOutput */
  using ImageOptionnalType         = TImageOut;
  /** Typedef to describe the optionnal output image pointer type. */
  using ImageOptionnalPointer      = typename ImageOptionnalType::Pointer;
  using ImageOptionnalConstPointer = typename ImageOptionnalType::ConstPointer;
  using ImageOptionnalRegionType   = typename ImageOptionnalType::RegionType;
  using ImageOptionnalIndexType    = typename ImageOptionnalType::IndexType;
  using ImageOptionnalSizeType     = typename ImageOptionnalType::SizeType;


  /** Typedef to DEM image type : otb::Image with DEM geometry (only metadata)  */
  using ImageDEMType               = TImageDEM;
  using ImageDEMPointer            = typename ImageDEMType::Pointer;

  /** Typedef to SAR image type : otb::Image with SAR geometry (only metadata)  */
  using ImageSARType               = TImageSAR;
  using ImageSARPointer            = typename ImageSARType::Pointer;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Define RSTRansform For Localisation inverse (SAR to DEM)
  using RSTransformType2D = otb::GenericRSTransform<double,2,2>;

  using FunctionType      = TFunction;
  using FunctionPointer   = typename FunctionType::Pointer;

  // Typedef for iterators
  using InputIterator     = itk::ImageScanlineConstIterator< ImageInType >;
  using OutputIterator    = itk::ImageScanlineIterator< ImageOutType >;
  using OutputOptIterator = itk::ImageRegionIterator< ImageOutType >;

  // Setter
#if OTB_VERSION_MAJOR >= 8
  void SetSARImageMetadata(ImageMetadata sarImageKWL);
#else
  void SetSARImageKeyWorList(ImageKeywordlist const& sarImageKWL);
#endif
  void SetSARImagePtr(ImageSARPointer sarPtr);
  void SetDEMImagePtr(ImageDEMPointer demPtr);
  void SetDEMInformation(double gain, int DEMDirC, int DEMDirL);
  void SetFunctorPtr(FunctionPointer functor);
  // nodata can't be set: it'll be read from input image, and reused directly.
  // Getter
  itkGetConstMacro(Gain, double);

  // Initialize margin and GenericRSTransform
  void initializeMarginAndRSTransform();

protected:
  // Default Constructor
  SARDEMPolygonsAnalysisImageFilter() = default;

  // Destructor
  ~SARDEMPolygonsAnalysisImageFilter() final = default;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const final;

  /**
   * SARDEMPolygonsAnalysisImageFilter produces an image which are into SAR geometry or ML geometry.
   * The differences between output and input images are the size of images and the dimensions. The input image
   * are a otb::VectorImage and the output can be a classic image or a vector image.
   * As such, SARDEMPolygonsAnalysisImageFilter needs to provide an implementation for
   * GenerateOutputInformation() in order to inform the pipeline execution model.
   */
  void GenerateOutputInformation() final;

  /**
   * SARDEMPolygonsAnalysisImageFilter needs a input requested region that corresponds to the projection
   * into our main output requested region.
   * As such, SARQuadraticAveragingImageFilter needs to provide an implementation for
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  void GenerateInputRequestedRegion() final;

  /**
   * OutputRegionToInputRegion returns the input region.
   * This input region corresponds to the outputRegion.
   */
  ImageInRegionType OutputRegionToInputRegion(
      ImageOutRegionType const& outputRegion,
      itk::ThreadIdType         threadId) const;

  void BeforeThreadedGenerateData() final;

  /**
   * SARDEMPolygonsAnalysisImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread, itk::ThreadIdType threadId) final;

  /**
   * Select for the current SAR Line (ind_LineSAR), the polygons that intersect the current SAR line.
   * This selection is always made with the SAR Line (even in ML geometry for the main output).
   */
  bool PolygonAndSarLineIntersection(const int ind_LineSAR,
      const ImageInSubPixelType ligUL, const ImageInSubPixelType colUL,
      const ImageInSubPixelType ligUR, const ImageInSubPixelType colUR,
      const ImageInSubPixelType ligLR, const ImageInSubPixelType colLR,
      const ImageInSubPixelType ligLL, const ImageInSubPixelType colLL,
      int & i_InSideUp, int & i_InSideDown,
      int & i_OutSideUp, int & i_OutSideDown);

  /**
   * Scan all selected polygons and apply on it, the choosen functor.
   */
  void PolygonsScan(const int ind_LineSAR,
      std::vector<std::vector<FastPixelType> const*> const& CLZY_InSideUp_Vec,
      std::vector<std::vector<FastPixelType> const*> const& CLZY_InSideDown_Vec,
      std::vector<std::vector<FastPixelType> const*> const& CLZY_OutSideUp_Vec,
      std::vector<std::vector<FastPixelType> const*> const& CLZY_OutSideDown_Vec,
      int firstCol_into_outputRegion,
      otb::Span<ImageOutPixelType> outValueTab, int threadId);

private:
  SARDEMPolygonsAnalysisImageFilter(const Self&) = delete;
  Self& operator=(const Self &) = delete;

  // SAR Image (only metadata)
  ImageSARPointer m_SarImagePtr;

  // DEM Image (only metadata)
  ImageDEMPointer m_DemImagePtr;

#if OTB_VERSION_MAJOR >= 8
  // SAR Image metadata
  ImageMetadata m_SarImageKwl;
#else
  // SAR Image KeyWorldList
  ImageKeywordlist m_SarImageKwl;
#endif

  // Multiplying gain to obtain a mean radiometry at 100
  double m_Gain = 100.;

  // Directions to scan the DEM in function of SAR geometry (from near to far range)
  int m_DEMdirL;
  int m_DEMdirC;

  // Margin for polygon selection
  int m_Margin = 0;

  // Values for NoDATA
  double m_srcNoData = -32768;

  // Function to apply on DEM Polygon
  FunctionPointer m_FunctionOnPolygon;

  // Output Geometry (SAR or ML)
  std::string m_OutGeometry;
  // ML factor (for ML geometry)
  unsigned int m_MLRan = 1;
  unsigned int m_MLAzi = 1;

  // Sar dimensions
  int m_NbLinesSAR;
  int m_NbColSAR;

  // RSTransform (for inverse localisation)
  std::vector<RSTransformType2D::Pointer> m_RSTransforms;

  std::vector<bool> m_is_lineX_sorted;
  bool              m_all_lines_are_sorted = false;
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "Filters/otbSARDEMPolygonsAnalysisImageFilter2.hxx"
#endif

#endif
