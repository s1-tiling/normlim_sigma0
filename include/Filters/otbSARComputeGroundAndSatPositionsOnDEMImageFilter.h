/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARComputeGroundAndSatPositionsOnDEMImageFilter_h
#define otbSARComputeGroundAndSatPositionsOnDEMImageFilter_h

#include "SARCalibrationExtendedExport.h"
#include "Filters/otbImageSourceProjectionReferenceDecorator.h"
#include "Filters/otbSARComputeGroundAndSatPositionsImplHelper.h"
#include "Common/otbNewMacro.h"
#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
// #include "otbSARMetadata.h"
#if OTB_VERSION_MAJOR < 8
# error "OTB 7x is not supported!"
#endif

#include <vector>

namespace otb
{

/**
 * Computes ECEF ground and associated satellite positions.
 *
 * On DEM geometry, this filter computes the associated cartesian coordinates (in ECEF) of ground
 * and satellite positions.
 *
 * \tparam TImageIn   Type of output image.
 * \tparam TImageOut  Type of input image (DEM).
 *
 * \warning While `otb::VectorImage<float>` can be used as output image type, it's best avoided.
 * Prefer instead `otb::VectorImage<double>` as `floats` only have at best 7-digits of precisions,
 * and ground ECEF coordinates (in meters) have a magnitude order of 10⁷ meters.
 */
template <typename TImageIn,  typename TImageOut>
class SARCalibrationExtended_EXPORT SARComputeGroundAndSatPositionsOnDEMImageFilter
: public ImageFilterProjectionReferenceDecorator<itk::ImageToImageFilter<TImageIn,TImageOut>>
, private SARComputeGroundAndSatPositionsImplHelper<TImageOut>
{
public:

  // Standard class typedefs
  using Self         = SARComputeGroundAndSatPositionsOnDEMImageFilter;
  using Superclass   = ImageFilterProjectionReferenceDecorator<itk::ImageToImageFilter<TImageIn,TImageOut>>;
  using Pointer      = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  /** Method for creation. */
  otbNewMacro(SARComputeGroundAndSatPositionsOnDEMImageFilter);
  // Run-time type information
  itkTypeMacro(SARComputeGroundAndSatPositionsOnDEMImageFilter,ImageToImageFilter);

  /** Typedef to image type */
  using ImageInType              = TImageIn;
  /** Typedef to describe the inout image pointer type. */
  using ImageInPointer           = typename ImageInType::Pointer;
  using ImageInConstPointer      = typename ImageInType::ConstPointer;

  /** Typedef to describe the inout image region type. */
  using ImageInRegionType        = typename ImageInType::RegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  using ImageInPixelType         = typename ImageInType::PixelType;
  using ImageInPointType         = typename ImageInType::PointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  using ImageInIndexType         = typename ImageInType::IndexType;
  using ImageInIndexValueType    = typename ImageInType::IndexValueType;
  using ImageInSizeType          = typename ImageInType::SizeType;
  using ImageInSizeValueType     = typename ImageInType::SizeValueType;
  using ImageInSpacingType       = typename ImageInType::SpacingType;
  using ImageInSpacingValueType  = typename ImageInType::SpacingValueType;


  using ImageOutType             = TImageOut;
  /** Typedef to describe the output image pointer type. */
  using ImageOutPointer          = typename ImageOutType::Pointer;
  using ImageOutConstPointer     = typename ImageOutType::ConstPointer;
  /** Typedef to describe the output image region type. */
  using ImageOutRegionType       = typename ImageOutType::RegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  using ImageOutPixelType        = typename ImageOutType::PixelType;
  using ImageOutPointType        = typename ImageOutType::PointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  using ImageOutIndexType        = typename ImageOutType::IndexType;
  using ImageOutIndexValueType   = typename ImageOutType::IndexValueType;
  using ImageOutSizeType         = typename ImageOutType::SizeType;
  using ImageOutSizeValueType    = typename ImageOutType::SizeValueType;
  using ImageOutSpacingType      = typename ImageOutType::SpacingType;
  using ImageOutSpacingValueType = typename ImageOutType::SpacingValueType;

  using ImplHelperType = SARComputeGroundAndSatPositionsImplHelper<TImageOut>;
  using Point2DType  = typename ImplHelperType::Point2DType;
  using Point3DType  = typename ImplHelperType::Point3DType;
  using Vector3DType = typename ImplHelperType::Vector3DType;
  using TimeType     = typename ImplHelperType::TimeType;

  // Forward setters and getters
  using ImplHelperType::SetNoData;
  using ImplHelperType::SetwithXYZ;
  using ImplHelperType::SetwithH;
  using ImplHelperType::SetwithSatPos;
  using ImplHelperType::GetwithXYZ;
  using ImplHelperType::GetwithH;
  using ImplHelperType::GetwithSatPos;

protected:
  /// No default constructor.
  SARComputeGroundAndSatPositionsOnDEMImageFilter() = delete;

  /** Init constructor.
   * \pre Expects `geoid_filename == DEMHandler.GetGeoidFile()`
   *
   * This constructor won't update the Geoid data on `DEMHandler` singleton.
   * The singleton is expected to have been correctly configured beforehand.
   * However with older OSSIM-based versions of OTB, it will configure the
   * internal OSSIM based GEOID reader in the case of egm96 geoids.
   */
  SARComputeGroundAndSatPositionsOnDEMImageFilter(
      std::string        geoid_filename,
      std::vector<Orbit> osvs,
      unsigned int       polynomial_degree = 8
  );

  /// Destructor.
  ~SARComputeGroundAndSatPositionsOnDEMImageFilter() override = default;

  /** SARComputeGroundAndSatPositionsOnDEMImageFilter produces an image which
   * the same size its input image.
   * The different between output and input images are the dimensions. The
   * output image are a otb::VectorImage with four components.
   * As such, `SARComputeGroundAndSatPositionsOnDEMImageFilter` needs to
   * provide an implementation for `GenerateOutputInformation()` in order to
   * inform the pipeline execution model.
   */
  void GenerateOutputInformation() override;

  /** SARComputeGroundAndSatPositionsOnDEMImageFilter needs a input requested
   * region with the same size of the output requested region.
   * \sa ProcessObject::GenerateInputRequestedRegion()
   */
  void GenerateInputRequestedRegion() override;

  /** SARComputeGroundAndSatPositionsOnDEMImageFilter can be implemented as a
   * multithreaded filter.
   * Therefore, this implementation provides a `ThreadedGenerateData()`
   * routine which is called for each processing thread. The output image
   * data is allocated automatically by the superclass prior to calling
   * `ThreadedGenerateData()`.  `ThreadedGenerateData` can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa `ImageToImageFilter::ThreadedGenerateData()`,
   *     `ImageToImageFilter::GenerateData()`
   */
  void ThreadedGenerateData(
      const ImageOutRegionType& outputRegionForThread,
      itk::ThreadIdType         threadId) override;

private:
  template <typename GeoidOffsetPolicy>
  void DoThreadedGenerateData(
      const ImageOutRegionType& outputRegionForThread,
      itk::ThreadIdType         threadId,
      GeoidOffsetPolicy         get_geoid_offset);

  SARComputeGroundAndSatPositionsOnDEMImageFilter(const Self&) = delete;
  void operator=(const Self &) = delete;

  // ----[ Attributes
  std::string m_geoid_filename;
  double      m_srcNoData = -32768; //!< nodata value found in input DEM
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "Filters/otbSARComputeGroundAndSatPositionsOnDEMImageFilter.hxx"
#endif

#endif  // otbSARComputeGroundAndSatPositionsOnDEMImageFilter_h
