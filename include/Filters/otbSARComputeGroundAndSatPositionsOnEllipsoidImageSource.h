/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARComputeGroundAndSatPositionsOnEllipsoidImageSource_h
#define otbSARComputeGroundAndSatPositionsOnEllipsoidImageSource_h

#include "SARCalibrationExtendedExport.h"
#include "Filters/otbImageSourceGeometryDecorator.h"
#include "Filters/otbImageSourceProjectionReferenceDecorator.h"
#include "Filters/otbSARComputeGroundAndSatPositionsImplHelper.h"
#include "Common/otbNewMacro.h"
#include "itkImageSource.h"
#include <type_traits>

namespace otb
{

/**
 * Computes ECEF ellipsoid and associated satellite positions.
 *
 * On output geometry, this filter computes the associated cartesian coordinates (in ECEF) of
 * ellipsoid surface points and satellite positions.
 *
 * The output geometry is filled through extended `ImageSourceGeometryDecorator` interface.
 *
 * \tparam TImageIn   Type of output image.
 * \warning While `otb::VectorImage<float>` can be used as output image type, it's best avoided.
 * Prefer instead `otb::VectorImage<double>` as `floats` only have at best 7-digits of precisions,
 * and ground ECEF coordinates (in meters) have a magnitude order of 10⁷ meters.
 */
template <typename TImageOut>
class SARCalibrationExtended_EXPORT SARComputeGroundAndSatPositionsOnEllipsoidImageSource
: public ImageSourceProjectionReferenceDecorator<ImageSourceGeometryDecorator<itk::ImageSource<TImageOut>>>
, private SARComputeGroundAndSatPositionsImplHelper<TImageOut>
{
public:
  /**\name Standard class typedefs */
  //@{

  /** This _image source_ will produce XYZ ECEF coordinates, as `float`
   * precision is non sufficient, `doubles` are hard-coded in this class.
   */
  using PixelRealType = double;

  using OutputImageType               = TImageOut;
  using Self                          = SARComputeGroundAndSatPositionsOnEllipsoidImageSource;
  using Superclass                    = ImageSourceProjectionReferenceDecorator<ImageSourceGeometryDecorator<itk::ImageSource<TImageOut>>>;
  using Pointer                       = itk::SmartPointer<Self>;
  using ConstPointer                  = itk::SmartPointer<const Self>;

  // Image source typedefs
  static_assert(std::is_same<OutputImageType, typename Superclass::OutputImageType>::value, "shall match");
  using OutputImagePointer            = typename Superclass::OutputImagePointer;
  using SpacingType                   = typename Superclass::SpacingType;
  using SizeType                      = typename Superclass::SizeType;
  using PointType                     = typename Superclass::PointType;
  using IndexType                     = typename Superclass::IndexType;
  using PixelType                     = typename Superclass::PixelType;
  using OutputImageRegionType         = typename Superclass::OutputImageRegionType;

  // Implementation helper typedefs
  using ImplHelperType                = SARComputeGroundAndSatPositionsImplHelper<OutputImageType>;
  using Point2DType                   = typename ImplHelperType::Point2DType;
  using Point3DType                   = typename ImplHelperType::Point3DType;
  using Vector3DType                  = typename ImplHelperType::Vector3DType;
  using TimeType                      = typename ImplHelperType::TimeType;
  //@}

  // Forward setters and getters from implementation helper
  using ImplHelperType::SetNoData;
  using ImplHelperType::SetwithXYZ;
  using ImplHelperType::SetwithH;
  using ImplHelperType::SetwithSatPos;
  using ImplHelperType::GetwithXYZ;
  using ImplHelperType::GetwithH;
  using ImplHelperType::GetwithSatPos;

  otbNewMacro(Self);

protected:
  // void BeforeThreadedGenerateData() override;
  void ThreadedGenerateData(
      OutputImageRegionType const& outputRegionForThread,
      itk::ThreadIdType            threadId) override;

  void GenerateOutputInformation() override;

private:

  /// No default constructor.
  SARComputeGroundAndSatPositionsOnEllipsoidImageSource() = delete;
  virtual ~SARComputeGroundAndSatPositionsOnEllipsoidImageSource() = default;

  /** Init constructor.
   */
  SARComputeGroundAndSatPositionsOnEllipsoidImageSource(
      std::vector<Orbit> osvs,
      unsigned int       polynomial_degree = 8
  );

  SARComputeGroundAndSatPositionsOnEllipsoidImageSource(SARComputeGroundAndSatPositionsOnEllipsoidImageSource const&) = delete;
  SARComputeGroundAndSatPositionsOnEllipsoidImageSource& operator=(SARComputeGroundAndSatPositionsOnEllipsoidImageSource const&) = delete;
};

} // otb namespace

#ifndef OTB_MANUAL_INSTANTIATION
#include "Filters/otbSARComputeGroundAndSatPositionsOnEllipsoidImageSource.hxx"
#endif

#endif  // otbSARComputeGroundAndSatPositionsOnEllipsoidImageSource_h
