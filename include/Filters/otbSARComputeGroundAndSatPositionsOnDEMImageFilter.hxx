/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARComputeGroundAndSatPositionsOnDEMImageFilter_hxx
#define otbSARComputeGroundAndSatPositionsOnDEMImageFilter_hxx

#include "Filters/otbSARComputeGroundAndSatPositionsOnDEMImageFilter.h"
#include "Metadata/otbMetadataHelper.h"
#include "otbDEMHandler.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include <cassert>

namespace otb
{
/**
 * Constructor with default initialization
 */
template <class TImageIn, class TImageOut>
SARComputeGroundAndSatPositionsOnDEMImageFilter<TImageIn ,TImageOut>
::SARComputeGroundAndSatPositionsOnDEMImageFilter(
    std::string        geoid_filename,
    std::vector<Orbit> osvs,
    unsigned int       polynomial_degree
)
: SARComputeGroundAndSatPositionsImplHelper<TImageOut>(std::move(osvs), polynomial_degree)
, m_geoid_filename(std::move(geoid_filename))
{
  auto& demHandler = DEMHandler::GetInstance();
  assert(demHandler.GetGeoidFile() == geoid_filename
      && "DEMHandler is expected to have already been initialized w/ the GEOID");
  (void) demHandler; // silence "unused value"
}

/**
 * Method GenerateOutputInformation()
 */
template<class TImageIn, class TImageOut>
void
SARComputeGroundAndSatPositionsOnDEMImageFilter< TImageIn, TImageOut >
::GenerateOutputInformation()
{
  // Call superclass implementation
  Superclass :: GenerateOutputInformation();

  // Get pointers to the input and output
  ImageInConstPointer inputPtr = this->GetInput();
  ImageOutPointer     outputPtr = this->GetOutput();

  // Image metadata
  auto metadata = inputPtr->GetImageMetadata();
  m_srcNoData   = ExtractNoDataValue(metadata);
  otbLogMacro(Info, << "NoData in input DEM image: " << m_srcNoData);

  this->WriteBandMetaData(metadata, *outputPtr);

  // Compute the output image size, spacing and origin (same as the input)
  ImageOutRegionType outputLargestPossibleRegion = inputPtr->GetLargestPossibleRegion();
  ImageInSizeType const &    inputSize = outputLargestPossibleRegion.GetSize();
  ImageInPointType           origin    = inputPtr->GetOrigin();
  ImageInSpacingType const & inSP      = inputPtr->GetSpacing();

  outputLargestPossibleRegion.SetSize(static_cast<ImageOutSizeType>(inputSize));
  outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
  outputPtr->SetOrigin(static_cast<ImageOutPointType>(origin));
  outputPtr->SetSpacing(static_cast<ImageOutSpacingType>(inSP));

  // Set Image KeyWordList
  outputPtr->SetImageMetadata(metadata);
}

/**
 * Method GenerateInputRequestedRegion
 */
template<class TImageIn, class TImageOut>
void
SARComputeGroundAndSatPositionsOnDEMImageFilter< TImageIn, TImageOut >
::GenerateInputRequestedRegion()
{
  ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

  ImageInRegionType inputRequestedRegion = static_cast<ImageInRegionType>(outputRequestedRegion);
  ImageInPointer  inputPtr = const_cast< ImageInType * >( this->GetInput() );

  inputPtr->SetRequestedRegion(inputRequestedRegion);
}

/**
 * Method ThreadedGenerateData
 */
template<class TImageIn, class TImageOut >
template <typename GeoidOffsetPolicy>
void
SARComputeGroundAndSatPositionsOnDEMImageFilter< TImageIn, TImageOut >
::DoThreadedGenerateData(
    ImageOutRegionType const & outputRegionForThread,
    itk::ThreadIdType threadId,
    GeoidOffsetPolicy get_geoid_offset)
{
  assert(this->m_NbComponents ==
        (this->m_withXYZ    ? 3 : 0)
      + (this->m_withH      ? 1 : 0)
      + (this->m_withSatPos ? 3 : 0));

  ImageOutPixelType pixelOutput;
  pixelOutput.Reserve(this->m_NbComponents);
  ImageOutPixelType no_data;
  no_data.Reserve(this->m_NbComponents);
  for (int id = 0; id < this->m_NbComponents; id++)
  {
    no_data[id] = this->m_dstNoData;
  }

  // Compute corresponding input region (same as output here)
  ImageInRegionType inputRegionForThread = static_cast<ImageInRegionType>(outputRegionForThread);

  //  Define/declare an iterator that will walk the input region for this
  // thread.
  using InputIterator  = itk::ImageScanlineConstIterator< ImageInType >;
  InputIterator  InIt(this->GetInput(), inputRegionForThread);

  // Define/declare an iterator that will walk the output region for this
  // thread. NO VECTOR IMAGE INTO ITERATOR TEMPLATE
  using OutputIterator = itk::ImageScanlineIterator< ImageOutType >;

  auto compute_pixel = [&, srcnodata=m_srcNoData](
      Point2DType lonlat, std::ptrdiff_t /*idx*/, OutputIterator & OutIt, InputIterator const& InIt)
  {
    auto const dem_hgt = InIt.Get();
    if (dem_hgt != srcnodata)
      this->ComputePixel(pixelOutput, lonlat, dem_hgt + get_geoid_offset(lonlat));
    else
      pixelOutput = no_data;
    
    OutIt.Set(pixelOutput);
  };

  this->DoGenerateTileDataLineWise(compute_pixel, outputRegionForThread, threadId, InIt);
}

/**
 * Method ThreadedGenerateData
 */
template<class TImageIn, class TImageOut >
void
SARComputeGroundAndSatPositionsOnDEMImageFilter< TImageIn, TImageOut >
::ThreadedGenerateData(
    ImageOutRegionType const & outputRegionForThread,
    itk::ThreadIdType threadId)
{
  auto& demHandler = DEMHandler::GetInstance();
  // demHandler.ClearElevationParameters(); // NO!!!!!
  auto& demHandlerTLS = demHandler.GetHandlerForCurrentThread();

  if (m_geoid_filename.empty())
  {
    DoThreadedGenerateData(
        outputRegionForThread, threadId,
        [](auto const&){return 0.;});
  }
  else
  {
    // TODO: GetGeoidHeight has very bad performances (*): GDAL
    // isn't meant to be used for one coordinate at the time.
    // Instead: have a multi-pass algorithm
    // 1. get in a SINGLE call the geoid height for all points in
    //    the buffer
    // 2. Compute the rest
    //
    // (*)
    // - 96.3% of execution time spent in ThreadedGenerateData
    //   - 61.3% in GetGeoidHeight
    //     - 31.3% GDALRasterBand::RasterIO
    //       - 11.7% in mutex loc+unlock
    //     - 29.2% OCRCoordinateTransform
    //       - 20.2% in get_pid
    //
    // => Eventually call a future DEMHandler function that returns heights
    // for all points in buffer[lon|lat]
    auto get_geoid_offset
      = [&](auto demLonLatPoint)
      { return GetGeoidHeight(demHandlerTLS, demLonLatPoint); };

    DoThreadedGenerateData(outputRegionForThread, threadId, get_geoid_offset);
  }
}

} /*namespace otb*/

#endif
