/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARComputeGroundAndSatPositionsImplHelper_h
#define otbSARComputeGroundAndSatPositionsImplHelper_h

#include "SARCalibrationExtendedExport.h"
#include "SAR/otbLagrangianInterpolator.h"
#include "Common/otbMemberVariable.h"
#include "otbCoordinateTransformation.h"

#if OTB_VERSION_MAJOR < 8
#  error "OTB 7x is not supported!"
#else
#  include "otbImageMetadata.h"
#endif
#include "otbSARMetadata.h"

#include "itkPoint.h"
#include <vector>

namespace otb
{

/**
 * Helper class for implementing filters and images sources that produce ECEF cartesian positions
 * for ground points and the associated satellite position.
 * \tparam TImageOut  Output image type
 */
template <typename TImageOut>
class SARCalibrationExtended_EXPORT SARComputeGroundAndSatPositionsImplHelper
{
public:
  /**\name Typedefs */
  //@{
  using ImageOutType      = TImageOut;
  using ImageOutPixelType = typename ImageOutType::PixelType;

  // Define Point2DType and Point3DType
  using Point2DType  = itk::Point<double, 2>;
  using Point3DType  = itk::Point<double, 3>;
  using Vector3DType = itk::Point<double, 3>;
  using TimeType     = MetaData::TimePoint;
  //@}

protected:
  /**\name Creation/Destruction */
  //@{
  /// No default constructor.
  SARComputeGroundAndSatPositionsImplHelper() = delete;

  /// Init constructor.
  SARComputeGroundAndSatPositionsImplHelper(
      std::vector<Orbit> osvs,
      unsigned int       polynomial_degree = 8
  );

  /** Destructor.
   * It's protected and non virtual by design.
   */
  ~SARComputeGroundAndSatPositionsImplHelper() = default;
  //@}

  /**\name Internal services for child classes only. */
  //@{
  /**
   * Complete image metadata with band information.
   * Write band names and associated nodata values.
   * \param[in,out] metadata  metadata dictionary to update
   * \param[in,out] image     related output image
   */
  void WriteBandMetaData(
      ImageMetadata & metadata,
      ImageOutType  & image);

  /**
   * Fills pixel channels with realted value computed.
   * \param[in,out] pixelOutput  multi-channels pixel to update
   * \param[in] lonlat           lon/lat coordinates of the pixel
   * \param[in] elevation        height associated to the lon/lat coordinates
   *
   * Depending on:
   * - withXYZ: XCart, YCart and ZCart ECEF coordinates associated to {lon, lat, height} are written
   * in the first channels
   * - withH: height is written in the following channel
   * - withSatPos: associated satellite position ECEF coordinates are written in the last channels
   */
  void ComputePixel(
      ImageOutPixelType & pixelOutput,
      Point2DType         lonlat,
      double              elevation
  ) const;
  //@}

public:

  /**\name Public services for clients of child classes. */
  //@{
  // ----[ Setters
  /**
   * Set desired no-data value for output images.
   */
  void SetNoData(double nodata) noexcept
  {
    m_dstNoData = nodata;
  }
  otbSetMacro(withXYZ);
  otbSetMacro(withH);
  otbSetMacro(withSatPos);
  //@}

private:
  SARComputeGroundAndSatPositionsImplHelper(SARComputeGroundAndSatPositionsImplHelper const&) = delete;
  SARComputeGroundAndSatPositionsImplHelper& operator=(SARComputeGroundAndSatPositionsImplHelper const&) = delete;

  /**\name Private Internal services */
  //@{
  // ----[ Functions stolen from FastSarSensorModel
  struct ZeroDopplerInfo
  {
    TimeType     azimuthTime;
    Point3DType  sensorPos;
    Vector3DType sensorVel;
  };
  using OrbitIterator = std::vector<Orbit>::const_iterator;

  std::tuple<TimeType, OrbitIterator, OrbitIterator>
  ZeroDopplerTimeLookupInternal(Point3DType const& inEcefPoint) const;

  ZeroDopplerInfo ZeroDopplerLookup(Point3DType const& inEcefPoint) const;

  std::pair<Point3DType, Vector3DType>
  interpolateSensorPosVel(TimeType azimuthTime, OrbitIterator itRecord1) const;
  //@}

protected:

  // ----[ Attributes
  std::vector<Orbit>       m_orbit;
  LagrangianInterpolator   m_orbitInterpolator;

  unsigned int             m_polynomial_degree = 8;

  // Value for NoData into DEM
  double m_dstNoData = -32768; //!< nodata value for image produced

  // Flags that tell which components shall be produced
  bool m_withXYZ    = false; //!< Produce cartesian coordinates?
  bool m_withH      = false; //!< Produce height?
  bool m_withSatPos = false; //!< Produce topographic phase?

  int m_NbComponents = 0;
  int m_indXYZ       = -1; //!< index into output to set the XYZ components
  int m_indH         = -1; //!< index into output to set the H component
  int m_indSatPos    = -1; //!< index into output to set the SatPos components

public:

  // ----[ Getters
  otbGetMacro(withXYZ);
  otbGetMacro(withH);
  otbGetMacro(withSatPos);
};

} // otb namespace

#ifndef OTB_MANUAL_INSTANTIATION
#include "Filters/otbSARComputeGroundAndSatPositionsImplHelper.hxx"
#endif

#endif  // otbSARComputeGroundAndSatPositionsImplHelper_h
