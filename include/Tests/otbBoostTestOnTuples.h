/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @file include/Tests/otbBoostTestOnTuples.h
 * This file provides definitions that permit to use Boost.Test `per_element` decorator on tuples.
 *
 * The standard `per_element` decorator from boost only works on arrays and strings.
 *
 * This file extends is to `tuple`. This will permit to write things like:
 * ```c++
 * BOOST_AUTO_TEST_CASE(ComputeNormal_from_real_file, *boost::unit_test::tolerance(0.00001)) {
 *    std::tuple tref{-0.60000002384185791015625, 0.800000011920928955078125,
 *                    -0.000111111119622364640235900878906};
 *    std::tuple ttst{+0.60000002384185791015625, 0.800000011920928955078125,
 *                    +0.000111111119622364640235900878906};
 *
 *    BOOST_TEST(tref == ttst, boost::test_tools::per_element());
 * }
 * ```
 *
 * \warning This file needs to be included **before** `<boost/test/included/unit_test.hpp>`
 */
#ifndef otbBoostTestOnTuples_h
#define otbBoostTestOnTuples_h

#include "Common/otbMetaProgrammingLibrary.h"

#include <boost/core/enable_if.hpp>
#include <boost/test/tools/assertion.hpp>
#include <boost/test/tools/assertion_result.hpp>
#include <boost/test/tools/detail/print_helper.hpp>
#include <tuple>

namespace boost { namespace test_tools { namespace assertion {
namespace op {

template <typename OP, typename Lhs, typename Rhs, std::size_t i, std::size_t size>
struct tuple_compare_
{
    static_assert(otb::is_tuple_v<Lhs>, "Expecting a tuple");
    static_assert(otb::is_tuple_v<Rhs>, "Expecting a tuple");

    static void eval(Lhs const& lhs, Rhs const& rhs, assertion_result & ar)
    {
        auto const& left  = std::get<i>(lhs);
        auto const& right = std::get<i>(rhs);
        assertion_result element_ar = OP::eval(left, right);
        if (!element_ar) {
            ar = false;
            ar.message() << "\n  - mismatch at position <" << i << ">: ["
                << boost::test_tools::tt_detail::print_helper(left)
                << OP::forward()
                << boost::test_tools::tt_detail::print_helper(right)
                << "] is false";
            if(!element_ar.has_empty_message())
                ar.message() << ": " << element_ar.message();
        }

        tuple_compare_<OP, Lhs, Rhs, i + 1, size>::eval(lhs, rhs, ar);
    }
};

template <typename OP, typename Lhs, typename Rhs, std::size_t size>
struct tuple_compare_<OP, Lhs, Rhs, size, size>
{
    static void eval(Lhs const& , Rhs const&, assertion_result &) {}
};


/**
 * Overload of element_compare for tuple.
 * \warning this needs to be define before `boost::test_tools::assertion::op::compare_collections`
 * which uses it in `<boost/test/tools/collection_comparison_op.hpp>`
 *
 * \tparam OP  Operation type: `boost::type<EQ<void,void>>` /`LT`/`LE`/`GT`/`GE`/`NE`
 * \tparam Lhs  Left hand side type, only tuples will be matched
 * \tparam Rhs  Right hand side type, only tuples will be matched
 * \param[in] lhs  first tuple to compare
 * \param[in] rhs  second tuple to compare
 *
 * \return a boost `assertion_result` object filled with information in case of failure
 */
template <typename OP, typename Lhs, typename Rhs>
inline
typename boost::enable_if_c<
    otb::is_tuple_v<Lhs> && otb::is_tuple_v<Rhs>,
    assertion_result>::type
element_compare( Lhs const& lhs, Rhs const& rhs)
{
    static_assert(std::tuple_size<Lhs>::value == std::tuple_size<Rhs>::value, "Tuples sizes mismatch");
    static_assert(otb::is_tuple_v<Lhs>, "Expecting a tuple");
    static_assert(otb::is_tuple_v<Rhs>, "Expecting a tuple");
    assertion_result ar( true );
    tuple_compare_<OP, Lhs, Rhs, 0, std::tuple_size<Lhs>::value>::eval(lhs, rhs, ar);
    return ar;
}

}}}}

// now we need to include the rest of boost header
#include <boost/test/included/unit_test.hpp>

namespace boost { namespace test_tools { namespace assertion {
namespace op {

#define DEFINE_TUPLE_COMPARISON( oper, name, rev, name_inverse )    \
template <>                                                         \
struct name<void, void, void>                                       \
{                                                                   \
    template <typename Lhs, typename Rhs>                           \
    static auto eval(Lhs const& lhs, Rhs const& rhs) {              \
        return name<Lhs, Rhs>::eval(lhs, rhs);                      \
    }                                                               \
    template<typename PrevExprType, typename Rhs>                   \
    static void                                                     \
    report( std::ostream&       ostr,                               \
            PrevExprType const& lhs,                                \
            Rhs const&          rhs)                                \
    {                                                               \
        lhs.report( ostr );                                         \
        ostr << revert()                                            \
             << tt_detail::print_helper( rhs );                     \
    }                                                               \
                                                                    \
    static char const* forward()                                    \
    { return " " #oper " "; }                                       \
    static char const* revert()                                     \
    { return " " #rev " "; }                                        \
};                                                                  \
template<typename Lhs,typename Rhs>                                 \
struct name<Lhs,Rhs,typename boost::enable_if_c<                    \
    otb::is_tuple_v<Lhs>  && otb::is_tuple_v<Rhs>                   \
    >::type> {                                                      \
public:                                                             \
    using result_type           = assertion_result;                 \
    using inverse               = name_inverse<Lhs, Rhs>;           \
                                                                    \
    using OP                    = name<Lhs, Rhs>;                   \
                                                                    \
    using is_specialized = typename                                 \
        mpl::if_c<                                                  \
          mpl::or_<                                                 \
              typename is_c_array<Lhs>::type,                       \
              typename is_c_array<Rhs>::type                        \
          >::value,                                                 \
          mpl::true_,                                               \
          typename                                                  \
              mpl::if_c<is_same<typename decay<Lhs>::type,          \
                                typename decay<Rhs>::type>::value,  \
                        typename cctraits<OP>::is_specialized,      \
                        mpl::false_>::type                          \
          >::type;                                                  \
                                                                    \
    using elem_op = name<void, void>;                               \
                                                                    \
    static assertion_result                                         \
    eval( Lhs const& lhs, Rhs const& rhs)                           \
    {                                                               \
        return assertion::op::compare_collections( lhs, rhs,        \
            (boost::type<elem_op>*)0,                               \
            is_specialized() );                                     \
    }                                                               \
                                                                    \
    template<typename PrevExprType>                                 \
    static void                                                     \
    report( std::ostream&,                                          \
            PrevExprType const&,                                    \
            Rhs const& ) {}                                         \
                                                                    \
    static char const* forward()                                    \
    { return " " #oper " "; }                                       \
    static char const* revert()                                     \
    { return " " #rev " "; }                                        \
                                                                    \
};                                                                  \
/**/

// Register EQ<>, GT<>...
// - for tuples: which is what we need at first
// - and for void: which permits to forward the test to exact element type that we cannot
//   beforehand.
#if BOOST_VERSION >= 107300
// The macro is expected to have 4 parameters starting with boost 1.73
BOOST_TEST_FOR_EACH_COMP_OP( DEFINE_TUPLE_COMPARISON )
#else
// The macro was expected to have 3 parameters until boost 1.72
#  define OLD_DEFINE_TUPLE_COMPARISON(oper, name, rev) \
  DEFINE_TUPLE_COMPARISON(oper, name, rev, otb::void_t)
BOOST_TEST_FOR_EACH_COMP_OP( OLD_DEFINE_TUPLE_COMPARISON )
#  undef OLD_DEFINE_TUPLE_COMPARISON
#endif
#undef DEFINE_TUPLE_COMPARISON


} // op namespace
} }
} // boost::test_tools::assertion namespaces


#endif  // otbBoostTestOnTuples_h
