/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Filters/otbXYZToImageGenerator.h"
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
// Elevation handler
#include "otbWrapperElevationParametersHandler.h"
#include "otbDEMHandler.h"

namespace otb
{

namespace Wrapper
{

/**
 * This application takes cares of projecting a DEM unto the geometry of a
 * reference image.
 *
 * \author Luc Hermitte
 * \copyright CS Group
 * \ingroup App???
 * \ingroup SARCalibrationExtended
 */
class DEMProjection : public Application
{
public:
  /** Standard class typedefs. */
  using Self                    = DEMProjection;
  using OutputPixelType         = double;
  using OutputImageType         = otb::VectorImage<OutputPixelType>;
  // using InputImageType          = otb::Image<OutputPixelType, 2>;
  using XYZToImageGeneratorType = otb::XYZToImageGenerator<OutputImageType>;
  // using WriterType       = otb::ImageFileWriter<InputImageType>;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(DEMProjection, unused);

private:
  void DoInit() override
  {
    SetName("DEMProjection");
    SetDescription("This application projects a DEM unto the geometry of a reference image");

    // Documentation
    SetDocLongDescription("This application projects a DEM unto the geometry of a reference image" );


    SetDocLimitations("None");
    SetDocAuthors("Luc Hermitte (CS Group)");

    AddDocTag(Tags::Calibration);

    AddParameter(ParameterType_InputImage, "ref",  "Filename of the reference image");
    SetParameterDescription("ref", "Filename of the reference image");
    AddParameter(ParameterType_OutputImage, "out", "Output DEM+normal filename");
    SetParameterDescription("out", "Output DEM+normal filename");

    // Elevation
    ElevationParametersHandler::AddElevationParameters(this, "elev");
    // AddParameter(ParameterType_InputImage, "geoid",  "Geoid File  (optional, off by default)");

    // AddParameter(ParameterType_OutputImage, "out",  "Image of normal vectors");
    // SetParameterDescription("out",  "Image of normal vectors");

#if 0
    AddParameter(ParameterType_Float, "nodata", "NoData value");
    SetParameterDescription("nodata", "DEM empty cells are filled with this value (optional -32768 by default)");
    SetDefaultParameterFloat("nodata", -32768);
    MandatoryOff("nodata");
#endif

    AddRAMParameter();

    SetDocExampleParameterValue("out", "DEM+normal.tif");
    SetDocExampleParameterValue("ref", "S1Image.tif");
    SetOfficialDocLink();
  }

  void DoUpdateParameters() override
  {
    // Clear and reset the DEM Handler
#if OTB_VERSION_MAJOR >= 8
    // TODO: private function... can't be cleared
    otb::DEMHandler::GetInstance().ClearElevationParameters();
    // otb::DEMHandler::GetInstance().ClearDEMs();
    // otb::DEMHandler::GetInstance();
#else
    otb::DEMHandler::Instance()->ClearDEMs();
#endif
    otb::Wrapper::ElevationParametersHandler::SetupDEMHandlerFromElevationParameters(this, "elev");
  }

  void DoExecute() override
  {
    auto* inputImage = GetParameterImage("ref");
    auto generator = XYZToImageGeneratorType::New();

// #if OTB_VERSION_MAJOR >= 8
// #else
    generator->SetOutputParametersFromImage(inputImage);
// #endif

    SetParameterOutputImage("out", generator->GetOutput());
    // SetParameterOutputImagePixelType("dem",ImagePixelType_float);

    // Call register pipeline to allow streaming and garbage collection
    RegisterPipeline();
  }
};

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::DEMProjection)
