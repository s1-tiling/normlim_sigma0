/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Filters/otbSARComputeIncidenceAngle.h"
#include "Common/otbAlgoAny.h"
#include "Applications/otbApplicationWithNoData.h"
#include "otbWrapperApplicationFactory.h"

// #define DOUBLES_EVERYWHERE

namespace
{ // Anonymous namespace
const std::string k_in_xyz     = "in.xyz";
const std::string k_in_normals = "in.normals";
const std::string k_out_deg    = "out.deg";
const std::string k_out_sin    = "out.sin";
const std::string k_out_cos    = "out.cos";
const std::string k_out_tan    = "out.tan";
} // Anonymous namespace

namespace otb
{

namespace Wrapper
{

/**
 * This application computes Local Incidence Angle from sensor to ground surface
 * normal.
 *
 * \author Luc Hermitte
 * \copyright CS Group
 * \ingroup App???
 * \ingroup SARCalibrationExtended
 */
class SARComputeIncidenceAngle : public ApplicationWithNoData<double>
{
public:
  /** Standard class typedefs. */
  using Self                    = SARComputeIncidenceAngle;
  using RealInputPixelType      = double; // Need precision on XYZ
  using RealOutputPixelType     = float;  // Float is enough for IA products
  using InputImageType          = otb::VectorImage<RealInputPixelType, 2>;
  using OutputImageType         = otb::Image<RealOutputPixelType, 2>;
  // Computing degrees as float enable for more precision that can be casted to int when serialiing
  // the output image.
  using DegOutputImageType      = otb::Image<RealOutputPixelType, 2>;
  // using DegOutputImageType      = otb::Image<std::int16_t, 2>;
  using FilterType              = otb::SARComputeIncidenceAngle<InputImageType, OutputImageType, DegOutputImageType>;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(SARComputeIncidenceAngle, unused);

private:
  void DoInit() override
  {
    SetName("SARComputeIncidenceAngle");
    SetDescription(
        "This application computes Local or Ellipsoid Incidence Angle from sensor to ground/ellipsoid surface normal."
        "\n"
        "\nActually this application doesn't expect the 4 -out.deg, -out.sin... parameters, but just a single one is enough."
        "\n"
        );

    // Documentation
    SetDocLongDescription(
        "This application computes Local or Ellipsoid Incidence Angle from sensor to ground/ellipsoid surface normal."
        "\n"
        "\nActually this application doesn't expect the 4 -out.deg, -out.sin... parameters, but just a single one is enough."
    );


    SetDocLimitations("None");
    SetDocAuthors("Luc Hermitte (CS Group)");

    AddDocTag(Tags::Calibration);

    AddParameter(ParameterType_Group, "in", "inputs group");
    AddParameter(ParameterType_Group, "out", "outputs group");

    AddParameter(ParameterType_InputImage, k_in_xyz,  "Image of cartesian XYZ coordinates, in SAR geometry");
    SetParameterDescription(k_in_xyz, "Image of cartesian XYZ coordinates, in SAR geometry. Sensor trajectory will be extracted from the metadata of this image.");
    AddParameter(ParameterType_InputImage, k_in_normals,  "Image of ground surface normals, in SAR geometry");
    SetParameterDescription(k_in_normals, "Image of ground surface normals, in the same SAR geometry of the 'xyz' image");

    AddParameter(ParameterType_OutputImage, k_out_deg, "Optional output image with abs(IA) in degree * 100");
    SetParameterDescription(k_out_deg, "Optional output image with abs(IA) in degree * 100");
    // SetParameterOutputImagePixelType(k_out_deg, ImagePixelType_int16);
#if defined(DOUBLES_EVERYWHERE)
    SetDefaultOutputPixelType(k_out_deg, ImagePixelType_double);
#endif
    MandatoryOn(k_out_deg);

    AddParameter(ParameterType_OutputImage, k_out_sin, "Output image with abs(sin IA)");
    AddParameter(ParameterType_OutputImage, k_out_cos, "Output image with abs(cos IA)");
    AddParameter(ParameterType_OutputImage, k_out_tan, "Output image with abs(tan IA)");
    MandatoryOn(k_out_sin);
    MandatoryOn(k_out_cos);
    MandatoryOn(k_out_tan);
#if defined(DOUBLES_EVERYWHERE)
    SetDefaultOutputPixelType(k_out_sin, ImagePixelType_double);
#endif
    SetParameterDescription(k_out_sin, "Output image with abs(sin IA)");

    DoInit_NoData();
    // AddParameter(ParameterType_Float, "nodata", "NoData value");
    // SetParameterDescription("nodata", "DEM empty cells are filled with this value (optional -32768 by default)");
    // SetDefaultParameterFloat("nodata", -32768);
    // MandatoryOff("nodata");

    AddRAMParameter();

    SetDocExampleParameterValue(k_in_xyz, "S1-XYZ.tiff");
    SetDocExampleParameterValue(k_in_normals, "S1-normals.tiff");
    SetDocExampleParameterValue(k_out_deg, "S1-LIA.tiff");
    SetDocExampleParameterValue(k_out_sin, "S1-LIASIN.tiff");
    SetOfficialDocLink();

    SetMultiWriting(true); // Two images will be produced
  }

  void DoUpdateParameters() override
  {
    // Here we work around OTB issue 2444:
    // https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb/-/issues/2444
    // -> All -out parameters are flagged as mandatory, but as soon as one is
    // detected, we remove the mandatory flag from every -out parameters.
    // This way if none is specified, the first one will be reported as missing
    // even if actually, we just expected any one of the 4.

    auto is_output_requested = [&](std::string const& key) {
      return IsParameterEnabled(key) && HasValue(key);
    };
    bool const produce_deg_map = is_output_requested(k_out_deg);
    bool const produce_sin_map = is_output_requested(k_out_sin);
    bool const produce_cos_map = is_output_requested(k_out_cos);
    bool const produce_tan_map = is_output_requested(k_out_tan);
    if (otb::any_of({produce_deg_map, produce_sin_map, produce_cos_map, produce_tan_map}))
    {
      MandatoryOff(k_out_sin);
      MandatoryOff(k_out_cos);
      MandatoryOff(k_out_tan);
      MandatoryOff(k_out_deg);
    }
  }

  void DoExecute() override
  {
    auto is_output_requested = [&](std::string const& key) {
      return IsParameterEnabled(key) && HasValue(key);
    };

    auto* inputXYZ     = GetParameterImage<InputImageType>(k_in_xyz);
    auto* inputNormals = GetParameterImage<InputImageType>(k_in_normals);

    bool const produce_deg_map = is_output_requested(k_out_deg);
    bool const produce_sin_map = is_output_requested(k_out_sin);
    bool const produce_cos_map = is_output_requested(k_out_cos);
    bool const produce_tan_map = is_output_requested(k_out_tan);

    auto filter = FilterType::New(
        produce_deg_map, produce_sin_map, produce_cos_map, produce_tan_map);
    filter->SetXYZInputImage(inputXYZ);
    filter->SetNormalInputImage(inputNormals);
    // filter->ShallComputeIASign(true);

    auto const ia_nodata = GetParameterNodata();
    filter->SetNoData(ia_nodata);

    if (produce_sin_map)
    {
      SetParameterOutputImage(k_out_sin, filter->GetSinOutputImage());
    }
    if (produce_cos_map)
    {
      SetParameterOutputImage(k_out_cos, filter->GetCosOutputImage());
    }
    if (produce_tan_map)
    {
      SetParameterOutputImage(k_out_tan, filter->GetTanOutputImage());
    }
    if (produce_deg_map)
    {
      SetParameterOutputImage(k_out_deg, filter->GetDegOutputImage());
    }

    // Call register pipeline to allow streaming and garbage collection
    RegisterPipeline();
  }
};

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::SARComputeIncidenceAngle)
