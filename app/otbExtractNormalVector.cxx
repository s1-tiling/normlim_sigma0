/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Metadata/otbCompleteBandMetadataFilter.h"
#include "Filters/otbNormalCompute.h"
#include "Filters/otbBandIds.h"
#include "Common/otbRepack.h"
#include "Common/otbAlgoErase.h"

#include "Applications/otbApplicationWithNoData.h"
#include "otbWrapperApplicationFactory.h"
#include "otbFunctorImageFilter.h"
#include "Common/otbStringHelpers.h"

#include "itkFixedArray.h"
#include <array>
#include <cmath>

namespace // anonymous
{

using FloatType = double;


/** Type traits to factorize all type definitions. */
template <typename InputImageType>
struct input_types
{
  using InputPixelType           = typename InputImageType::PixelType;
  using RealType                 = typename itk::NumericTraits<InputPixelType>::ValueType;
  using InputV3                  = vnl_vector_fixed<RealType, 3>;
  using NeighborhoodIteratorType = itk::ConstNeighborhoodIterator<InputImageType>;
};

/** Helper functur to extract an {X, Y, Z} pixel from band information. */
template <typename InputImageType>
struct XYZ_t
{
  using InputV3 = typename input_types<InputImageType>::InputV3;

  explicit XYZ_t(InputImageType & image)
  {
#if OTB_VERSION_MAJOR >= 8
    auto const& kwl = image.GetImageMetadata();
#else
    auto const& kwl = image.GetImageKeywordlist();
#endif
    xband = value_or_unless(kwl, otb::bands::k_id_X, "extracting number of X band", 0);
    yband = value_or_unless(kwl, otb::bands::k_id_Y, "extracting number of Y band", 1);
    zband = value_or_unless(kwl, otb::bands::k_id_Z, "extracting number of Z band", 2);
    otbLogMacro(Info, <<"Using XYZ cartesian coordinates in bands " << xband << ", " << yband << ", " << zband);
  }

  template <typename PixelType>
  auto operator()(PixelType const& pixel) const {
    return otb::repack<InputV3>(pixel[xband], pixel[yband], pixel[zband]);
  }

private:
  int xband;
  int yband;
  int zband;
};

template <typename InputImageType, bool does_compute_mean_on_sides>
struct side_extractors_t;

/** Specialization that returns a single pixel value for each side. */
template <typename InputImageType>
struct side_extractors_t<InputImageType, false>
{
  side_extractors_t(
      FloatType        x_spacing,
      FloatType        y_spacing,
      FloatType        /*src_nodata*/,
      FloatType        /*dst_nodata*/,
      InputImageType & image)
    : XYZ(image)
    , above_idx{y_spacing > 0 ? 7 : 1}
    , below_idx{y_spacing > 0 ? 1 : 7}
    , left_idx {x_spacing > 0 ? 3 : 5}
    , right_idx{x_spacing > 0 ? 5 : 3}
    {}

  using NeighborhoodIteratorType = typename input_types<InputImageType>::NeighborhoodIteratorType;
  using InputV3                  = typename input_types<InputImageType>::InputV3;

  auto above(NeighborhoodIteratorType const& n) const { return otb::Above<InputV3>(XYZ(n.GetPixel(above_idx))); }
  auto below(NeighborhoodIteratorType const& n) const { return otb::Below<InputV3>(XYZ(n.GetPixel(below_idx))); }
  auto left (NeighborhoodIteratorType const& n) const { return otb::Left <InputV3>(XYZ(n.GetPixel(left_idx))); }
  auto right(NeighborhoodIteratorType const& n) const { return otb::Right<InputV3>(XYZ(n.GetPixel(right_idx))); }

  static constexpr auto center_idx = 4;
  auto center(NeighborhoodIteratorType const& n) const { return XYZ(n.GetPixel(center_idx)); }

private:
    XYZ_t<InputImageType> XYZ;
    int                   above_idx;
    int                   below_idx;
    int                   left_idx;
    int                   right_idx;
};

/** Specialization that returns pixel means on all four sides. */
template <typename InputImageType>
struct side_extractors_t<InputImageType, true>
{
  side_extractors_t(
      FloatType        x_spacing_,
      FloatType        y_spacing_,
      FloatType        src_nodata_,
      FloatType        dst_nodata_,
      InputImageType & image_)
    : XYZ(image_)
    , nodata_out{dst_nodata_, dst_nodata_, dst_nodata_}
    , all_above_idx{y_spacing_ > 0 ? std::array<int, 3>{6, 7, 8} : std::array<int, 3>{0, 1, 2}}
    , all_below_idx{y_spacing_ > 0 ? std::array<int, 3>{0, 1, 2} : std::array<int, 3>{6, 7, 8}}
    , all_left_idx {x_spacing_ > 0 ? std::array<int, 3>{0, 3, 6} : std::array<int, 3>{2, 5, 8}}
    , all_right_idx{x_spacing_ > 0 ? std::array<int, 3>{2, 5, 8} : std::array<int, 3>{0, 3, 6}}
    , src_nodata{src_nodata_}
    {}

  using NeighborhoodIteratorType = typename input_types<InputImageType>::NeighborhoodIteratorType;
  using InputV3                  = typename input_types<InputImageType>::InputV3;

  template <typename PixelType>
  bool is_invalid0(PixelType const& v) const
  {
    return
      // SARCartesianMeanEstimation will return {0, 0, 0}
      (v[0] == 0 && v[1] == 0 && v[2] == 0)
      // ossim may produce nan
      || std::isnan(v[0]) || std::isnan(v[1]) || std::isnan(v[2])
      // other tools may produce nodata, like SARDEMProjection2
      || (v[0] == src_nodata && v[1] == src_nodata && v[2] == src_nodata)
      ;
  }

  vnl_vector_fixed<FloatType, 3> mean_side(
      NeighborhoodIteratorType const& n,
      std::array<int,3>        const& all_idx) const
  {
    unsigned int nb_valids = 0;
    vnl_vector_fixed<FloatType, 3> res {0, 0, 0};
    for (auto idx : all_idx)
    {
      auto v = n.GetPixel(idx);
      if (! is_invalid0(v))
      {
        ++nb_valids;
        res[0] += v[0];
        res[1] += v[1];
        res[2] += v[2];
      }
    }
    return (nb_valids > 0) ? res / FloatType(nb_valids) : nodata_out;
  }

  auto above(NeighborhoodIteratorType const& n) const { return otb::Above<InputV3>(mean_side(n, all_above_idx)); }
  auto below(NeighborhoodIteratorType const& n) const { return otb::Below<InputV3>(mean_side(n, all_below_idx)); }
  auto left (NeighborhoodIteratorType const& n) const { return otb::Left <InputV3>(mean_side(n, all_left_idx )); }
  auto right(NeighborhoodIteratorType const& n) const { return otb::Right<InputV3>(mean_side(n, all_right_idx)); }

  static constexpr auto center_idx = 4;
  auto center(NeighborhoodIteratorType const& n) const { return XYZ(n.GetPixel(center_idx)); }

private:
    XYZ_t<InputImageType>          XYZ;
    vnl_vector_fixed<FloatType, 3> nodata_out;
    std::array<int,3>              all_above_idx;
    std::array<int,3>              all_below_idx;
    std::array<int,3>              all_left_idx;
    std::array<int,3>              all_right_idx;
    FloatType                      src_nodata;
};

/** Helper function emulating CTAD in C++14. */
template < bool shall_compute_mean_on_sides, typename InputImageType >
auto make_side_extractors(
    FloatType         x_spacing,
    FloatType         y_spacing,
    FloatType         src_nodata,
    FloatType         dst_nodata,
    InputImageType  & image)
{
  // x_spacing > 0 and y_spacing > 0 ==>
  //    6  7  8
  //    3  4  5
  //    0  1  2
  // x_spacing > 0 and y_spacing < 0 ==>
  //    0  1  2
  //    3  4  5
  //    6  7  8
  // x_spacing < 0 and y_spacing > 0 ==>
  //    8  7  6
  //    5  4  3
  //    2  1  0

  return side_extractors_t<InputImageType, shall_compute_mean_on_sides>(
      x_spacing, y_spacing, src_nodata, dst_nodata, image);
}

/**
 * Returns the lambda that'll be used to produce the normal vector at the
 * center of the neighbourhood.
 *
 * \note As a simplification, this function ignores Geiod correction on
 * neighbourhood pixels. They are all supposed identical.
 * \todo Handle cases with nodata => assume the data from the center; 0 if the
 * center is nodata
 */
template <bool shall_compute_mean_on_sides,
         typename OutputPixelType, typename InputImageType>
inline
auto AppSimpleNormalMethod(
    FloatType         x_spacing,
    FloatType         y_spacing,
    FloatType         src_nodata,
    FloatType         dst_nodata,
    InputImageType  & image)
{
  using InputV3                  = typename input_types<InputImageType>::InputV3;
  using NeighborhoodIteratorType = typename input_types<InputImageType>::NeighborhoodIteratorType;

  auto const side_extractors = make_side_extractors<shall_compute_mean_on_sides>(
        x_spacing, y_spacing, src_nodata, dst_nodata, image);

  auto l0 = otb::SimpleNormalMethodForCartesianPoints<vnl_vector_fixed<FloatType, 3>>(dst_nodata);

  auto const nodata_res = otb::repack<OutputPixelType>(dst_nodata, dst_nodata, dst_nodata);

  // Need to copy all local variables captured by the lambda
  auto lambda = [=](NeighborhoodIteratorType const& in)
  {
    // Compute n-> at current point
    assert(in.Size() == 9);

    auto const is_invalid = [src_nodata](auto v)
    {
      using otb::value;
      InputV3 const& w = value(v); // Remove the StrongType<> layer
      return
        // SARCartesianMeanEstimation will return {0, 0, 0}
        w == InputV3{0., 0., 0.}
        // ossim may produce nan
        || std::isnan(w[0]) || std::isnan(w[1]) || std::isnan(w[2])
#if 1
        // other tools may produce nodata;
        || (w[0] == src_nodata && w[1] == src_nodata && w[2] == src_nodata)
#endif
        ;
    };

    auto const a = side_extractors.above(in);
    auto const l = side_extractors.left(in);
    auto const r = side_extractors.right(in);
    auto const b = side_extractors.below(in);
    auto const c = side_extractors.center(in);
    if (is_invalid(a) || is_invalid(l) || is_invalid(r) || is_invalid(b) || is_invalid(c))
    {
      return nodata_res;
    }

    InputV3 n = l0(a, l, r, b);
    if (InputV3{0., 0., 0.} == n) // only way to be invalid => {0, 0, 0}
    {
      return nodata_res;
    }

    assert(! std::isnan(dot_product(n, c)));
    if (dot_product(n, c) < 0)
    {
      // TODO: we could expect this is the case for every single vector
      // computed for this image => factorize
      n = -n;
    }
    assert(dot_product(n, c) >= 0);
#if 0
    static int i = 0;
    if (++i % 10000000 == 0)
    {
      std::cout << "-----@" << in.GetIndex() << " from " <<
        InputV3(above(in)) << " ; " << InputV3(left(in)) <<  " ; " << InputV3(right(in)) <<  " ; " << InputV3(below(in))
        << " --> " << n
        <<std::endl;
    }
#endif

    auto const res = otb::repack<OutputPixelType>(n);
    return res;
  };
  return lambda;
}

} // anonymous namespace.

namespace otb
{

namespace Wrapper
{

/**
 * This application takes cares of building the normal vector from a DEM image.
 *
 * \author Luc Hermitte
 * \copyright CS Group
 * \ingroup App???
 * \ingroup SARCalibrationExtended
 */
class ExtractNormalVector : public ApplicationWithNoData<FloatType>
{
public:
  /** Standard class typedefs. */
  using Self             = ExtractNormalVector;
  // using InputPixelType   = itk::FixedArray<FloatType, 3>;
  /** Pixel Type of input image: VLV.
   * Some XYZ producers may provide mode than 3 bands... */
  using InputPixelType   = itk::VariableLengthVector<double>;
  using InputImageType   = otb::VectorImage<FloatType, 2>;
  using OutputPixelType  = itk::VariableLengthVector<FloatType>;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(ExtractNormalVector, unused);

  ExtractNormalVector()
    : ApplicationWithNoData("nodata", -32768)
    {}

private:
  void DoInit() override
  {
    SetName("ExtractNormalVector");
    SetDescription("This application produces normal vectors from a DEM image");

    // Documentation
    SetDocLongDescription("This application produces normal vectors from a DEM image" );


    SetDocLimitations("None");
    SetDocAuthors("Luc Hermitte (CS Group)");

    AddDocTag(Tags::Calibration);

    AddParameter(ParameterType_InputImage, "xyz",  "filename for the file containing XYZ cartesian coordinates aligned of their file geometry");
    SetParameterDescription("xyz", "filename for the file containing XYZ cartesian coordinates aligned of their file geometry");

    AddParameter(ParameterType_OutputImage, "out",  "Image of normal vectors");
    SetParameterDescription("out",  "Image of normal vectors");

    AddParameter(ParameterType_Bool, "mean", "Compute normals from means of cartesian positions on each side");
    SetParameterDescription("mean", "Compute normals from means of cartesian positions on each side. By default, use only the positions of the 4 immediate side pixels (right, left, above, below)");

    DoInit_NoData();

    AddRAMParameter();

    SetDocExampleParameterValue("xyz", "xyz.tif");
    SetDocExampleParameterValue("out", "normal.tif");
    SetOfficialDocLink();
  }

  void DoUpdateParameters() override
  {
  }

  template <bool does_compute_mean_on_sides>
  void RegisterFilter(
      FloatType x_spacing, FloatType y_spacing, FloatType dst_nodata,
      InputImageType & inputImage)
  {
    // This function has been made template inorder to avoid branching on pixel
    // fetching. Instead the branching is done once when registering the exact
    // filter.
    double const src_nodata = ExtractNoDataValueFromImage(inputImage);
    otbLogMacro(Info, << "NoData value in input XYZ grd image:  " << src_nodata);
    otbLogMacro(Info, << "NoData value in output normals image: " << dst_nodata);

    // λ that computes ground normal on a ground pixel according to its
    // neighbourhood.
    auto const lambda = AppSimpleNormalMethod<does_compute_mean_on_sides, OutputPixelType>(
        x_spacing, y_spacing, src_nodata, dst_nodata, inputImage);

    // Main FunctorImageFilter that computes round normals
    auto filter = otb::NewFunctorFilter(lambda, 3, {{1,1}});
    filter->SetInput(&inputImage);

    // Decorate with extract in-place filter that updates metadata
#if OTB_VERSION_MAJOR < 8
    auto add_meta = decorate_inplace<CompleteBandMetadataFilter>(
        filter, dst_nodata, "nX", "nY", "nZ");
#else
    // Remove meta data from SARDEMProjection2
    auto extra_clean_previous_metas = [](PortableMetadataType & meta)
    {
      erase_if(
          meta.ExtraKeys,
          [](auto const& kv){
          return starts_with(kv.first, bands::k_prefix_band)
          ||     starts_with(kv.first, bands::k_prefix_scandir)
          ||     kv.first == "Gain";
          });
      AddMetadata(meta, bands::k_id_nX, "0");
      AddMetadata(meta, bands::k_id_nY, "1");
      AddMetadata(meta, bands::k_id_nZ, "2");
    };

    auto add_meta = decorate_inplace<CompleteBandMetadataFilter>(
        filter,
        dst_nodata,
        MetaData::NewBands{"nX", "nY", "nZ"},
        extra_clean_previous_metas);
#endif

    SetParameterOutputImage("out", add_meta->GetOutput());

    // Call register pipeline to allow streaming and garbage collection
    RegisterPipeline();
  }

  void DoExecute() override
  {
    auto* inputImage = GetParameterImage<InputImageType>("xyz");
    assert(inputImage);

    auto const nodata = GetParameterNodata();
    // std::cout << "nodata      => " << nodata << std::endl;
    bool const does_compute_mean_on_sides = GetParameterInt("mean");
    otbLogMacro(Info, <<"Computing normals from "
        << (does_compute_mean_on_sides
          ?  "all pixels on each side" : "a single pixel from each side"));
    // AddNodataInMetadata(nodata, {"nX", "nY", "nZ"});

    auto const spacing = inputImage->GetSignedSpacing();
    // spacing is in double actually
    // static_assert(std::is_same<double, std::remove_cv_t<std::remove_reference_t<decltype(spacing[0])>>>::value, "mismatch");
    FloatType const x_spacing = spacing[0];
    FloatType const y_spacing = spacing[1];

    if (does_compute_mean_on_sides)
      RegisterFilter<true>(x_spacing, y_spacing, nodata, *inputImage);
    else
      RegisterFilter<false>(x_spacing, y_spacing, nodata, *inputImage);

    // Call register pipeline to allow streaming and garbage collection
    // RegisterPipeline();
  }
};

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::ExtractNormalVector)
