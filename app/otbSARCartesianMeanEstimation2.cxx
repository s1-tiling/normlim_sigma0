/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Applications/otbApplicationWithNoData.h"
#include "otbWrapperApplicationFactory.h"

#include "Filters/otbSARDEMPolygonsAnalysisImageFilter2.h"
#include "Filters/otbSARCartesianMeanFunctor2.h"

#include "otbWrapperOutputImageParameter.h"

#include <string>
#include <sstream>

namespace otb
{
namespace Wrapper
{

class SARCartesianMeanEstimation2 : public ApplicationWithNoData<double>
{
public:
  using Self                     = SARCartesianMeanEstimation2;
  using Pointer                  = itk::SmartPointer<Self>;

  itkNewMacro(Self);
  itkTypeMacro(SARCartesianMeanEstimation2, otb::Wrapper::Application);

  // Pixels
  using ImageInPixelType         = typename FloatVectorImageType::PixelType;
  using ImageOutPixelType        = typename FloatImageType::PixelType;

  // Function
  using PolygonsFunctorType      = otb::Function::SARPolygonsFunctor<ImageInPixelType, ImageOutPixelType>;
  using CartesianMeanFunctorType = otb::Function::SARCartesianMeanFunctor<ImageInPixelType, ImageInPixelType>;

  // Filters
  using FilterType               = otb::SARDEMPolygonsAnalysisImageFilter < FloatVectorImageType, FloatVectorImageType, FloatImageType, ComplexFloatImageType, CartesianMeanFunctorType >;

private:
  void DoInit() override
  {
    SetName("SARCartesianMeanEstimation2");
    SetDescription("SAR Cartesian mean estimation thanks to the associated DEM.");

    SetDocLongDescription("This application estimates a simulated cartesian mean image thanks to a DEM file.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "indemproj",   "Input vector of DEM projected into SAR geometry");
    SetParameterDescription("indemproj", "Input vector image for cartesian mean estimation.");

    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "DEM to extract DEM geometry.");

    AddParameter(ParameterType_InputImage,  "insar",   "Input SAR image");
    SetParameterDescription("insar", "SAR Image to extract SAR geometry.");

    AddParameter(ParameterType_Int,  "indirectiondemc", "Range direction for DEM scan");
    SetParameterDescription("indirectiondemc", "Range direction for DEM scan.");
    SetDefaultParameterInt("indirectiondemc", 1);
    MandatoryOff("indirectiondemc");

    AddParameter(ParameterType_Int,  "indirectiondeml", "Azimut direction for DEM scan");
    SetParameterDescription("indirectiondeml", "Azimut direction for DEM scan.");
    SetDefaultParameterInt("indirectiondeml", 1);
    MandatoryOff("indirectiondeml");

    AddParameter(ParameterType_Int, "mlran", "Averaging on distance (output geometry)");
    SetParameterDescription("mlran","Averaging on distance (output geometry)");
    SetDefaultParameterInt("mlran", 3);
    SetMinimumParameterIntValue("mlran", 1);
    MandatoryOff("mlran");

    AddParameter(ParameterType_Int, "mlazi", "Averaging on azimut (output geometry)");
    SetParameterDescription("mlazi","Averaging on azimut (output geometry)");
    SetDefaultParameterInt("mlazi", 3);
    SetMinimumParameterIntValue("mlazi", 1);
    MandatoryOff("mlazi");

    AddParameter(ParameterType_OutputImage, "out", "Output cartesian (mean) Image for DEM Projection");
    SetParameterDescription("out","Output cartesian (mean) Image for DEM Projection."
        " The first 3 bands contain X, Y and Z cartesian coordinates of the point in original SAR geometry."
        " The last band is a mask band: 0: XYZ coordinates cannot be computed; 1: coordinates are nominally computed; 2: coordinates are estimated, but point is estimated in shadow"
    );

    AddRAMParameter();

    SetDocExampleParameterValue("indemproj","CLZY_S21E055.tiff");
    SetDocExampleParameterValue("indem","S21E055.hgt");
    SetDocExampleParameterValue("insar","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
    SetDocExampleParameterValue("out","s1a-s4-simu-cartMean.tiff");
  }

  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute() override
  {
    // Get numeric parameters : ML factors and directions for DEM scan
    int mlRan = GetParameterInt("mlran");
    int mlAzi = GetParameterInt("mlazi");
    int DEMScanDirectionC = GetParameterInt("indirectiondemc");
    int DEMScanDirectionL = GetParameterInt("indirectiondeml");

    otbAppLogINFO(<<"ML Range : "<<mlRan);
    otbAppLogINFO(<<"ML Azimut : "<<mlAzi);
    otbAppLogINFO(<<"Direction (DEM scan) in range : "<<DEMScanDirectionC);
    otbAppLogINFO(<<"Direction (DEM scan) in azimut : "<<DEMScanDirectionL);

    // Start the first pipelines (Read SAR and DEM image metedata)
    ComplexFloatImageType::Pointer SARPtr = GetParameterComplexFloatImage("insar");
    FloatImageType::Pointer DEMPtr = GetParameterFloatImage("indem");

    // CartesianMeanEstimation Filter (use SARDEMPolygonsAnalysisImageFilter with SARCartesianMeanEstimationFunctor
    // to estimate cartesian coordonates mean image)
    FilterType::Pointer filterCartesianMeanEstimation = FilterType::New();
    m_Ref.push_back(filterCartesianMeanEstimation.GetPointer());
#if OTB_VERSION_MAJOR >= 8
    filterCartesianMeanEstimation->SetSARImageMetadata(SARPtr->GetImageMetadata());
#else
    filterCartesianMeanEstimation->SetSARImageKeyWorList(SARPtr->GetImageKeywordlist());
#endif
    filterCartesianMeanEstimation->SetSARImagePtr(SARPtr);
    filterCartesianMeanEstimation->SetDEMImagePtr(DEMPtr);
    filterCartesianMeanEstimation->SetDEMInformation(0, DEMScanDirectionC, DEMScanDirectionL);
    filterCartesianMeanEstimation->initializeMarginAndRSTransform();

    // Function Type : SARCartesianMeanEstimationFunctor
    int nbThreads = filterCartesianMeanEstimation->GetNumberOfThreads();
    CartesianMeanFunctorType::Pointer functor =  CartesianMeanFunctorType::New(nbThreads, mlRan, mlAzi,
        SARPtr->GetOrigin()[0]-0.5);
    filterCartesianMeanEstimation->SetFunctorPtr(functor);


    // Define the main pipeline (controlled with extended FileName)
    std::string origin_FileName = GetParameterString("out");

    // Check if FileName is extended (with the ? caracter)
    // If not extended then override the FileName
    if (origin_FileName.find("?") == std::string::npos && !origin_FileName.empty())
    {
      std::string extendedFileName = origin_FileName;

      // Get the ram value (in MB)
      int ram = GetParameterInt("ram");

      // Define with the ram value, the number of lines for the streaming
      int nbColSAR = SARPtr->GetLargestPossibleRegion().GetSize()[0];
      int nbLinesSAR = SARPtr->GetLargestPossibleRegion().GetSize()[1];
      // To determine the number of lines :
      // nbColSAR * nbLinesStreaming * sizeof(OutputPixel) = RamValue/2 (/2 to be sure)
      // OutputPixel are float => sizeof(OutputPixel) = 4 (Bytes)
      long int ram_In_KBytes = (ram/2) * 1024;
      long int nbLinesStreaming = (ram_In_KBytes / (nbColSAR)) * (1024/4);

      // Check the value of nbLinesStreaming
      int nbLinesStreamingMax = 10000;
      if (nbLinesStreamingMax > nbLinesSAR)
      {
        nbLinesStreamingMax = nbLinesSAR;
      }
      if (nbLinesStreaming <= 0 || nbLinesStreaming >  nbLinesStreamingMax)
      {
        nbLinesStreaming =  nbLinesStreamingMax;
      }

      // Construct the extendedPart
      std::ostringstream os;
      os << "?&streaming:type=stripped&streaming:sizevalue=" << nbLinesStreaming;

      // Add the extendedPart
      std::string extendedPart = os.str();
      extendedFileName.append(extendedPart);

      // Set the new FileName with extended options
      SetParameterString("out", extendedFileName);
    }

    // Execute the main Pipeline
    filterCartesianMeanEstimation->SetInput(GetParameterImage("indemproj"));
    SetParameterOutputImage("out", filterCartesianMeanEstimation->GetOutput());
  }
  // Vector for filters
  std::vector<itk::ProcessObject::Pointer> m_Ref;
};

}
}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARCartesianMeanEstimation2)
