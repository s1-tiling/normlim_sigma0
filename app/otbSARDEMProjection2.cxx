/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This file is a fork of DiapOTB SARDEMProjection adapted to OTB 8 source code
// for the needs of S1Tiling

#include "Applications/otbApplicationWithNoData.h"
// #include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "Filters/otbSARDEMProjectionImageFilter2.h"
#include "Filters/otbSARStreamingDEMInformationFilter.h"
#include "Filters/otbSARStreamingDEMCheckFilter.h"
#include "otbDEMHandler.h"
#include "otbConfigurationManager.h"

#include "cpl_error.h"
#include <boost/stacktrace.hpp>

#include <iostream>
#include <string>

void CPL_STDCALL MyGDALErrorHandler(CPLErr eErrClass, CPLErrorNum nError, char const* msg)
{
  CPLDefaultErrorHandler(eErrClass, nError, msg);
  if (eErrClass != CE_Debug)
  {
    std::cerr << boost::stacktrace::stacktrace();
  }
}

namespace otb
{
namespace Wrapper
{

class SARDEMProjection2 : public ApplicationWithNoData<double>
{
public:

  using Self    = SARDEMProjection2;
  using Pointer = itk::SmartPointer<Self>;

  itkNewMacro(Self);
  itkTypeMacro(SARDEMProjection2, useless);

  // Filters
  using DEMInfoFilterType  = otb::SARStreamingDEMInformationFilter<FloatImageType>;

private:

  void DoInit() override
  {
    SetName("SARDEMProjection2");
    SetDescription("Projects a DEM into SAR geometry.");

    SetDocLongDescription("This application puts a DEM file into SAR geometry"
        " and estimates two additional coordinates.\nIn all for each point of "
        "the DEM input four components are calculated : C (colunm into SAR "
        "image), L (line into SAR image), Z and Y.");

    // Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    // Parameter declarations
    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "DEM to perform projection on.");

    AddParameter(ParameterType_InputImage,  "insar",   "Input SAR image");
    SetParameterDescription("insar", "SAR Image to extract SAR geometry.");

    // Empty parameters to allow more components into the projeted DEM
    AddParameter(ParameterType_Bool, "withcryz", "Set col, row, and YZ coordinated components into projection");
    SetParameterDescription("withcryz", "If true, then set col, row, and YZ coordinated components into projection (actual default: true)");
    SetDefaultParameterInt("withcryz", true); // does not seem to work...

    AddParameter(ParameterType_Bool, "withh", "Set H components into projection");
    SetParameterDescription("withh", "If true, then H components into projection.");

    AddParameter(ParameterType_Bool, "withxyz", "Set XYZ Cartesian components into projection");
    SetParameterDescription("withxyz", "If true, then XYZ Cartesian components into projection.");

    AddParameter(ParameterType_Bool, "withsatpos", "Set SatPos components into projection");
    SetParameterDescription("withsatpos", "If true, then SatPos components into projection.");

    // Use NoData as double => nan can be used, as well as ints
    // But without guarantee that OTB#2305 is fixed, we need to read nodata as
    // a string.
    DoInit_NoData();

    AddParameter(ParameterType_OutputImage,
        "out",
        "Output Vector Image. Actual default pixel type is float32; However it becomes double64 when XYZ ECEF coordinates are produced.");
    SetParameterDescription(
        "out",
        "Output Vector Image (C,L,Z,Y)."
        "\nActual default pixel type is float32; However it becomes double64 when XYZ ECEF coordinates are produced.");
    SetDefaultOutputPixelType("out", ImagePixelType_uint8);
    // +-> OutputPixelType==uint8 makes voluntarily no sense => this will
    //     permit to detect the case "not set".

    AddParameter(ParameterType_Float,"gain","gain value (output of this application)");
    SetParameterDescription("gain", "Gain value.");
    SetParameterRole("gain", Role_Output);
    SetDefaultParameterFloat("gain", 0);

    AddParameter(
        ParameterType_Int,
        "directiontoscandemc","direction in range into DEM geometry to across SAR geometry from Near range to Far range (output of this applicxation)"
        );

    SetParameterDescription("directiontoscandemc", "Range direction for DEM scan.");
    SetParameterRole("directiontoscandemc", Role_Output);
    SetDefaultParameterInt("directiontoscandemc", 1);

    AddParameter(
        ParameterType_Int,
        "directiontoscandeml","direction in range into DEM geometry to across SAR geometry from Near range to Far range (output of this application)"
        );
    SetParameterDescription("directiontoscandeml", "Azimut direction for DEM scan.");
    SetParameterRole("directiontoscandeml", Role_Output);
    SetDefaultParameterInt("directiontoscandeml", 1);

    AddParameter(ParameterType_Group, "elev", "Elevation management");
    SetParameterDescription("elev", "This group of parameters allows managing elevation values.");

    AddParameter(ParameterType_InputFilename, "elev.geoid",
        "Input file path to Geoid. If not set, environment variable $OTB_GEOID_FILE will be read."
        " `-elev.geoid @` overrides $OTB_GEOID_FILE and forces to work without any Geoid data");
    SetParameterDescription("elev.geoid",
        "Input file path to Geoid. If not set, environment variable $OTB_GEOID_FILE will be read."
        " `-elev.geoid @` overrides $OTB_GEOID_FILE and forces to work without any Geoid data");
    MandatoryOff("elev.geoid");

    AddRAMParameter();

    SetDocExampleParameterValue("indem","S21E055.hgt");
    SetDocExampleParameterValue("insar","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
    SetDocExampleParameterValue("out","S21E55_proj.tif");
  }

  template <typename PixelFloatType>
  auto InstanciateSARDEMProjectionFilter(
      ComplexFloatImageType & sarInput,
      FloatImageType        * demInput,
      double                  noData,
      double                  gain,
      int                     directionDEM_forlines,
      int                     directionDEM_forcolunms
      )
  {
    // Filter Types
    using OutputImageType    = otb::VectorImage<PixelFloatType>;
    using DEMFilterType      = otb::SARDEMProjectionImageFilter2<FloatImageType, OutputImageType>;
    using DEMCheckFilterType = otb::SARStreamingDEMCheckFilter<OutputImageType>;

    // DEMProjection Filter
    typename DEMFilterType::Pointer filterDEMProjection = DEMFilterType::New(m_geoid_filename);
    m_Ref.push_back(filterDEMProjection.GetPointer());

#if OTB_VERSION_MAJOR >= 8
    filterDEMProjection->SetSARImageMetadata(sarInput.GetImageMetadata());
#else
    filterDEMProjection->SetSARImageKeyWorList(sarInput.GetImageKeywordlist());
#endif

    // Streaming Check Filter (To check if DEM cover the SAR Image)
    typename DEMCheckFilterType::Pointer filterStreamingDEMCheck = DEMCheckFilterType::New();
    m_Ref.push_back(filterStreamingDEMCheck.GetPointer());

    int sizeC = sarInput.GetLargestPossibleRegion().GetSize()[0];
    int sizeL = sarInput.GetLargestPossibleRegion().GetSize()[1];
    int originC = static_cast<int>(sarInput.GetOrigin()[0] - 0.5);
    int originL = static_cast<int>(sarInput.GetOrigin()[1] - 0.5);

    filterStreamingDEMCheck->setSARSizeAndOrigin(sizeC, sizeL, originC, originL);
    filterDEMProjection->SetInput(demInput);

    filterDEMProjection->SetwithH     (is_true("withh",      false));
    filterDEMProjection->SetwithXYZ   (is_true("withxyz",    false));
    filterDEMProjection->SetwithSatPos(is_true("withsatpos", false));
    auto const shall_produce_cryz = is_true("withcryz",   true);
    filterDEMProjection->SetwithCRYZ  (shall_produce_cryz);

    filterDEMProjection->SetNoData(noData);

    // Pass to DEMProjection filter, dem information to save into metadata
    if (shall_produce_cryz)
    {
      // This check
      filterDEMProjection->SetdirectionToScanDEMC(directionDEM_forcolunms);
      filterDEMProjection->SetdirectionToScanDEML(directionDEM_forlines);
      filterDEMProjection->Setgain(gain);

      // Check if DEM cover SAR Image (Counter must be > 0)
      filterStreamingDEMCheck->SetInput(filterDEMProjection->GetOutput());
      // Adapt streaming with ram parameter (default 256 MB)
      filterStreamingDEMCheck->GetStreamer()->SetAutomaticStrippedStreaming(GetParameterInt("ram"));
      filterStreamingDEMCheck->Update();

      long int counterOfDEMPts_intoSARGeo = 0;
      filterStreamingDEMCheck->GetDEMCheck(counterOfDEMPts_intoSARGeo);

      if (counterOfDEMPts_intoSARGeo <= 0)
      {
        std::cout << "Input DEM does not cover the SAR geometry. Please check your DEM" << std::endl;
        otbAppLogWARNING(<<"Input DEM does not cover the SAR geometry");
      } else
      {
        otbLogMacro(Info, << "Input DEM and SAR geometries intersect");
      }
    }

    return filterDEMProjection->GetOutput();
  }

  void DoUpdateParameters() override
  {
  }

  bool is_true(std::string const& key, bool def) const
  {
    bool const is_set = IsParameterEnabled(key) && HasValue(key);
    bool res = (is_set && GetParameterInt(key)) || (!is_set && def);
    // otbLogMacro(Info, << "Enabled  : " << key << " --> " << IsParameterEnabled(key));
    // otbLogMacro(Info, << "HasValue : " << key << " --> " << HasValue(key));
    // otbLogMacro(Info, << key << " --> " << res);
    return res;
  };

  void DoExecute() override
  {
    if (otb::Logger::Instance()->GetPriorityLevel() == itk::LoggerBase::DEBUG)
    {
      // With OTB_LOGGER_LEVEL=DEBUG, override GDAL logger handler to display
      // stacktrace on GDAL error messages.
      CPLSetErrorHandler(MyGDALErrorHandler);
    }

    // ====[ Update Elevation information
    // GEOID parameter is best taken, in priority order:
    // 1. As -elev.geom
    // 2. As $OTB_GEOID_FILE
    // 3. As: ignored
    auto get_geoid_filename = [&]() {
      if (IsParameterEnabled("elev.geoid") && HasValue("elev.geoid"))
      {
        // OTB dont permit to express, here is a parameter that is... empty
        // So, let's say that empty is expressed "@"
        auto const res = GetParameterString("elev.geoid");
        return res == "@" ? "" : res;
      } else
        return ConfigurationManager::GetGeoidFile();
    };

    m_geoid_filename = get_geoid_filename();

    // ====[ Set elevation information
#if OTB_VERSION_MAJOR >= 8
    auto& demHandler = DEMHandler::GetInstance();
    demHandler.ClearElevationParameters(); // NO!!!!!
#else
    auto& demHandler = *DEMHandler::Instance();
    demHandler.ClearDEMs();
#endif
    otbAppLogINFO(<< "Geoid file used : " << (m_geoid_filename.empty() ? "None" : m_geoid_filename));
    demHandler.SetDefaultHeightAboveEllipsoid(0);
    if (! m_geoid_filename.empty())
    {
      demHandler.OpenGeoidFile(m_geoid_filename);
    }

    // ====[ Get No data value
    auto noData = GetParameterNodata();
    otbAppLogINFO(<<"Dst No Data : "<<noData);

    // ====[ Start the first pipeline (Read SAR image metedata)
    ComplexFloatImageType::Pointer SARPtr = GetParameterComplexFloatImage("insar");

    // ====[ Streaming Gain Filter
    DEMInfoFilterType::Pointer filterStreamingDEMInfo = DEMInfoFilterType::New();
    m_Ref.push_back(filterStreamingDEMInfo.GetPointer());
#if OTB_VERSION_MAJOR >= 8
    filterStreamingDEMInfo->SetSARImageMetadata(SARPtr->GetImageMetadata());
#else
    filterStreamingDEMInfo->SetSARImageKeyWorList(SARPtr->GetImageKeywordlist());
#endif

    // ====[ Define the main pipeline
    FloatImageType::Pointer inputDEM = GetParameterFloatImage("indem");

    // Define the pipeline to estimate the gain
    filterStreamingDEMInfo->SetInput(inputDEM);
    // Adapt streaming with ram parameter (default 256 MB)

    filterStreamingDEMInfo->GetStreamer()->SetAutomaticStrippedStreaming(GetParameterInt("ram"));

    filterStreamingDEMInfo->Update();

    double gain;
    int directionDEM_forlines, directionDEM_forcolunms;
    filterStreamingDEMInfo->GetDEMInformation(gain, directionDEM_forcolunms, directionDEM_forlines);

    // ====[ Override unset output pixel type
    // If Cartesian coordinates (ground point, or sensor position) are
    // produced, then the working filter will be set to produce doubles and not
    // floats. Indeed, the mantissa of 32bits floats is just able to represent
    // 8 to 9 digits. This is not enough for coordinates used to represent a
    // heights of ~ 6000..7000 km.
    if (GetParameterOutputImagePixelType("out") == ImagePixelType_uint8)
    {
      auto pixel_type = (is_true("withxyz", false) || is_true("withsatpos", false))
        ? ImagePixelType_double
        : ImagePixelType_float;
      otbLogMacro(Debug, << "Override default output format to " << pixel_type);
      SetParameterOutputImagePixelType("out", pixel_type);
    }

    // ====[ Register filter and go!
    if (GetParameterOutputImagePixelType("out") == ImagePixelType_float)
    {
      otbLogMacro(Debug, << "Image will be internally computed in float32 precision");
      auto * output = InstanciateSARDEMProjectionFilter<float>(
          *SARPtr, inputDEM, noData, gain,
          directionDEM_forlines, directionDEM_forcolunms);
      // Output
      SetParameterOutputImage("out", output);
    }
    else
    {
      // Every internal computations are on doubles
      // => expect this output type by default
      otbLogMacro(Debug, << "Image will be internally computed in double64 precision");
      auto * output = InstanciateSARDEMProjectionFilter<double>(
          *SARPtr, inputDEM, noData, gain,
          directionDEM_forlines, directionDEM_forcolunms);
      // Output
      SetParameterOutputImage("out", output);
    }

    // ====[ Assign Gain and Directions
    SetParameterFloat("gain", gain);
    SetParameterInt("directiontoscandemc",  directionDEM_forcolunms);
    SetParameterInt("directiontoscandeml",  directionDEM_forlines);
  }

  // Vector for filters; TODO Use RegisterPipeline()
  std::vector<itk::ProcessObject::Pointer> m_Ref;

  std::string m_geoid_filename;
};

} // namespace otb::Wrapper
} // namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::SARDEMProjection2)
