/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This file is a fork of DiapOTB SARComputeGroundAndSatPositionsOnDEM adapted to OTB 8 source code
// for the needs of S1Tiling

#include "Applications/otbApplicationWithNoData.h"
#include "Filters/otbSARComputeGroundAndSatPositionsOnDEMImageFilter.h"
#include "SAR/otbPreciseOrbit.h"
#include "SAR/otbPositionHelpers.h"
#include "SAR/otbFindOrbit.h"
#include "otbLogHelpers.h"
#include "otbWrapperApplicationFactory.h"

#include "otbExtractROI.h"
#include "otbDEMHandler.h"
#include "otbConfigurationManager.h"

#include "cpl_error.h"
#include <boost/stacktrace.hpp>

#include <iostream>
#include <string>
#include <array>

void CPL_STDCALL MyGDALErrorHandler(CPLErr eErrClass, CPLErrorNum nError, char const* msg)
{
  CPLDefaultErrorHandler(eErrClass, nError, msg);
  if (eErrClass != CE_Debug)
  {
    std::cerr << boost::stacktrace::stacktrace();
  }
}

namespace
{ // Anonymous namespace

using soa_lonlathgt = std::array<std::array<double, 4>, 3>;
using aos_lonlathgt = std::array<otb::Orbit::PointType, 4>;

template <typename ImageType>
aos_lonlathgt
GetImageCornersAsLatLon(ImageType const& image)
{
  auto const projection_ref = image.GetProjectionRef();
  auto const& region = image.GetLargestPossibleRegion();
  auto const& index  = region.GetIndex();
  auto const& size   = region.GetSize();

  long const x0 = index[0];
  long const y0 = index[1];
  long const x1 = index[0] + size[0] - 1;
  long const y1 = index[1] + size[1] - 1;

  // Part to extract height coordinate in DEM+Geoid corners
  using ExtractROIFilterType = otb::ExtractROI<typename ImageType::PixelType, typename ImageType::PixelType>;
  typename ExtractROIFilterType::Pointer extractor = ExtractROIFilterType::New();
  extractor->SetInput(&image);
  // typename ImageType::IndexType idx0{0, 0};
  typename ImageType::SizeType one_pix_size;
  one_pix_size.Fill(1);
  typename ImageType::RegionType one_pix_region;
  one_pix_region.SetSize(one_pix_size);

  std::array<double, 4> xs = {};
  std::array<double, 4> ys = {};
  std::array<double, 4> hs = {};

  unsigned i = 0;
  for (auto x: {x0, x1})
    for (auto y : {y0, y1})
    {
      typename ImageType::IndexType idx{x, y};
      auto const phy_position = TransformIndexToPhysicalPoint(image, idx);
      xs[i] = phy_position[0];
      ys[i] = phy_position[1];
      ++i;

      one_pix_region.SetIndex(idx);
      otbLogMacro(Info, << "Extracting pixel φ" << phy_position << " -> " << otb::NeatRegionLogger(one_pix_region));
      extractor->SetExtractionRegion(one_pix_region);
      extractor->Update();
      idx.Fill(0); // index relative to ROI region
      hs[i] = extractor->GetOutput()->GetPixel(idx);
    }

  auto transform = otb::make_transform_to_lonlat_for(projection_ref);
  if (transform)
  {
    // static_assert(xs.size() == ys.size(), "Same size of 4 is expected");
#if GDAL_VERSION_NUM >= 3030000
    // #   pragma message "GDAL >= 3.3 => Using TransformWithErrorCodes"
    transform->TransformWithErrorCodes(
        xs.size(),
        &xs[0], &ys[0], &hs[0], nullptr,
        nullptr);
#else
#   pragma message "GDAL < 3.3 => Using Transform"
    transform->Transform(
        xs.size(),
        &xs[0], &ys[0], &hs[0], nullptr,
        nullptr);
#endif
  }
  return {{
    otb::repack<otb::Orbit::PointType>(xs[0], ys[0], hs[0]),
      otb::repack<otb::Orbit::PointType>(xs[1], ys[1], hs[1]),
      otb::repack<otb::Orbit::PointType>(xs[2], ys[2], hs[2]),
      otb::repack<otb::Orbit::PointType>(xs[3], ys[3], hs[3]),
  }};
}

} // Anonymous namespace


namespace otb
{
namespace Wrapper
{

class SARComputeGroundAndSatPositionsOnDEM : public ApplicationWithNoData<double>
{
public:

  using Self    = SARComputeGroundAndSatPositionsOnDEM;
  using Pointer = itk::SmartPointer<Self>;

  itkNewMacro(Self);
  itkTypeMacro(SARComputeGroundAndSatPositionsOnDEM, useless);

private:

  void DoInit() override
  {
    SetName("SARComputeGroundAndSatPositionsOnDEM");
    SetDescription("Projects a DEM into SAR geometry.");

    SetDocLongDescription("This application puts a DEM file into SAR geometry"
        " and estimates two additional coordinates.");

    // Optional descriptors
    SetDocLimitations("Only Sentinel 1-A and -B (IW and StripMap mode) products are supported for now.");
    SetDocAuthors("OTB-Team");
    AddDocTag(Tags::SAR);

    // Parameter declarations
    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "DEM to perform projection on.");

    AddParameter(ParameterType_String,  "ineof", "Input Precise Orbit File");
    SetParameterDescription("ineof", "Name of Precise Orbit File");

    // TODO: Support an intermediary mode where caller knows exactly the
    // absolute number to use.
    AddParameter(ParameterType_Int, "inrelorb", "Reference Relative Orbit Number");
    SetParameterDescription("inrelorb", "Reference Relative Orbit Number, aka track.");

    // Empty parameters to allow more components into the projeted DEM
    AddParameter(ParameterType_Bool, "withh", "Set H components into projection");
    SetParameterDescription("withh", "If true, then H components into projection.");

    AddParameter(ParameterType_Bool, "withxyz", "Set XYZ Cartesian components into projection");
    SetParameterDescription("withxyz", "If true, then XYZ Cartesian components into projection.");

    AddParameter(ParameterType_Bool, "withsatpos", "Set SatPos components into projection");
    SetParameterDescription("withsatpos", "If true, then SatPos components into projection.");

    // Use NoData as double => nan can be used, as well as ints
    // But without guarantee that OTB#2305 is fixed, we need to read nodata as
    // a string.
    DoInit_NoData();

    AddParameter(ParameterType_OutputImage,
        "out",
        "Output Vector Image. Actual default pixel type is float64.");
    SetParameterDescription(
        "out",
        "Output Vector Image."
        "\nActual default pixel type is float64.");
    SetDefaultOutputPixelType("out", ImagePixelType_double);

    AddParameter(ParameterType_Group, "elev", "Elevation management");
    SetParameterDescription("elev", "This group of parameters allows managing elevation values.");

    AddParameter(ParameterType_InputFilename, "elev.geoid",
        "Input file path to Geoid. If not set, environment variable $OTB_GEOID_FILE will be read."
        " `-elev.geoid @` overrides $OTB_GEOID_FILE and forces to work without any Geoid data");
    SetParameterDescription("elev.geoid",
        "Input file path to Geoid. If not set, environment variable $OTB_GEOID_FILE will be read."
        " `-elev.geoid @` overrides $OTB_GEOID_FILE and forces to work without any Geoid data");
    MandatoryOff("elev.geoid");

    AddRAMParameter();

    SetDocExampleParameterValue("indem","S21E055.hgt");
    SetDocExampleParameterValue("ineof","TODO");
    SetDocExampleParameterValue("out","S21E55_proj.tif");
  }

  template <typename PixelFloatType>
  auto InstanciateSARComputeGroundAndSatPositionsOnDEMFilter(
      std::vector<Orbit> const& OSVs,
      FloatImageType          * demInput,
      double                    noData
  )
  {
    // Filter Types
    using OutputImageType = otb::VectorImage<PixelFloatType>;
    using DEMFilterType   = otb::SARComputeGroundAndSatPositionsOnDEMImageFilter<FloatImageType, OutputImageType>;

    // DEMProjection Filter
    typename DEMFilterType::Pointer filterDEMProjection = DEMFilterType::New(m_geoid_filename, OSVs);

    filterDEMProjection->SetInput(demInput);

    filterDEMProjection->SetwithH     (is_true("withh",      false));
    filterDEMProjection->SetwithXYZ   (is_true("withxyz",    true));
    filterDEMProjection->SetwithSatPos(is_true("withsatpos", true));

    filterDEMProjection->SetNoData(noData);

    // Output
    auto * output = filterDEMProjection->GetOutput();
    SetParameterOutputImage("out", output);

    // Call register pipeline to allow streaming and garbage collection
    RegisterPipeline();

    return output;
  }

  void DoUpdateParameters() override
  {
  }

  bool is_true(std::string const& key, bool def) const
  {
    bool const is_set = IsParameterEnabled(key) && HasValue(key);
    bool res = (is_set && GetParameterInt(key)) || (!is_set && def);
    // otbLogMacro(Info, << "Enabled  : " << key << " --> " << IsParameterEnabled(key));
    // otbLogMacro(Info, << "HasValue : " << key << " --> " << HasValue(key));
    // otbLogMacro(Info, << key << " --> " << res);
    return res;
  };

  void DoExecute() override
  {
    if (otb::Logger::Instance()->GetPriorityLevel() == itk::LoggerBase::DEBUG)
    {
      // With OTB_LOGGER_LEVEL=DEBUG, override GDAL logger handler to display
      // stacktrace on GDAL error messages.
      CPLSetErrorHandler(MyGDALErrorHandler);
    }

    // ====[ Update Elevation information
    // GEOID parameter is best taken, in priority order:
    // 1. As -elev.geom -- where "@" <=> none
    // 2. As $OTB_GEOID_FILE
    // 3. As: ignored
    auto get_geoid_filename = [&]() {
      if (IsParameterEnabled("elev.geoid") && HasValue("elev.geoid"))
      {
        // OTB dont permit to express, here is a parameter that is... empty
        // So, let's say that empty is expressed "@"
        auto res = GetParameterString("elev.geoid");
        return res == "@" ? "" : res;
      } else
        return ConfigurationManager::GetGeoidFile();
    };

    m_geoid_filename = get_geoid_filename();

    // ====[ Set elevation information
    auto& demHandler = DEMHandler::GetInstance();
    demHandler.ClearElevationParameters(); // NO!!!!!
    otbAppLogINFO(<< "Geoid file used : " << (m_geoid_filename.empty() ? "None" : m_geoid_filename));
    demHandler.SetDefaultHeightAboveEllipsoid(0); // TODO: should we use an option?
    if (! m_geoid_filename.empty())
    {
      demHandler.OpenGeoidFile(m_geoid_filename);
    }

    // ====[ Get No data value
    auto const noData = GetParameterNodata();
    otbAppLogINFO(<<"Output No Data : "<<noData);

    // ====[ Read precise orbit file
    std::string const eof_file = GetParameterString("ineof");
    int const rel_orb_num = GetParameterInt("inrelorb");
    auto OSVs = read_precise_orbit(eof_file, rel_orb_num);

    // ====[ Define the main pipeline
    FloatImageType::Pointer inputDEM = GetParameterFloatImage("indem");

    // ====[ Determine best date range that covers inputDEM
    auto corners = ::GetImageCornersAsLatLon(*inputDEM);
    FilterOrbit(OSVs, corners);

    // ====[ Register filter and go!
    if (GetParameterOutputImagePixelType("out") == ImagePixelType_float)
    {
      otbLogMacro(Debug, << "Image will be internally computed in float32 precision");
      auto * output = InstanciateSARComputeGroundAndSatPositionsOnDEMFilter<float>(
          OSVs.orbit, inputDEM, noData);
      (void) output;
    }
    else
    {
      // Every internal computations are on doubles
      // => expect this output type by default
      otbLogMacro(Debug, << "Image will be internally computed in double64 precision");
      auto * output = InstanciateSARComputeGroundAndSatPositionsOnDEMFilter<double>(
          OSVs.orbit, inputDEM, noData);
      (void) output;
    }

  }

  std::string m_geoid_filename;
};

} // namespace otb::Wrapper
} // namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::SARComputeGroundAndSatPositionsOnDEM)
