/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// #include "otbCompleteBandMetadataFilter.h"
// #include "otbBandIds.h"

#include "Applications/otbApplicationWithNoInputImage.h"
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "Filters/otbComputeNormalsOnEllipsoidImageSource.h"

namespace otb
{

namespace Wrapper
{

/**
 * This application takes cares of building the normal vector on ellipsoid surface, on the specified
 * grid.
 *
 * \author Luc Hermitte
 * \copyright CS Group
 * \ingroup App???
 * \ingroup SARCalibrationExtended
 */
class ExtractNormalVectorToEllipsoid
: public ApplicationWithoutInputImage<>
{
public:
  /** Standard class typedefs. */
  using Self             = ExtractNormalVectorToEllipsoid;
  using OutputPixelType  = itk::VariableLengthVector<double>;

  /** Standard macros */
  itkNewMacro(Self);
  itkTypeMacro(ExtractNormalVectorToEllipsoid, unused);

  ExtractNormalVectorToEllipsoid() = default;

private:
  void DoInit() override
  {
    SetName("ExtractNormalVectorToEllipsoid");
    SetDescription("This application takes cares of building the normal vector on ellipsoid surface, on the specified grid.");

    SetDocLongDescription("This application takes cares of building the normal vector on ellipsoid surface, on the specified grid." );
    SetDocLimitations("None");
    SetDocAuthors("Luc Hermitte (CS Group)");

    AddDocTag(Tags::Calibration);

    // Set the parameters
    AddParameter(ParameterType_OutputImage, "out",  "Image of normal vectors");
    SetParameterDescription("out",  "Image of normal vectors");

    DoInit_NoInputImage();

    AddRAMParameter();

    // Doc example parameter settings
    SetDocExampleParameterValue("out", "normal.tif");

    SetOfficialDocLink();
  }

  void DoUpdateParameters() override
  {
    // We cannot use GetParameterDouble("*") to obtain the parameters passed in order to compute
    // missing parameters.
    // Indeed, in DoUpdateParameters(), we can only fetch the default values.
    // As such, here we do nothing, and we set the defaults in DoUpdateParameters_NoInputImage().
  }

  void DoExecute() override
  {
    this->DoUpdateParameters_NoInputImage();

    // can use auto because a GeneratorType::Pointer is returned
    auto generator = this->InstanciateImageGenerator<otb::ComputeNormalsOnEllipsoidImageSource>();
    this->SetParameterOutputImage<DoubleVectorImageType>("out", generator->GetOutput());

    // Call register pipeline to allow streaming and garbage collection
    RegisterPipeline();
  }

  std::string                 m_OutputProjectionRef;
};

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::ExtractNormalVectorToEllipsoid)
