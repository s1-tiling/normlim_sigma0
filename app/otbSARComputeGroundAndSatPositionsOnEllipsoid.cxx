/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Applications/otbApplicationWithNoInputImage.h"
#include "Applications/otbApplicationWithNoData.h"
#include "otbWrapperApplicationFactory.h"

#include "Filters/otbSARComputeGroundAndSatPositionsOnEllipsoidImageSource.h"
#include "SAR/otbPreciseOrbit.h"
#include "SAR/otbFindOrbit.h"

#include "cpl_error.h"
#include <boost/stacktrace.hpp>

#include <iostream>
#include <string>
#include <array>

void CPL_STDCALL MyGDALErrorHandler(CPLErr eErrClass, CPLErrorNum nError, char const* msg)
{
  CPLDefaultErrorHandler(eErrClass, nError, msg);
  if (eErrClass != CE_Debug)
  {
    std::cerr << boost::stacktrace::stacktrace();
  }
}

/*-------------------------------[ The application ]-------------------------*/
namespace otb
{
namespace Wrapper
{

class SARCalibrationExtended_EXPORT SARComputeGroundAndSatPositionsOnEllipsoid
: public ApplicationWithoutInputImage<ApplicationWithNoData<double>>
{
public:

  using Self    = SARComputeGroundAndSatPositionsOnEllipsoid;
  using Pointer = itk::SmartPointer<Self>;

  itkNewMacro(Self);
  itkTypeMacro(SARComputeGroundAndSatPositionsOnEllipsoid, useless);

private:

  void DoInit() override
  {
    SetName("SARComputeGroundAndSatPositionsOnEllipsoid");
    SetDescription("Computes XYZ and satellite position matching ellipsoid lat/lon");

    SetDocLongDescription("Computes XYZ and satellite position matching ellipsoid lat/lon");

    // Optional descriptors
    SetDocLimitations("Only Sentinel 1-A and -B (IW and StripMap mode) products are supported for now.");
    SetDocAuthors("OTB-Team");
    AddDocTag(Tags::SAR);

    // Parameter declarations
    AddParameter(ParameterType_String,  "ineof", "Input Precise Orbit File");
    SetParameterDescription("ineof", "Name of Precise Orbit File");

    // TODO: Support an intermediary mode where caller knows exactly the
    // absolute number to use.
    AddParameter(ParameterType_Int, "inrelorb", "Reference Relative Orbit Number");
    SetParameterDescription("inrelorb", "Reference Relative Orbit Number, aka track.");

    // Empty parameters to allow more components into the projeted DEM
    AddParameter(ParameterType_Bool, "withh", "Set H components into projection");
    SetParameterDescription("withh", "If true, then H components into projection.");

    AddParameter(ParameterType_Bool, "withxyz", "Set XYZ Cartesian components into projection");
    SetParameterDescription("withxyz", "If true, then XYZ Cartesian components into projection.");

    AddParameter(ParameterType_Bool, "withsatpos", "Set SatPos components into projection");
    SetParameterDescription("withsatpos", "If true, then SatPos components into projection.");

    // Use NoData as double => nan can be used, as well as ints
    // But without guarantee that OTB#2305 is fixed, we need to read nodata as
    // a string.
    this->DoInit_NoData();
    DoInit_NoInputImage();

    AddParameter(ParameterType_OutputImage,
        "out",
        "Output Vector Image. Actual default pixel type is float64.");
    SetParameterDescription(
        "out",
        "Output Vector Image."
        "\nActual default pixel type is float64.");
    SetDefaultOutputPixelType("out", ImagePixelType_double);

    DoInit_NoInputImage();

    AddRAMParameter();

    SetDocExampleParameterValue("ineof","TODO");
    SetDocExampleParameterValue("out","S21E55_proj.tif");
  }

  template <typename PixelFloatType>
  auto InstanciateSARComputeGroundAndSatPositionsOnEllipsoidFilter(
      std::vector<Orbit> OSVs,
      double             noData
  )
  {
    // Filter Types
    using OutputImageType    = otb::VectorImage<PixelFloatType>;
    using ImageGeneratorType = otb::SARComputeGroundAndSatPositionsOnEllipsoidImageSource<OutputImageType>;

    // can use auto because a GeneratorType::Pointer is returned
    auto generator = this->InstanciateImageGenerator<ImageGeneratorType>(std::move(OSVs));

    generator->SetwithH     (is_true("withh",      false));
    generator->SetwithXYZ   (is_true("withxyz",    true));
    generator->SetwithSatPos(is_true("withsatpos", true));

    generator->SetNoData(noData);

    // Output
    auto * output = generator->GetOutput();
    SetParameterOutputImage("out", output);

    // Call register pipeline to allow streaming and garbage collection
    RegisterPipeline();

    return output;
  }

  void DoUpdateParameters() override
  {
  }

  bool is_true(std::string const& key, bool def) const
  {
    bool const is_set = IsParameterEnabled(key) && HasValue(key);
    bool res = (is_set && GetParameterInt(key)) || (!is_set && def);
    // otbLogMacro(Info, << "Enabled  : " << key << " --> " << IsParameterEnabled(key));
    // otbLogMacro(Info, << "HasValue : " << key << " --> " << HasValue(key));
    // otbLogMacro(Info, << key << " --> " << res);
    return res;
  };

  void DoExecute() override
  {
    // ====[ Make sure all region related parameters are set
    this->DoUpdateParameters_NoInputImage();

    // ====[ Register logger for gdal errors
    if (otb::Logger::Instance()->GetPriorityLevel() == itk::LoggerBase::DEBUG)
    {
      // With OTB_LOGGER_LEVEL=DEBUG, override GDAL logger handler to display
      // stacktrace on GDAL error messages.
      CPLSetErrorHandler(MyGDALErrorHandler);
    }

    // ====[ Get No data value
    auto const noData = GetParameterNodata();
    otbAppLogINFO(<<"Output No Data : "<<noData);

    // ====[ Read precise orbit file
    std::string const eof_file = GetParameterString("ineof");
    int const rel_orb_num = GetParameterInt("inrelorb");
    auto OSVs = read_precise_orbit(eof_file, rel_orb_num);

    // ====[ Determine best date range that covers inputDEM
    auto corners = GetImageCornersAsLatLon(
        GetParameterULX(), GetParameterULY(),
        GetParameterLRX(), GetParameterLRY(),
        GetOutputProjectionRef(),
        [](auto, auto) { return 0.;}
    );
    FilterOrbit(OSVs, corners);

    // ====[ Register filter and go!
    if (GetParameterOutputImagePixelType("out") == ImagePixelType_float)
    {
      otbLogMacro(Debug, << "Image will be internally computed in float32 precision");
      auto * output = InstanciateSARComputeGroundAndSatPositionsOnEllipsoidFilter<float>(
          OSVs.orbit, noData);
      (void) output;
    }
    else
    {
      // Every internal computations are on doubles
      // => expect this output type by default
      otbLogMacro(Debug, << "Image will be internally computed in double64 precision");
      auto * output = InstanciateSARComputeGroundAndSatPositionsOnEllipsoidFilter<double>(
          OSVs.orbit, noData);
      (void) output;
    }

  }

  std::string m_geoid_filename;
};

} // namespace otb::Wrapper
} // namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::SARComputeGroundAndSatPositionsOnEllipsoid)
