/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Filters/otbComputeNormalsOnEllipsoidImageSource.h"
#include "SAR/otbPositionHelpers.h"
#include "Filters/otbBandIds.h"
#include "Common/otbMdSpan.h"
#include "Metadata/otbMetadataHelper.h"
#include "otbMath.h"
#include "otbLogHelpers.h"
#include "itkImageScanlineIterator.h"


namespace
{ // Anonymous namespace
using sincos_view_t = otb::mdspan<double, 4, otb::dynamic_extent>;

constexpr double to_degrees = otb::CONST_PI_180;

/**
 * Compute sines and cosines on a series of (lon, lat) coordinates.
 * \param[in]  lonlat_view  input 2D array of longitudes and latitudes
 * \param[out] sincos_view  ouput 2D array of sines and cosines
 *
 * \pre  `sincos_view` first row will contain sines of longitudes
 * \pre  `sincos_view` first row will contain cosines of longitudes
 * \pre  `sincos_view` first row will contain sines of latitudes
 * \pre  `sincos_view` first row will contain cosines of latitudes
 * \throw None
 */
void ComputeAllSinCos(
    otb::lonlat_view_t const lonlat_view,
    sincos_view_t            sincos_view
)
{
  assert(lonlat_view.extent(0) == 2);
  assert(sincos_view.extent(0) == 4);
  assert(lonlat_view.extent(1) == sincos_view.extent(1));

  auto const line_size = lonlat_view.extent(1);
  for (std::ptrdiff_t i = 0; i < line_size; ++i)
  {
    auto const lon = lonlat_view(0, i) * to_degrees;
    sincos_view(0, i) = std::sin(lon);
    sincos_view(1, i) = std::cos(lon);

    auto const lat = lonlat_view(1, i) * to_degrees;
    sincos_view(2, i) = std::sin(lat);
    sincos_view(3, i) = std::cos(lat);
  }
}

} // Anonymous namespace


void
otb::ComputeNormalsOnEllipsoidImageSource
::GenerateOutputInformation()
{
  Superclass :: GenerateOutputInformation();

  OutputImagePointer output = this->GetOutput();
  output->SetNumberOfComponentsPerPixel(3);

  auto meta = GetImageMetadata(*output);
  // Metadatas that tell the band number associated to a key
  AddMetadata(meta, bands::k_id_nX, "0");
  AddMetadata(meta, bands::k_id_nY, "1");
  AddMetadata(meta, bands::k_id_nZ, "2");

  // Bands names
  meta.Bands.clear();
  ImageMetadataBase bmd;
  AddMetadata(bmd, MDStr::BandName, "nX");
  meta.Bands.push_back(std::move(bmd));
  AddMetadata(bmd, MDStr::BandName, "nY");
  meta.Bands.push_back(std::move(bmd));
  AddMetadata(bmd, MDStr::BandName, "nZ");
  meta.Bands.push_back(std::move(bmd));

  SetImageMetadata(*output, std::move(meta));
};

void
otb::ComputeNormalsOnEllipsoidImageSource
::ThreadedGenerateData(
    OutputImageRegionType const& outputRegionForThread,
    itk::ThreadIdType threadId
)
{
  PixelType pixelOutput;
  pixelOutput.Reserve(3);

  otbMsgDevMacro("thr #"<< threadId<<" works on output region: "
                 << NeatRegionLogger{outputRegionForThread});

  using OutputIterator = itk::ImageScanlineIterator< OutputImageType >;

  std::size_t const line_size = outputRegionForThread.GetSize()[0];
  std::vector<double> buffer_sincoslonlats(line_size * 4);

  // ---[ Compute normals for all pixels of the current line
  // https://en.wikipedia.org/wiki/N-vector#Converting_latitude/longitude_to_n-vector
  sincos_view_t sincos_view(buffer_sincoslonlats.data(), narrow{}, line_size);
  auto compute_sincos_on_line = [&](lonlat_view_t lonlat_view, OutputIterator const& /*OutIt*/)
  {
    assert(sincos_view.size() == std::ptrdiff_t(buffer_sincoslonlats.size()));
    ComputeAllSinCos(lonlat_view, sincos_view);
  };

  auto compute_pixel_value = [&](Point2DType /*lonlat*/, std::ptrdiff_t i, OutputIterator OutIt)
  {
    auto const sin_lon = sincos_view(0, i);
    auto const cos_lon = sincos_view(1, i);
    auto const sin_lat = sincos_view(2, i);
    auto const cos_lat = sincos_view(3, i);
    auto const nx = cos_lat * cos_lon;
    auto const ny = cos_lat * sin_lon;
    auto const nz = sin_lat;
    // norm² = nx² + ny² + nz²
    // = (cos φ cos λ)² + (cos φ sin λ)² + (sin φ)²
    // = cos²φ (cos²λ + sin²λ) + sin²φ
    // = cos²φ + sin²φ
    // = 1
    pixelOutput[0] = nx;
    pixelOutput[1] = ny;
    pixelOutput[2] = nz;
#if 0
    if (i == 0 && line % 1000 == 0)
    {
      otbMsgDebugMacro(
          << "lon=" << lonlat_view(0, i)
          << "; lat=" << lonlat_view(1, i)
          << "; nx=" << nx
          << "; ny=" << ny
          << "; nz=" << nz
          << "; ==> " << pixelOutput
      );
    }
#endif
    OutIt.Set(pixelOutput);
  };

  this->DoGenerateTileDataLineWise(
      compute_sincos_on_line,
      compute_pixel_value,
      outputRegionForThread,
      threadId
  );
}
