/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "SAR/otbFindOrbit.h"
#include "otbGeocentricTransform.h"  // otb::Projection
#include "otbMacro.h"
#include "otbLogger.h"

namespace 
{ // Anonymous namespace

double DotProduct(itk::Point<double, 3> const& pt1, itk::Point<double, 3> const& pt2)
{
  // Manual dot product is a bit faster w/ gcc...
  return pt1[0]*pt2[0] + pt1[1]*pt2[1] + pt1[2]*pt2[2];
  // return std::inner_product(pt1.Begin(), pt1.End(), pt2.Begin(), 0.);
}

double dist2(otb::Orbit::PointType a, otb::Orbit::PointType b)
{
  auto const d = a - b;
  return DotProduct(d, d);
}

} // Anonymous namespace

otb::CoordinateTransformation::CoordinateTransformationPtr
otb::make_transform_to_lonlat_for(std::string const& projection_ref)
{
  static auto const& wgs84Srs = OGRSpatialReference::GetWGS84SRS();
  assert(wgs84Srs->GetAxisMappingStrategy() == OAMS_TRADITIONAL_GIS_ORDER);
  OGRSpatialReference inputSrs(projection_ref.c_str());

  if (!inputSrs.IsSame(wgs84Srs))
  {
    return otb::CoordinateTransformation::CoordinateTransformationPtr
    {
      OGRCreateCoordinateTransformation(&inputSrs, wgs84Srs)
    };
  }
  else
  {
    return {};
  }
}


void
otb::FilterOrbit(otb::OrbitInformation & orbit_info, aos_lonlathgt const& corners_llh)
{
  auto & OSVs = orbit_info.orbit;
  if (OSVs.empty()) return;

  constexpr auto max = std::numeric_limits<double>::max();
  std::array<std::size_t, 4> closest_indexes   { 0, 0, 0, 0 };
  std::array<double,      4> closest_distances { max, max, max, max };

  otbLogMacro(Info, << "Corner 0 lon/lat/hgt: " << corners_llh[0]);
  otbLogMacro(Info, << "Corner 1 lon/lat/hgt: " << corners_llh[1]);
  otbLogMacro(Info, << "Corner 2 lon/lat/hgt: " << corners_llh[2]);
  otbLogMacro(Info, << "Corner 3 lon/lat/hgt: " << corners_llh[3]);

  std::array<otb::Orbit::PointType, 4> corners_xyz;
  for (std::size_t i = 0; i < 4; ++i)
  {
    corners_xyz[i] = otb::Projection::WorldToEcef(corners_llh[i]);
  }
  otbLogMacro(Info, << "Corner 0 XYZ: " << corners_xyz[0]);
  otbLogMacro(Info, << "Corner 1 XYZ: " << corners_xyz[1]);
  otbLogMacro(Info, << "Corner 2 XYZ: " << corners_xyz[2]);
  otbLogMacro(Info, << "Corner 3 XYZ: " << corners_xyz[3]);

  // static_assert(boost::size(corners) == boost::size(closest_indexes), "mismatching sizes");

  otbLogMacro(Info, << "Exploring "<<(orbit_info.last_idx-orbit_info.first_idx) << " OSVs #["
              << orbit_info.first_idx << ".." << orbit_info.last_idx << "[.  " "I.e. from "
              << OSVs[orbit_info.first_idx].time << " to " << OSVs[orbit_info.last_idx].time);

  for (std::size_t o = orbit_info.first_idx; o < orbit_info.last_idx; ++o)
  {
    for (auto i = 0u ; i < closest_indexes.size(); ++i)
    {
      auto const d2 = dist2(corners_xyz[i], OSVs[o].position);
      if (d2 < closest_distances[i])
      {
        closest_indexes[i]   = o;
        closest_distances[i] = d2;
      }
    }
  }

  auto const range = std::minmax_element(std::begin(closest_indexes), std::end(closest_indexes));
  auto const idx_first = std::distance(std::begin(closest_indexes), range.first);
  auto const idx_last  = std::distance(std::begin(closest_indexes), range.second);

  auto log_corner_distance = [&](std::size_t id) 
  {
    otbLogMacro(Info,
                << "Corner "<< id <<" has a distance of " << std::sqrt(closest_distances[id])
                << "m to " << OSVs[closest_indexes[id]].position << " @ pos #" << closest_indexes[id]
                << " -> @ " << OSVs[closest_indexes[id]].time);
  };
  log_corner_distance(0);
  log_corner_distance(1);
  log_corner_distance(2);
  log_corner_distance(3);

  auto const offset = 10;  // TODO: improve the time selection
  auto const first = closest_indexes[idx_first] > offset ? closest_indexes[idx_first] - offset : 0;
  auto const last  = std::min<std::size_t>(closest_indexes[idx_last] + offset, OSVs.size());
  OSVs.erase(OSVs.begin() + last, OSVs.end());
  OSVs.erase(OSVs.begin(),        OSVs.begin() + first);

  assert(!OSVs.empty());
  otbLogMacro(Info,
              << "Keeping "<<(last-first) << " OSVs #["
              << first << ".." << last << "[.  " "I.e. from "
              << OSVs.front().time << " to " << OSVs.back().time);
}
