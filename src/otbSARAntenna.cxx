/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "SAR/otbSARAntenna.h"
namespace otb
{

#if defined(DRAFT_OTB8) || OTB_VERSION_MAJOR >= 8
std::unique_ptr<SarSensorModel> make_sensor_model(ImageMetadata const& kwl)
{
  auto res = std::make_unique<SarSensorModel>(kwl);
  return res;
}
#else
SarSensorModelAdapter::Pointer make_sensor_model(ImageKeywordlist const& kwl)
{ // parameter taken by value to permit copy elision & move-semantics
  SarSensorModelAdapter::Pointer res = SarSensorModelAdapter::New();

  if(!res->LoadState(kwl) || !res->IsValidSensorModel())
  {
    itkGenericExceptionMacro(<<"SAR image does not contain a valid SAR sensor model.");
  }

  return res;
}
#endif

} // otb namespace

