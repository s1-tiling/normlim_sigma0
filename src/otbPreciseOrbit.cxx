/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "SAR/otbPreciseOrbit.h"
#include "SAR/otbPreciseOrbitDetails.h"
#include "Common/otbXmlWrapper.h"
#include "Common/otbRaiseMacro.h"
#include "otbMacro.h"

#include <string>

using TimeType = otb::MetaData::TimePoint;

using namespace otb::xml;

namespace
{ // Anonymous namespace

/**
 * Checks the document is a "Precise Orbit Ephemerides (POE) Orbit File" in
 * EARTH_FIXED frame.
 * \param[in] doc  xml document instance
 * \throw std::runtime_error if the document is not as expected
 */
void check_document_prerequisites(Document const& doc)
{
  auto header   = doc.find("Earth_Explorer_File", "Earth_Explorer_Header");
  auto filetype = header.get("Fixed_Header", "File_Type");
  if (filetype != "AUX_POEORB")
  {
    otbRaise(
        "Unexpected File_Type '" << filetype
        <<"' found in " << header.context().context());
  }

  auto frame = header.get("Variable_Header", "Ref_Frame");
  if (frame != "EARTH_FIXED")
  {
    otbRaise(
        "Unexpected reference frame '" << frame
        <<"' found in " << header.context().context());
  }
}

/**
 * {offset, modulo} pairs to convert from absolute to relative orbit.
 * To complete this array for new Sentinel-1 satellites, check the information for a new satellite
 * product, and compute: `({effective_absolute_orbit} - {effective_relative_orbit} + 1) % 175`, this
 * will give the associated offset.
 */
static constexpr otb::details::orbit_conv k_orbit_converters[] = {
  {73, 175}, // S1-A
  {27, 175}, // S1-B
  {99, 175}  // S1-C
};

TimeType time(otb::string_view txt_time)
{
  if (otb::starts_with(txt_time, "UTC="))
  {
    txt_time.remove_prefix(4);
  }
  return otb::to<TimeType>(txt_time, "extracting OSV UTC");
}

} // Anonymous namespace

otb::details::orbit_conv
otb::details::get_orbit_converter(otb::string_view mission)
{
  if (mission == "Sentinel-1A") { return k_orbit_converters[0]; }
  if (mission == "Sentinel-1B") { return k_orbit_converters[1]; }
  if (mission == "Sentinel-1C") { return k_orbit_converters[2]; }
  // if (mission == "Sentinel-1D") { return k_orbit_converters[3]; }
  otbRaise(
      "Mission " << mission << " isn't supported."
      " Only Sentinel-1A, 1-B and 1-C are supported at the moment.");
}

std::vector<otb::Orbit>
otb::read_precise_orbit(std::string const& orbit_filename)
{
  Document doc(orbit_filename);
  check_document_prerequisites(doc);

  auto osv_list = doc.find(
      "Earth_Explorer_File", "Data_Block", "List_of_OSVs");

  using PointType = Orbit::PointType;
  std::vector<otb::Orbit> res;
  for (auto && osv : osv_list)
  {
    double pos[3] = {osv.get<double>("X"),  osv.get<double>("Y"),  osv.get<double>("Z")};
    double vel[3] = {osv.get<double>("VX"), osv.get<double>("VY"), osv.get<double>("VZ")};
#if __cplusplus >= 202002L
    // C++ permits aggregate construction from emplace_back
    res.emplace_back(time(osv.get("UTC")), PointType{pos}, PointType{vel});
#else
    res.emplace_back(
        otb::Orbit{time(osv.get("UTC")), PointType{pos}, PointType{vel}}
    );
#endif
  }

  return res;
}

otb::OrbitInformation
otb::read_precise_orbit(
    std::string const& orbit_filename,
    int                relative_orbit_number,
    int                extra)
{
  Document doc(orbit_filename);
  check_document_prerequisites(doc);

  auto const mission = doc.get(
      "Earth_Explorer_File", "Earth_Explorer_Header", "Fixed_Header", "Mission");
  auto const orbit_converter = details::get_orbit_converter(mission);

  auto osv_list = doc.find(
      "Earth_Explorer_File", "Data_Block", "List_of_OSVs");

  int const first_absolute_orbit_number = osv_list.get<int>(
      "OSV", "Absolute_Orbit");

  int const absolute_orbit_number = orbit_converter.closest_absolute(
      first_absolute_orbit_number, relative_orbit_number);
  if (first_absolute_orbit_number == absolute_orbit_number)
  {
    otbLogMacro(Warning,
                << "Orbit +" << absolute_orbit_number << " that matches relative orbit "
                << relative_orbit_number << " is the first found in '" << doc.context().filename()
                << "'."
                << "\nThis precise orbit file may not be the best to extract satellite positions."
                << "\nPrevious file may provide better a better cover of requested relative orbit."
    );
  }

  otbLogMacro(Info,
      << "Closest absolute orbit number associated to requested relative orbit "
      << relative_orbit_number << " --> " << absolute_orbit_number);

  using PointType = Orbit::PointType;
  std::vector<otb::Orbit> res;
  std::size_t first_orb_idx = 0;
  std::size_t last_orb_idx  = 0;
  for (auto && osv : osv_list)
  {
    int const abs_orb = osv.get<int>("Absolute_Orbit");
    // expect OSVs to be sorted by absolute orbit numbers

    // keep more in case we are around z==0
    if (abs_orb < absolute_orbit_number - extra) {
      continue; // not yet the right orbit
    } else if (abs_orb > absolute_orbit_number + extra) {
      break;   // everything has been seen
    } else if (abs_orb < absolute_orbit_number) {
      ++first_orb_idx;
      ++last_orb_idx;
    } else if (abs_orb == absolute_orbit_number) {
      ++last_orb_idx;
    }


    double pos[3] = {osv.get<double>("X"),  osv.get<double>("Y"),  osv.get<double>("Z")};
    double vel[3] = {osv.get<double>("VX"), osv.get<double>("VY"), osv.get<double>("VZ")};
#if __cplusplus >= 202002L
    // C++ permits aggregate construction from emplace_back
    res.emplace_back(time(osv.get("UTC")), PointType{pos}, PointType{vel});
#else
    res.emplace_back(
        otb::Orbit{time(osv.get("UTC")), PointType{pos}, PointType{vel}}
    );
#endif
  }
  if (res.empty())
  {
    otbRaise(
        "Orbit +" << absolute_orbit_number << " that matches relative orbit "
        << relative_orbit_number << " is not found in '"
        << doc.context().filename() << "'");
  }

  return OrbitInformation{
    res,
      first_orb_idx,
      last_orb_idx,
      relative_orbit_number
  };
}
