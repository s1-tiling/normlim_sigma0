# NORMLIM_sigma0


### Table of contents

* [Purpose](#purpose)
* [Workflow](#workflow)
    * [Example](#example)
* [Installation](#installation)
    * [Requirements](#requirements)
    * [Steps](#steps)
        * [0. Make sure OTB is properly loaded on your system](#-make-sure-otb-is-properly-loaded-on-your-system)
        * [1. Clone the repository](#1-clone-the-repository)
        * [2. Build it in a separate directory (highly recommended)](#2-build-it-in-a-separate-directory-highly-recommended)
        * [3. Special case, HAL users](#3-special-case-hal-users)


## Purpose

Implement OTB applications for computing NORMLIM/sigma 0 calibration (Working project. Will be eventually merged into OTB)

This application will perform a radiometric correction on SAR images to reduce the effect of topography.

The NORMLIM correction normalize the sigma0 value of the pixel with the Local Incidence angle.
NORMLIM means NORMalization by Local Incidence Mask [Small D. "Flattening Gamma: Radiometric Terrain Correction for SAR Imagery"](https://www.researchgate.net/deref/http%3A%2F%2Fdx.doi.org%2F10.1109%2FTGRS.2011.2120616)

```math
\sigma^0_{NORMLIM}=\sigma^0\sin(\theta_{LIM})/\sin(\theta_E)
```
where:

$`\theta_{LIM}`$ is the Local Incidence Angle

$`\theta_{E}`$ is the Incidence Angle on ellipsoid

$`\sigma^0`$ is the calibrated pixel value

The application shall have the following parameters:

- Inputs:
   - SAR images (calibrated in $`\sigma^0`$)
   - DEM directory

- Outputs:
   - SAR images with radiometric correction (calibrated in $`\sigma^0_{NORMLIM}`$)
   - Local Incidence Angle image ($`\theta_{LIM}`$)
   - Incidence Angle image ($`\theta_E`$)

## Workflow

The expected workflow to apply $`\sigma^0_{NORMLIM}`$ calibration on SAR images
is the following.

Given a S1 tile geometry -- it could be any SAR geometry.

1. Build a VRT of the DEM files that completely overlap the S1 tile
2. Generate an image made of the XYZ cartesian coordinates of Earth surface in
   the geometry of the S1 tile from step 1. result

   Use either:

   - `DEMProjection` application defined here (quite slow with OSSIM kernel
     used in OTB 7)
   - or DiapOTB `SARDEMProjection` + `SARCartesianMeanEstimation` applications

3. Generate an image of surface normal vectors $`\overrightarrow{n}`$ from the
   result of step 2. (in the geometry of S1 image)

   Use `ExtractNormalVector` application defined here.

4. Generate the associated $`\theta_{LIM}`$ and $`\sin(\theta_{LIM})`$ from the
   scalar product, or the cross product, between $`\overrightarrow{n}`$ and
   $`\overrightarrow{ST}`$ (S: sensor, T: target on Earth surface)

   The sensor trajectory is always the same in the case of S1 tiles.

5. `SARCalibration` application already produces $`\sigma^0_E`$ calibrated SAR
   images.
   $`\sigma^0_{NORLIM}`$ calibration can be obtained by multiplying each
   $`\sigma^0_E`$ calibrated pixel by the associated $`\sin(\theta_{LIM})`$.

   Use `BandMath` `*` operation.

Note:

- In Sentinel-1 case, the steps 1. - 4. will produce the same values for each
  product on the same S1 tile: we can compute the result once and cache it.
- Step 5. needs to be executed on each SAR image.

### Example

```bash
GEOD_FILE=....../Geoid/egm96.grd
SRTM_DIR=....../MNT/SRTM_30_hgt
SRTM_SHP=....../shapefile/srtm.shp
INSAR=path/to/s1a-iw-grd-vh-20200108t044150-20200108t044215-030704-038506-002.tiff

# 1. --> 5.
OTB_GEOID_FILE="${GEOD_FILE}" python3 from_s1_to_LIA.py "${INSAR}" -v --srtm-dir "${SRTM_DIR}" -o chain-reldeb --srtm-shp ${SRTM_SHP}  --ram 10000 --geoid "${GEOD_FILE}"

# If we prefer to use an already existing VRT DEM, we can use the following instead of: --srtm-shp ${SRTM_SHP}
OTB_GEOID_FILE="${GEOD_FILE}" python3 from_s1_to_LIA.py "${INSAR}" -v --srtm-dir "${SRTM_DIR}" -o chain-reldeb --ram 10000 --geoid "${GEOD_FILE}" --from DEM --srtm-file "${PATH_TO_DEM_VRT}"

## ----------------[ OR ] --------------

# 2.a. Generate the projected DEM (DiapOTB applications)
OTB_GEOID_FILE="${GEOD_FILE} "otbcli_SARDEMProjection -indem dem.vrt -insar "${INSAR}" -withxyz on -out demsprj-onto-S1-030704.tiff

# Beware the -indirectiondemc/l parameter values are printed in stdout by the
# previous SARDEMProjection application
otbcli_SARCartesianMeanEstimation -indem dems.vrt -insar "${INSAR}"  -indemproj demsprj-onto-S1-030704.tiff -mlran 1 -mlazi 1 -indirectiondemc -1 -indirectiondeml --1 -out dems-onto-S1-030704.tiff

# 2.b. OR Generate the projected DEM
ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=1 OTB_GEOID_FILE="${GEOD_FILE}" otbcli_DEMProjection -ref "${INSAR}" -out dems-onto-S1-030704.tiff -elev.dem "${SRTM_DIR}" -elev.geoid "${GEOD_FILE}"

# 3. Generate the ground normals
otbcli_ExtractNormalVector -xyz dems-onto-S1-030704.tiff -out normals-onto-S1-030704.tiff

# 4. Generate the Local Incidence Angle Map
otbcli_SARComputeIncidenceAngle -in.xyz dems-onto-S1-030704.tiff -in.normals normals-onto-S1-030704.tiff -out "lia-onto-S1-030704.tiff?&gdal:co:COMPRESS=DEFLATE"
```

## Installation

### Requirements

- you need OTB 7.4 (exactly!), g++ + libstdc++ (version 6+; the later the
  better), CMake
- you need to choose:
    - a directory where the software source files will be __cloned__
    - a directory where the software will be __build__
    - a directory where the software will be __installed__

    The first two directories can be discarded once the installation is done

### Steps

#### 0. Make sure OTB is properly loaded on your system

On HAL, it could be done with:

```bash
module load otb/7.4-python3.8.4
# or if you prefer...
module load otb/7.4-python3.7.2

# and don't forget CMake!
module load cmake
```

If you're using OTB precompiled binaries, you'll have to load the
environment with:

```bash
export OTB_INSTALL_DIRNAME="/some/path/you/have.chosen"
source "${OTB_INSTALL_DIRNAME}/otbenv.profile"
# You'll also have to make sure CMake and g++ are available!
```

**Beware** In that case CMake configuration needs to be done with
`-DCMAKE_CXX_FLAGS=-D_GLIBCXX_USE_CXX11_ABI=0`

#### 1. Clone the repository

```bash
cd /where/i/want/to/work
# Clone with https (should be enough for most)
git clone https://gitlab.orfeo-toolbox.org/s1-tiling/normlim_sigma0.git

# Or, clone with ssh (preferred method for maintainers)
git clone git@gitlab.orfeo-toolbox.org:s1-tiling/normlim_sigma0.git
```

#### 2. Build it in a separate directory (highly recommended)

Let's use:

- `_buildir/` in the source tree.
- `/where/i/want/to/install/SARCalibrationExtended`

    A quick and __dirty__ _hack_ would be to use `"${OTB_INSTALL_DIRNAME}"` if
    OTB comes from a Linux binaries release. This way, no need to update
    environment variables to find the SARCalibrationExtended applications.

```bash
# Configure: to be done once!
cmake -S . -B _builddir -DOTB_BUILD_MODULE_AS_STANDALONE=ON \
      -DCMAKE_INSTALL_PREFIX=/where/i/want/to/install/SARCalibrationExtended \
      -DCMAKE_BUILD_TYPE=Release

# WARNING: Add "-DCMAKE_CXX_FLAGS=-D_GLIBCXX_USE_CXX11_ABI=0" if you're using
#          OTB precompiled binaries

# Compile:
cmake --build _builddir -j 4

# Install
# - with CMake 3.15+
cmake --install _builddir
# - with CMake 3.14-
cmake --build _builddir --target install
```

__Important__: the product shall not be installed in the build directory.
Please use a different directory. Eventually, the build directory can be
completely removed with:

```bash
rm -r /where/i/want/to/work/normlim_sigma0
```

#### 3. Special case, HAL users

If you're working on HAL cluster, a
[Lmod modulefile](https://lmod.readthedocs.io/en/latest/) compatible with
HAL is automatically generated. It will also automagically declare its
dependence to the OTB module used to compile the project.

You will have to update your `${MODULEPATH}` environment variable to also
list the chosen installation directory. For instance:

```bash
# The following line makes sure to prepend ${MODULEPATH} with the directory
# where the modulefile has been installed, or just to set ${MODULEPATH} if it
# was empty
export MODULEPATH="/where/i/want/to/install/SARCalibrationExtended/modulefile:${MODULEPATH:+:$MODULEPATH}"
```

Then to use the new module, just load it with

```bash
module load SARCalibrationExtended
```

Then, the following should execute without crashing

```bash
otbcli_ExtractNormalVector -h
```
