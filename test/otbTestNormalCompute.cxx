/*
 * Copyright (C) 2005-2021 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Filters/otbNormalCompute.h"
#define BOOST_TEST_MODULE "otb::SimpleNormalMethod unit testing"
#define BOOST_TEST_DYN_LINK
#if defined(__GNUC__) || defined(__clang__)
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wunused-parameter"
#   pragma GCC diagnostic ignored "-Woverloaded-virtual"
#   include "Tests/otbBoostTestOnTuples.h"
#   include <boost/test/included/unit_test.hpp>
#   pragma GCC diagnostic pop
#else
#   include "Tests/otbBoostTestOnTuples.h"
#   include <boost/test/included/unit_test.hpp>
#   include <ostream>
#endif

namespace std
{
ostream & operator<<(ostream & os, const std::tuple<double, double, double> & v)
{
  return os
    << '{' << std::get<0>(v)
    << ", "<< std::get<1>(v)
    << ", "<< std::get<2>(v)
    << '}';
}
} // std namespace

namespace
{ // Anonymous namespace
auto above = [](auto v){ return otb::Above<decltype(v)>(v); };
auto below = [](auto v){ return otb::Below<decltype(v)>(v); };
auto left  = [](auto v){ return otb::Left <decltype(v)>(v); };
auto right = [](auto v){ return otb::Right<decltype(v)>(v); };
} // Anonymous namespace

BOOST_AUTO_TEST_CASE(ComputeNormal_TrivialCases)
{
  constexpr double x_spacing = 1.;
  constexpr double y_spacing = 1.;
  const auto norm = otb::SimpleNormalMethodForRegularGrid(x_spacing, y_spacing);

  auto const _1_per_sq2 = 1. / std::sqrt(2.);
  auto const _1_per_sq3 = 1. / std::sqrt(3.);

  // δz/δx = 0, δz/δy = 0
  auto const _0_0_0_0 = std::make_tuple(0., 0., 1.);
  BOOST_CHECK_EQUAL(_0_0_0_0, norm(above(10.), left(10.), right(10.), below(10.)));

  // δz/δx = 0, δz/δy = ±1
  auto const _1_0_0_m1 = std::make_tuple(0., -_1_per_sq2, _1_per_sq2);
  BOOST_CHECK_EQUAL(_1_0_0_m1, norm(above(11.), left(10.), right(10.), below(9.)));

  auto const _m1_0_0_1 = std::make_tuple(0., +_1_per_sq2, _1_per_sq2);
  BOOST_CHECK_EQUAL(_m1_0_0_1, norm(above(9.), left(10.), right(10.), below(11.)));

  // δz/δx = ±1, δz/δy = 0
  auto const _0_1_m1_0 = std::make_tuple(_1_per_sq2, 0., _1_per_sq2);
  BOOST_CHECK_EQUAL(_0_1_m1_0, norm(above(10.), left(11.), right(9.), below(10.)));

  auto const _0_m1_1_0 = std::make_tuple(-_1_per_sq2, 0., _1_per_sq2);
  BOOST_CHECK_EQUAL(_0_m1_1_0, norm(above(10.), left(9.), right(11.), below(10.)));

  // δz/δx = ±1, δz/δy = ±1
  auto const _m1_m1_1_1 = std::make_tuple(-_1_per_sq3, _1_per_sq3, _1_per_sq3);
  BOOST_CHECK_EQUAL(_m1_m1_1_1, norm(above(9.), left(9.), right(11.), below(11.)));

  auto const _1_m1_1_m1 = std::make_tuple(-_1_per_sq3, -_1_per_sq3, _1_per_sq3);
  BOOST_CHECK_EQUAL(_1_m1_1_m1, norm(above(11.), left(9.), right(11.), below(9.)));

  auto const _m1_1_m1_1 = std::make_tuple(+_1_per_sq3, _1_per_sq3, _1_per_sq3);
  BOOST_CHECK_EQUAL(_m1_1_m1_1, norm(above(9.), left(11.), right(9.), below(11.)));

  auto const _1_1_m1_m1 = std::make_tuple(+_1_per_sq3, -_1_per_sq3, _1_per_sq3);
  BOOST_CHECK_EQUAL(_1_1_m1_m1, norm(above(11.), left(11.), right(9.), below(9.)));
}

BOOST_AUTO_TEST_CASE(ComputeNormal_from_real_file, *boost::unit_test::tolerance(0.00001))
{
  constexpr double x_spacing =   0.00027777777777777777753684396167;
  constexpr double y_spacing = - 0.00027777777777777777753684396167;
  const auto norm = otb::SimpleNormalMethodForRegularGrid(x_spacing, -y_spacing);

  auto const _expected1 = std::make_tuple(-0.60000002384185791015625, 0.800000011920928955078125, +0.000111111119622364640235900878906);
  BOOST_TEST(_expected1 == norm(above(398.), left(398.), right(401.), below(402.)), boost::test_tools::per_element());

  auto const _expected2 = std::make_tuple(0.4472135603427886962890625, 0.894427120685577392578125, 0.000248451979132369160652160644531);
  BOOST_TEST(_expected2 == norm(above(518.), left(519.), right(518.), below(520.)), boost::test_tools::per_element());
}
