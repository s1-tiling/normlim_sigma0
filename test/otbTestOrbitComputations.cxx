/*
 * Copyright (C) 2005-2025 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "boost/test/tools/old/interface.hpp"

#include "SAR/otbPreciseOrbitDetails.h"

#define BOOST_TEST_MODULE "otb:: orbit computation unit testing"
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(TestOrbitConversions)
{

    auto s1a_converter = otb::details::get_orbit_converter("Sentinel-1A");

    BOOST_REQUIRE(s1a_converter.to_relative(30632) == 110);
    BOOST_REQUIRE(s1a_converter.to_relative(30704) == 7);
    BOOST_REQUIRE(s1a_converter.to_relative(51107) == 110);

    BOOST_REQUIRE(s1a_converter.closest_absolute(30632, 109) == 30806);
    BOOST_REQUIRE(s1a_converter.closest_absolute(30632, 110) == 30632);
    BOOST_REQUIRE(s1a_converter.closest_absolute(30704,   7) == 30704);
    BOOST_REQUIRE(s1a_converter.closest_absolute(51107, 110) == 51107);

    BOOST_REQUIRE(s1a_converter.closest_absolute(30631, 110) == 30632);
    BOOST_REQUIRE(s1a_converter.closest_absolute(30806, 110) == 30807);
    BOOST_REQUIRE(s1a_converter.closest_absolute(30633, 110) == 30632 + 175);
}
