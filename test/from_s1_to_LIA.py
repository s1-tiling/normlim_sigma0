#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =========================================================================
#   Copyright 2017-2021 (c) CESBIO. All rights reserved.
#
#   This file is part of S1Tiling project
#       https://gitlab.orfeo-toolbox.org/s1-tiling/s1tiling
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# =========================================================================
#
# Authors: Thierry KOLECK (CNES)
#          Luc HERMITTE (CS Group)
#
# =========================================================================


import os, sys, re
import copy
import logging
from pathlib import Path
import itertools
from timeit import default_timer as timer
import subprocess
from types import SimpleNamespace
import click
import osgeo  # To test __version__
from osgeo import ogr, osr, gdal
import otbApplication as otb

### Globals & Constants {{{1
class colors:
    reset      = '\033[0m'
    bold       = '\033[1m'
    underline  = '\033[4m'
    black      = '\033[30m'
    red        = '\033[31m'
    green      = '\033[32m'
    orange     = '\033[33m'
    blue       = '\033[34m'
    magenta    = '\033[35m'
    cyan       = '\033[36m'
    white      = '\033[37m'
    grey       = '\033[90m'
    lightred   = '\033[91m'
    lightgreen = '\033[92m'
    yellow     = '\033[93m'
    violet     = '\033[94m'
    pink       = '\033[95m'
    lightcyan  = '\033[96m'

    @staticmethod
    def file(s):
        return colors.green + s + colors.reset
    @staticmethod
    def YES():
        return colors.green + 'YES' + colors.reset
    @staticmethod
    def OK():
        return colors.green + 'OK' + colors.reset
    @staticmethod
    def NO():
        return colors.red + 'NO ' + colors.reset

### Context manager to measure execution times {{{1
class ExecutionTimer(object):
    """Context manager to help measure execution times

    Example:
    with ExecutionTimer("the code", True) as t:
        Code_to_measure()
    """
    def __init__(self, text, do_measure):
        self._text       = text
        self._do_measure = do_measure
    def __enter__(self):
        self._start = timer()
        return self
    def __exit__(self, type, value, traceback):
        if self._do_measure:
            end = timer()
            logging.info("%s took %ssec", self._text, end-self._start)
        return False

### Pipelines {{{1
class Pipelines(object):
    def __init__(self, do_measure):
        self._pipelines = []
        self.do_measure = do_measure

    def push(self, name, app):
        if not app:
            raise("Impossible to register %s" % (name))
        crt = {'name': name, 'app': app}
        self._pipelines += [crt]

    def execute(self):
        if not self._pipelines:
            return
        names = (n['name'] for n in self._pipelines)
        message = 'Execution of %s' % (' | '.join(names))
        logging.info('## '+message)
        with ExecutionTimer('-> '+message, self.do_measure) as t:
            assert(self._pipelines[-1]['app'])
            self._pipelines[-1]['app'].ExecuteAndWriteOutput()
        # And reset the pipeline
        for step in self._pipelines:
            del(step['app']) # Make sure to release application memory
        self._pipelines = []

    def last_application(self):
        # If any, return the last one, return None otherwise
        return self._pipelines and self._pipelines[-1]['app']

### Actions {{{1
def create_app(appname, params, extra_params=None):
    app = otb.Registry.CreateApplication(appname)
    if not app:
        raise click.UsageError("Cannot instanciate %s. Make sure it's available in $OTB_APPLICATION_PATH=%s" % (appname, os.getenv('OTB_APPLICATION_PATH', '')))
    app.SetParameters(params)
    msg_params = copy.deepcopy(params)
    if extra_params:
        msg_params.update(extra_params)
    message = 'otbcli_%s %s' % (appname, ' '.join('-%s %s' % (k, as_app_shell_param(v)) for k, v in msg_params.items()))
    logging.info('$> %s', message)
    return app


def do_execute_demProjection(insar, indem, withxyz, outPath, nodata=-32768, ram="4000", dryrun=False):
    """
        Register SARDEMProjection application.

        This application puts a DEM file into SAR geometry and estimates two additional coordinates.
        In all for each point of the DEM input four components are calculated :
        C (colunm into SAR image), L (line into SAR image), Z and Y.

        :param insar: SAR Image to extract SAR geometry.
        :param indem: DEM to perform projection on.
        :param withxyz: Set XYZ Cartesian components into projection.
        :param nodata: No Data values for the DEM.
        :param outPath: Output Vector Image with at least four components (C,L,Z,Y).

        :type insar: string
        :type indem: string
        :type withxyz: bool
        :type nodata: int
        :type outPath: string

        :return: Range/Azimut direction for DEM scan and Gain value
        :rtype: int, int, float
    """
    params = {
            'insar': insar,
            'indem': indem,
            'withxyz': withxyz,
            'nodata': nodata,
            'out': outPath,
            # 'ram:': ram  # Cannot be passed through SetParameters...
            }
    app = create_app('SARDEMProjection', params, {'ram': ram})
    app.SetParameterString("ram", ram)
    if not dryrun:
        with ExecutionTimer('-> SARDEMProjection', True) as t:
            app.ExecuteAndWriteOutput()

    return app.GetParameterInt('directiontoscandemc'), app.GetParameterInt('directiontoscandeml'), app.GetParameterFloat('gain')


def do_register_cartesianMeanEstimation(insar, indem, indemproj, indirectiondemc, indirectiondeml, outPath, ram="4000"):
    """
        Launch SARCartesianMeanEstimation application (from DiapOTB).

        This application estimates a simulated cartesian mean image thanks to a DEM file.

        :param insar: SAR Image to extract SAR geometry.
        :param indem: DEM to extract DEM geometry.
        :param indemproj: Input vector image for cartesian mean estimation.
        :param indirectiondemc: Range direction for DEM scan
        :param indirectiondeml: Azimut direction for DEM scan
        :param outPath: Output cartesian (mean) Image for DEM Projection.

        :type insar: string
        :type indem: string
        :type indemproj: string
        :type indirectiondemc: int
        :type indirectiondeml: int
        :type outPath: string

        :return: None
    """
    assert(indem)
    assert(insar)
    assert(indemproj)
    assert(outPath)
    assert(indirectiondemc is not None)
    assert(indirectiondeml is not None)
    params = {
            "insar": insar,
            "indem": indem,
            "indemproj":  indemproj,
            "indirectiondemc": indirectiondemc,
            "indirectiondeml": indirectiondeml,
            "mlran": 1,
            "mlazi": 1,
            "out":  outPath,
            # 'ram:': ram  # Cannot be passed through SetParameters...
            }
    app = create_app('SARCartesianMeanEstimation', params, {'ram': ram})
    app.SetParameterString("ram", ram)
    return app


def do_register_computeNormals(inxyz, outPath, in_memory, ram="4000"):
    """
        Register ExtractNormalVector application.

        This application computes surface normals.

        :param inxyz: Cartesian coordinates of surface in SAR geometry.
        :param outPath: Output Normal map of surface

        :return: None
    """
    appname = "ExtractNormalVector"
    params = {
            "out":  outPath,
            # 'ram:': ram  # Cannot be passed through SetParameters...
            }
    if type(inxyz) is str:
        params['xyz'] = inxyz
        app = create_app(appname, params, {'ram': ram})
    else:
        logging.debug('Connect %s[%s] with %s', appname, 'xyz', inxyz)
        app = create_app(appname, params, {'ram': ram, 'xyz': 'PIPE_XYZ'})
        app.ConnectImage('xyz', inxyz, 'out')
        app.PropagateConnectMode(in_memory)
    app.SetParameterString("ram", ram)
    return app


def do_register_computeLIA(inxyz, innormals, inxyz_filename, innormals_filename, out_lia, out_sin, in_memory, ram="4000"):
    """
        Register SARComputeLocalIncidenceAngle application.

        This application computes local incidence angle map.

        :param inxyz: Cartesian coordinates of surface in SAR geometry.
        :param innormals: Normal map of surface
        :param out_lia: Name of the file that'll contain LIA map
        :param out_sin: Name of the file that'll contain sin(LIA) map

        :return: None
    """
    appname = "SARComputeLocalIncidenceAngle"
    app = otb.Registry.CreateApplication(appname)
    if not app:
        raise click.UsageError("Cannot instanciate %s. Make sure it's available in $OTB_APPLICATION_PATH=%s" % (appname, os.getenv('OTB_APPLICATION_PATH', '')))
    params = {
            "out.lia":  out_lia,
            "out.sin":  out_sin,
            # 'ram:': ram  # Cannot be passed through SetParameters...
            }
    if type(inxyz) is str:
        params['in.xyz'] = inxyz
    if type(innormals) is str:
        params['in.normals'] = innormals
    app = create_app(appname, params, {'ram': ram, 'in.xyz': inxyz_filename, 'in.normals': innormals_filename})
    if type(inxyz) is not str:
        logging.debug('Connect %s[%s] with %s', appname, 'in.xyz', inxyz)
        app.ConnectImage('in.xyz', inxyz, 'out')
    if type(innormals) is not str:
        logging.debug('Connect %s[%s] with %s', appname, 'in.normals', innormals)
        app.ConnectImage('in.normals', innormals, 'out')
    app.SetParameterString("ram", ram)
    app.PropagateConnectMode(in_memory)

    return app


def do_execute_check_height(xyz, outPath, ram, dryrun):
    appname = "BandMath"
    params = {
            'il': [xyz],
            'out': outPath,
            'exp': 'sqrt(im1b1*im1b1+im1b2*im1b2+im1b3*im1b3)'
            }
    app = create_app(appname, params, {'ram': ram})
    if app and not dryrun:
        with ExecutionTimer('-> XYZ --> HGT', True) as t:
            app.ExecuteAndWriteOutput()
    return None


def do_execute_orthorectify(in_s1_filename, out_s2_filename,
        s2_tilename, s2_tile_origin, srtm_file, geoid_file,
        in_memory, dryrun, ram="4000"):
    """
        Register Orthorectification application.

        This application will project image in S1 geometry onto S2 grid

        :return: None
    """
    appname = "OrthoRectification"
    spacing           = 10.  # in meters
    grid_spacing      = 40.  # x4 spacing
    out_utm_zone      = s2_tilename[0:2]
    out_utm_northern  = s2_tilename[2] >= 'N'
    in_epsg             = 4326
    out_epsg            = 32600 + int(out_utm_zone)
    if not out_utm_northern:
        out_epsg += 100
    x_coord, y_coord, _ = convert_coord([s2_tile_origin[0]], in_epsg, out_epsg)[0]
    lrx, lry, _         = convert_coord([s2_tile_origin[2]], in_epsg, out_epsg)[0]

    if not out_utm_northern and y_coord < 0:
        y_coord += 10000000.
        lry     += 10000000.
    params = {
            'opt.ram'          : ram,
            'io.in'            : in_s1_filename,
            "io.out"           :  out_s2_filename,
            'interpolator'     : 'nn',
            'outputs.spacingx' : spacing,
            'outputs.spacingy' : -spacing,
            'outputs.sizex'    : int(round(abs(lrx - x_coord) / spacing)),
            'outputs.sizey'    : int(round(abs(lry - y_coord) / spacing)),
            'opt.gridspacing'  : grid_spacing,
            'map'              : 'utm',
            'map.utm.zone'     : int(out_utm_zone),
            'map.utm.northhem' : out_utm_northern,
            'outputs.ulx'      : x_coord,
            'outputs.uly'      : y_coord,
            'elev.dem'         : srtm_file,
            'elev.geoid'       : geoid_file
            }
    app = create_app(appname, params)

    if app and not dryrun:
        with ExecutionTimer('-> %s %s' % (appname, in_s1_filename), True) as t:
            app.ExecuteAndWriteOutput()
    return None


############################################################################
### Helper functions {{{1
def convert_coord(tuple_list, in_epsg, out_epsg):
    """
    Convert a list of coordinates from one epsg code to another

    Args:
      tuple_list: a list of tuples representing the coordinates
      in_epsg: the input epsg code
      out_epsg: the output epsg code

    Returns:
      a list of tuples representing the converted coordinates
    """
    tuple_out = []

    if tuple_list:
        in_spatial_ref = osr.SpatialReference()
        in_spatial_ref.ImportFromEPSG(in_epsg)
        out_spatial_ref = osr.SpatialReference()
        out_spatial_ref.ImportFromEPSG(out_epsg)
        if int(osgeo.__version__[0]) >= 3:
            # GDAL 2.0 and GDAL 3.0 don't take the CoordinateTransformation() parameters
            # in the same order: https://github.com/OSGeo/gdal/issues/1546
            #
            # GDAL 3 changes axis order: https://github.com/OSGeo/gdal/issues/1546
            in_spatial_ref.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
            # out_spatial_ref.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)

    for in_coord in tuple_list:
        lon = in_coord[0]
        lat = in_coord[1]

        coord_trans = osr.CoordinateTransformation(in_spatial_ref, out_spatial_ref)
        coord = coord_trans.TransformPoint(lon, lat)
        # logging.debug("convert_coord(lon=%s, lat=%s): %s, %s ==> %s", in_epsg, out_epsg, lon, lat, coord)
        tuple_out.append(coord)
    return tuple_out


def as_app_shell_param(param):
    """
    Internal function used to stringigy value to appear like a a parameter for a program
    launched through shell.

    foo     -> 'foo'
    42      -> 42
    [a, 42] -> 'a' 42
    """
    if   isinstance(param, list):
        return ' '.join(as_app_shell_param(e) for e in param)
    elif isinstance(param, int):
        return param
    else:
        return "'%s'" % (param,)


class Layer:
    """
    Thin wrapper that requests GDL Layers and keep a living reference to intermediary objects.
    """
    def __init__(self, grid):
        self.__grid        = grid
        self.__driver      = ogr.GetDriverByName("ESRI Shapefile")
        self.__data_source = self.__driver.Open(self.__grid, 0)
        self.__layer       = self.__data_source.GetLayer()

    def __iter__(self):
        return self.__layer.__iter__()

    def reset_reading(self):
        """
        Reset feature reading to start on the first feature.

        This affects iteration.
        """
        return self.__layer.ResetReading()

    def find_tile_named(self, tile_name_field):
        """
        Search for a tile that maches the name.
        """
        for tile in self.__layer:
            if tile.GetField('NAME') in tile_name_field:
                return tile
        return None


def get_origin(manifest):
    """Parse the coordinate of the origin in the manifest file to return its footprint.

    Args:
      manifest: The manifest from which to parse the coordinates of the origin

    Returns:
      the parsed coordinates (or throw an exception if they could not be parsed)
    """
    with open(manifest, "r") as save_file:
        for line in save_file:
            if "<gml:coordinates>" in line:
                coor = line.replace("                <gml:coordinates>", "")\
                           .replace("</gml:coordinates>", "").split(" ")
                coord = [(float(val.replace("\n", "").split(",")[0]),
                          float(val.replace("\n", "")
                                .split(",")[1]))for val in coor]
                return coord[0], coord[1], coord[2], coord[3]
        raise Exception("Coordinates not found in " + str(manifest))


def get_shape(manifest):
    """
    Returns the shape of the footprint of the S1 product.
    """
    nw_coord, ne_coord, se_coord, sw_coord = get_origin(manifest)

    poly = ogr.Geometry(ogr.wkbPolygon)
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(nw_coord[1], nw_coord[0], 0)
    ring.AddPoint(ne_coord[1], ne_coord[0], 0)
    ring.AddPoint(se_coord[1], se_coord[0], 0)
    ring.AddPoint(sw_coord[1], sw_coord[0], 0)
    ring.AddPoint(nw_coord[1], nw_coord[0], 0)
    poly.AddGeometry(ring)
    return poly


def get_s1image_poly(s1image):
    manifest = Path(s1image).parents[1] / 'manifest.safe'
    logging.debug("Manifest: %s", manifest)
    assert(manifest.exists())
    poly = get_shape(manifest)
    return poly


def check_SRTM_overlaps_s1image(s1image, srtm_file):
    poly = get_s1image_poly(s1image)
    # TODO: how can we extract shape of SRTM???
    srtm_layer = Layer(srtm_file)
    footprint = srtm_layer.GetGeometryRef()
    intersection = poly.Intersection(footprint)
    logging.debug("S1 area: %s, ∩ area /w srtm: %s", poly.GetArea(), intersection.GetArea())
    return poly.GetArea() == intersection.GetArea()


def find_SRTM_intersection(s1image, srtm_shapefile):
    # srtm_layer = Layer(srtm_shapefile)
    needed_srtm_tiles = {}
    poly = get_s1image_poly(s1image)
    logging.info("Shape of %s: %s", os.path.basename(s1image), poly)

    assert(srtm_shapefile)
    assert(os.path.isfile(srtm_shapefile))
    srtm_layer = Layer(srtm_shapefile)
    for tile in srtm_layer:
        tile_footprint = tile.GetGeometryRef()
        intersection = poly.Intersection(tile_footprint)
        if intersection.GetArea() > 0.0:
            # logging.debug("Tile: %s", tile)
            file = tile.GetField('FILE')
            needed_srtm_tiles[file] = 1
    return sorted(needed_srtm_tiles.keys())


def get_origin_of_s1image_intersection_with_s2(s1image, s2_tilename, s2grid):
    logging.debug('Test intersection of %s', s2_tilename)
    layer = Layer(s2grid)
    tile = layer.find_tile_named(s2_tilename)
    if not tile:
        logging.info("Tile %s does not exist", s2_tilename)
        return None
    tile_footprint = tile.GetGeometryRef()

    poly = get_s1image_poly(s1image)
    intersection = poly.Intersection(tile_footprint)
    if intersection.GetArea() > 0:
        area_polygon = tile_footprint.GetGeometryRef(0)
        points = area_polygon.GetPoints()
        return [(point[0], point[1]) for point in points[:-1]]
    else:
        return None


def execute(params, dryrun):
    msg = ' '.join([str(p) for p in params])
    logging.info('$> '+msg)
    if not dryrun:
        with ExecutionTimer(msg, True) as t:
            subprocess.run(args=params, check=True)


############################################################################
### Main program {{{1
def build_VRT(options):
    # 2- Build TMP VRT
    if not options.srtm_dir:
        raise click.BadOptionUsage('--srtm-dir', "--str-dir is required when VRT is produced")
    execute(['gdalbuildvrt', options.srtm_file] + [os.path.join(options.srtm_dir, s) for s in options.srtms], options.dryrun)

def proj_DEM(options):
    # 3- SARDEMProjection
    directiontoscandemc, directiontoscandeml, gain = do_execute_demProjection(
            insar=options.s1image, indem=options.srtm_file, withxyz=True,
            outPath=options.s1_onto_dem,
            ram=options.ram, dryrun=options.dryrun)
    options.directiontoscandemc = directiontoscandemc
    options.directiontoscandeml = directiontoscandeml
    options.gain                = gain

def register_xyz(options):
    options.app_xyz = do_register_cartesianMeanEstimation(
            insar=options.s1image, indem=options.srtm_file, indemproj=options.s1_onto_dem,
            indirectiondemc=options.directiontoscandemc, indirectiondeml=options.directiontoscandeml,
            outPath=options.xyz_onto_s1, ram=options.ram)
    return options.app_xyz

def register_normals(options):
    xyz = options.app_xyz if hasattr(options, 'app_xyz') else options.xyz_onto_s1
    options.app_normal = do_register_computeNormals(
            inxyz=xyz, outPath=options.normals, in_memory=options.in_memory, ram=options.ram)
    return options.app_normal

def register_LIA(options):
    xyz    = options.app_xyz    if hasattr(options, 'app_xyz')    else options.xyz_onto_s1
    normal = options.app_normal if hasattr(options, 'app_normal') else options.normals
    options.app_lia = do_register_computeLIA(
            inxyz=xyz, innormals=normal,
            inxyz_filename=options.xyz_onto_s1, innormals_filename=options.normals,
            out_lia=options.deg_lia, out_sin=options.sin_lia,
            in_memory=options.in_memory, ram=options.ram)
    return options.app_lia

def execute_orthorectify(options):
    flatten = itertools.chain.from_iterable
    filenames = flatten([options.filenames[action] for action in options.selected_actions if action in options.filenames])
    if options.check_height:
        filenames = list(filenames) + [options.check_height]
    logging.info('Orthorectifying %s', filenames)
    for s1_filename in filenames:
        if os.path.isfile(s1_filename):
            execute(['copy-projection.py', options.s1image, s1_filename], dryrun=options.dryrun)
            dirname, basename = os.path.split(s1_filename)
            s2_filename = os.path.join(dirname, "%s_%s" % (options.s2_tilename, basename))
            do_execute_orthorectify(
                    s1_filename, s2_filename,
                    s2_tilename=options.s2_tilename, s2_tile_origin=options.s2_origin,
                    srtm_file=options.srtm_file, geoid_file=options.geoid,
                    in_memory=options.in_memory, dryrun=options.dryrun, ram=options.ram)
    return None

def execute_check_height(options):
    xyz = options.xyz_onto_s1
    app = do_execute_check_height(
            xyz=xyz, outPath=options.check_height, ram=options.ram, dryrun=options.dryrun)
    return None

k_actions = {
        'VRT' : build_VRT,
        'DEM' : proj_DEM,
        'XYZ' : register_xyz,
        'NORM': register_normals,
        'LIA' : register_LIA,
        # 'HGT' : execute_check_height,
        }

def confirm(options):
    def used(action, selected_actions):
        return (action in selected_actions) and colors.YES() or colors.NO()
    # Using Python 3.6 f-strings
    VRT  = used('VRT',  options.selected_actions)
    DEM  = used('DEM',  options.selected_actions)
    XYZ  = used('XYZ',  options.selected_actions)
    NORM = used('NORM', options.selected_actions)
    LIA  = used('LIA',  options.selected_actions)
    ORT  = colors.YES() if options.orthorectify else colors.NO()
    HGT  = colors.YES() if options.check_height else colors.NO()
    assert(options.s1image)
    assert(options.output_dir)
    s = f"""
=====< Execution parameters >=======================
Actions:
    1. VRT:        {VRT} --> Builds VRT of intersecting DEMS
    2. DEM:        {DEM} --> Maps XYZ on DEM geometry with SARDEMProjection
    3. XYZ:        {XYZ} --> Maps XYZ on SAR Geometry with SARCartesianMeanEstimation
    4. NORM:       {NORM} --> Computes map of normal vector in SAR Geometry with ExtractNormalVector
    5. LIA:        {LIA} --> Computes LIA maps with SARComputeLocalIncidenceAngle
    6. ORT:        {ORT} --> Orthorectify LIA maps
    Check height:  {HGT} --> Computes map of sqrt(X²+Y²+Z²)

Options:
    - RAM            : {options.ram}
    - Input S1 Image : {colors.file(options.s1image)}
    - Output dir     : {colors.file(options.output_dir)}
    - GEOID file     : {colors.file(options.geoid or 'Ø: None')}"""
    if options.srtm_file:
        s += f"""
    - SRTM file      : {colors.file(options.srtm_file)}"""
    if options.srtms:
        s += f"""
    - SRTMs          : {options.srtms}
    - SRTM dir       : {colors.file(options.srtm_dir)}
    - SRTM shape file: {colors.file(options.srtm_shp)}
        """
    if options.orthorectify:
        s += f"""
    Orthorelated options
    - S2 tilename : {colors.file(options.s2_tilename)}
    - S2 shp      : {colors.file(options.s2grid_shp)}
    - S2 origin   : {options.s2_origin}
        """

    print(s)
    cont = input("\n%sDo you want to continue? [Y/O/n]%s "% (colors.violet,colors.reset))
    print(colors.reset)

    return re.match('^(y|o|yes|oui|)$', cont, flags=re.IGNORECASE)

@click.command(context_settings=dict(help_option_names=["-h", "--help"]))
@click.version_option()
@click.option(
        "--verbose", "-v", "verbose",
        is_flag=True,
        help="Log more")
@click.option(
        "--dryrun",
        is_flag=True,
        help="Display the processing shall would be realized, but none is done.")
@click.option(
        "--in_memory/--no-in-memory",
        default=True,
        show_default=True,
        is_flag=True,
        help="Tells whether most chaining between applications shall be done in-memory, or whether each intermediary product shall be produced on disk")
@click.option(
        "--ram",
        default='10000',
        show_default=True,
        help="RAM allocated to OTB processes, in MB")
@click.option(
        "--srtm-shp",
        # is_flag=True,
        help="SRTM Shapefile",
        type=click.Path(exists=True)
        )
@click.option(
        "--srtm-dir",
        # is_flag=True,
        help="Path to SRTM directory",
        type=click.Path(exists=True)
        )
@click.option(
        "--srtm-file",
        help="Path to SRTM file that overlaps the input S1Image. Default generated from SRTM shapefile and SRTM directory.",
        type=click.Path(exists=True)
        )
@click.option(
        "--geoid",
        # is_flag=True,
        help="Path to geoid file",
        type=click.Path(exists=True)
        )
@click.option(
        "-o", "--output-dir",
        default=".",
        show_default=True,
        # is_flag=True,
        help="Output directory")
@click.option(
        "--from", "from_",
        type=click.Choice(k_actions.keys()),
        default='VRT',
        show_default=True,
        help="First step to execute"
        )
@click.option(
        "--upto", "upto_",
        type=click.Choice(k_actions.keys()),
        default='LIA',
        show_default=True,
        help="Last step to execute"
        )
@click.option(
        "--check-height",
        default=False, show_default=True,
        is_flag=True,
        help="Generate an image of height from Earth Center XYZ Cartesian map")
@click.option(
        "--dirl",
        # is_flag=True,
        type=click.INT,
        help="Used when using --from XYZ to override directiontoscandeml")
@click.option(
        "--dirc",
        # is_flag=True,
        type=click.INT,
        help="Used when using --from XYZ to override directiontoscandemc")
@click.option(
        "--orthorectify/--no-orthorectify",
        default=True, show_default=True,
        is_flag=True,
        help="Orthorectify XYZ, NORM and LIA images produced")
@click.option(
        "--s2grid-shp",
        # is_flag=True,
        help="S2Grid Shapefile. Required only for --orthorectify.",
        type=click.Path(exists=True)
        )
@click.option(
        "--s2-tilename",
        # is_flag=True,
        help="Name of S2 to orthoproject to. Required only for --orthorectify.",
        )
@click.argument('s1image', type=click.Path(exists=True))
def main(dryrun, verbose, geoid, srtm_dir, srtm_shp, srtm_file, s1image,
        from_, upto_,
        output_dir, check_height,
        orthorectify, s2grid_shp, s2_tilename,
        dirc, dirl, in_memory, ram):
    if verbose:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        os.environ["OTB_LOGGER_LEVEL"]="DEBUG"
    else:
        logging.basicConfig(level=logging.INFO, format='%(levelname)s - %(message)s')

    options = SimpleNamespace()
    options.dryrun       = dryrun
    options.in_memory    = in_memory
    options.ram          = ram
    options.s1image      = s1image
    options.srtm_shp     = srtm_shp
    options.srtm_dir     = srtm_dir
    options.geoid        = geoid
    options.sar_basename = os.path.basename(s1image)
    options.orthorectify = orthorectify
    options.s2grid_shp   = s2grid_shp
    options.s2_tilename  = s2_tilename
    options.output_dir   = output_dir
    options.s1_onto_dem  = os.path.join(output_dir, 's1_onto_dem_' + options.sar_basename)
    options.xyz_onto_s1  = os.path.join(output_dir, 'xyz_onto_s1_' + options.sar_basename)
    options.normals      = os.path.join(output_dir, 'normals_'     + options.sar_basename)
    options.deg_lia      = os.path.join(output_dir, 'deg_lia_'     + options.sar_basename)
    options.sin_lia      = os.path.join(output_dir, 'sin_lia_'     + options.sar_basename)
    options.check_height = os.path.join(output_dir, 'hgt_onto_s1_' + options.sar_basename) if check_height else False
    options.filenames    = {
            'DEM' : options.s1_onto_dem,
            'XYZ' : options.xyz_onto_s1,
            'NORM': options.normals,
            'LIA' : [options.deg_lia, options.sin_lia],
            # 'HGT' : options.check_height
            }
    options.directiontoscandemc = dirc
    options.directiontoscandeml = dirl

    action_names = [k for k in k_actions.keys()]
    start = action_names.index(from_)
    stop  = action_names.index(upto_) + 1
    options.selected_actions = action_names[start:stop]

    # if not check_height and 'HGT' in options.selected_actions:
    #     options.selected_actions.remove('HGT')

    # Find/Check intersecting DEMs
    need_dem = list(set(['VRT', 'DEM', 'XYZ']) & set(options.selected_actions)) or options.orthorectify
    if srtm_file:
        # TODO: Check file intersects s1image
        #overlaps = check_SRTM_overlaps_s1image(s1image, srtm_file)
        #if not overlaps:
        #    print(colors.red + "SRTM doesn't overlaps S1 Image"+colors.reset)
        #    sys.exit(127)
        options.srtm_file = srtm_file
        options.srtms = ()
        # remove VRT action
    elif need_dem:
        if not options.srtm_dir:
            raise click.BadOptionUsage('--srtm-dir', "--srtm-dir is required when DEM.vrt is generated on-the-fly")
        if not options.srtm_shp:
            raise click.BadOptionUsage('--srtm-shp', "--srtm-shp is required when DEM.vrt is generated on-the-fly")
        options.srtms = find_SRTM_intersection(options.s1image, options.srtm_shp)
        vrt_basename = '-'.join([Path(s).stem for s in options.srtms])
        options.srtm_file = os.path.join(options.output_dir, vrt_basename + '.vrt')
        logging.debug('actions: %s', options.selected_actions)
        if ('VRT' not in options.selected_actions) and (not os.path.isfile(options.srtm_file)):
            logging.debug("Force VRT generation as %s doesn't exist", options.srtm_file)
            options.selected_actions.insert(0, 'VRT')
    else:
        options.srtms     = ()
        options.srtm_file = None

    if 'XYZ' in options.selected_actions and 'DEM' not in options.selected_actions:
        if not options.directiontoscandeml:
            raise click.BadOptionUsage('--dirl', "--dirl is required when producing XYZ map from existing DEM file")
        if not options.directiontoscandemc:
            raise click.BadOptionUsage('--dirc', "--dirc is required when producing XYZ map from existing DEM file")

    if options.orthorectify:
        if not options.s2_tilename:
            raise click.BadOptionUsage('--s2-tilename', "--s2-tilename is required to orthorectify results")
        if not options.s2grid_shp:
            raise click.BadOptionUsage('--s2grid-shp', "--s2grid-shp is required to orthorectify results")
        options.s2_origin = get_origin_of_s1image_intersection_with_s2(options.s1image, options.s2_tilename, options.s2grid_shp)

    # Shall we continue?
    if not confirm(options):
        print(colors.orange + "Abort"+colors.reset)
        sys.exit(127)
    logging.debug("continuing...")

    # Prep
    os.makedirs(output_dir, exist_ok=True)
    if geoid:
        os.environ['OTB_GEOID_FILE'] = geoid

    # Build pipelines/execute steps
    for action_name in options.selected_actions:
        app = k_actions[action_name](options)

    # Execute images production
    if app and not dryrun:
        with ExecutionTimer('-> '+'|'.join(options.selected_actions), True) as t:
            app.ExecuteAndWriteOutput()

    # If hgt shall be computed from XYZ
    if options.check_height:
        execute_check_height(options)

    # Last orthorectification steps
    execute_orthorectify(options)

if __name__ == '__main__':  # Required for Dask: https://github.com/dask/distributed/issues/2422
    main()

# }}}1
############################################################################
# vim: set sw=4:
